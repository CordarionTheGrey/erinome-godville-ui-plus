import ast
import codecs
import os.path
import re

from waflib.Task    import Task
from waflib.TaskGen import before, feature

import taskgen_utils


# Syntax:

DIRECTIVE_RX = re.compile(rb"^([ \t\f\v]*)//!([^\r\n]*)\r?\n?", re.M)


def parse_string(quoted_string):
	if quoted_string.startswith(('"""', "'''")):
		return None # Don't consider triple-quoted strings as valid syntax.
	try:
		# Despite the word "eval", this does not execute arbitrary Python code.
		return ast.literal_eval(quoted_string)
	except (ValueError, SyntaxError):
		return None


def parse_directive(line):
	m = re.fullmatch(rb"""include\s*((["']).*\2)\s*;""", line.strip())
	if m is not None:
		filename = parse_string(m.group(1).decode())
		if filename is not None:
			return ("include", filename.replace('\\', '/'))
	return (None, )


# Semantics:

def iter_dependencies(source):
	# We calculate dependencies non-recursively, relying on Waf for cascade updates.
	for m in DIRECTIVE_RX.finditer(source):
		directive, *args = parse_directive(m.group(2))
		if directive == "include":
			yield args[0]


# `textwrap.indent` does not work with byte strings.
def indent(text, prefix):
	lines = [ ]
	for line in text.splitlines(True):
		if 0x0D != line[0] != 0x0A: # not line.startswith((b'\r', b'\n'))
			lines.append(prefix)
		lines.append(line)
	return b"".join(lines)


def preprocess(source, load_file):
	def replacer(m):
		directive, *args = parse_directive(m.group(2))
		if directive == "include":
			text = load_file(args[0])
		else:
			return m.group() # Leave unchanged otherwise.
		return text if m.start(1) == m.end(1) else indent(text, m.group(1))

	return DIRECTIVE_RX.sub(replacer, source)


def resolve_include_path(path, file_dir):
	return os.path.join(file_dir, path) if path.startswith(("./", "../")) else path


# Waf-specific stuff:

class js_preproc(Task):
	color = "GREEN"
	weight = 1

	def __str__(self):
		return self.inputs[0].srcpath()

	def scan(self):
		nodes = [
			# Absolute include paths are intentionally unsupported.
			self.find_node(resolve_include_path(filename, self.file_dir))
			for filename in iter_dependencies(self.inputs[0].read("rb"))
		]
		return list(filter(None, nodes)), ()

	def load_file(self, filename):
		text = self.find_node(resolve_include_path(filename, self.file_dir)).read("rb")
		return text[3:] if text.startswith(codecs.BOM_UTF8) else text

	def run(self):
		result = preprocess(self.inputs[0].read("rb"), self.load_file)
		for node in self.outputs:
			node.write(result, "wb")


@feature("js_preproc")
@before("process_source")
def process_js_preproc(tgen):
	root_dir = getattr(tgen, "root_dir", tgen.path)
	if isinstance(root_dir, str):
		root_dir = tgen.path.find_dir(root_dir)
	find_node = getattr(tgen, "find_node", root_dir.find_or_declare)

	for source, target in zip(tgen.to_nodes(tgen.source), tgen.to_out_nodes(tgen.target)):
		task = tgen.create_task("js_preproc", source, target)
		task.find_node = find_node
		task.file_dir = os.path.relpath(source.parent.relpath(), root_dir.relpath())

	tgen.source = [ ]
