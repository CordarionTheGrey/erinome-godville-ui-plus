import itertools
import subprocess

from waflib.Task    import Task
from waflib.TaskGen import before, feature

import taskgen_utils


def configure(cnf):
	cnf.find_program("fontforge", var="FONTFORGE", mandatory=False)


class fontforge(Task):
	vars = ["FONTFORGE"]
	weight = 2

	@property
	def color(self):
		return "GREEN" if len(self.inputs) <= 2 else "YELLOW"

	def keyword(self):
		return "Executing"

	def _format_nodes(self, nodes):
		if len(nodes) == 1:
			return nodes[0].srcpath()
		else:
			return "{%s}" % ", ".join([node.srcpath() for node in nodes])

	def __str__(self):
		return "%s: %s -> %s" % (
			self.inputs[0].srcpath(),
			self._format_nodes(self.inputs[1:]),
			self._format_nodes(self.outputs),
		)

	def run(self):
		# ${FONTFORGE} --script ${SRC} ${TGT} 2>&1
		# `fontforge` requires absolute paths to all files.
		return self.exec_command(self.env.FONTFORGE + ["--script"] + [
			node.abspath() for node in itertools.chain(self.inputs, self.outputs)
		], stderr=subprocess.STDOUT)


@feature("fontforge")
@before("process_source")
def process_fontforge(tgen):
	if not tgen.env.FONTFORGE:
		tgen.bld.fatal(
			"'fontforge' executable was not found in the 'PATH'."
			" Run 'waf configure' with 'FONTFORGE' environment variable set."
		)
	tgen.create_task("fontforge", tgen.to_nodes(tgen.source), tgen.to_out_nodes(tgen.target))
	tgen.source = [ ]
