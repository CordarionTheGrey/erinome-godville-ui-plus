import os.path
import shutil

from waflib           import Utils
from waflib.Configure import conf
from waflib.Task      import Task
from waflib.TaskGen   import before, feature

import taskgen_utils


class cp(Task):
	color = "PINK"
	weight = -1

	def keyword(self):
		return "Copying"

	@property
	def _outputs(self):
		return self.outputs

	def _format_output(self, node):
		if node.name == self.inputs[0].name:
			return node.parent.srcpath() + os.path.sep
		else:
			return node.srcpath()

	def __str__(self):
		if len(self._outputs) == 1:
			return "%s -> %s" % (self.inputs[0].srcpath(), self._format_output(self._outputs[0]))
		else:
			outputs = ", ".join(map(self._format_output, self._outputs))
			return "%s -> {%s}" % (self.inputs[0].srcpath(), outputs)

	def run(self):
		data = self.inputs[0].read("rb") # Assume we won't get a MemoryError.
		for node in self._outputs:
			node.write(data, "wb")
			shutil.copystat(self.inputs[0].abspath(), node.abspath())


@feature("copy")
@before("process_source")
def process_copy(tgen):
	targets = tgen.to_out_nodes(tgen.target)
	for source in tgen.to_nodes(tgen.source):
		tgen.create_task("cp", source, targets)
	tgen.source = [ ]


@feature("subst_all")
@before("process_source")
def process_subst_all(tgen):
	"""
	Remove `strip` prefix from each of the `source` files and copy them to each of the `target`
	directories, substituting variables unless `is_copy` is True.
	"""

	sources = tgen.to_nodes(tgen.source)
	target_dirs = tgen.to_out_nodes(getattr(tgen, "target", [ ])) or [tgen.bld.bldnode]
	if sources and not target_dirs:
		raise ValueError("At least one target is required: %r" % tgen)
	strip = getattr(tgen, "strip", "")
	if isinstance(strip, str):
		strip = tgen.path.find_dir(strip)

	new_sources = [ ]
	new_targets = [ ]
	is_copy = getattr(tgen, "is_copy", False)
	for source_node in sources:
		relpath = source_node.path_from(strip)
		targets = [target_node.find_or_declare(relpath) for target_node in target_dirs]
		if is_copy:
			tgen.bld(
				features="copy",
				source=source_node,
				target=targets,
			)
		else:
			new_sources.append(source_node)
			new_targets.append(targets[0])
			if len(targets) > 1:
				tgen.bld(
					features="copy",
					source=targets[0],
					target=targets[1:],
				)

	tgen.source = new_sources
	tgen.target = new_targets
	if new_sources:
		tgen.meths.insert(tgen.meths.index("process_source"), "process_subst")


@conf
def subst_all(bld, source, target="", **kwargs):
	return bld(features="subst_all", source=source, target=target, **kwargs)


@conf
def copy_all(bld, *args, **kwargs):
	return bld.subst_all(*args, is_copy=True, **kwargs)
