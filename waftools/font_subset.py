import contextlib

from waflib         import Utils
from waflib.Task    import Task
from waflib.TaskGen import before, feature

import taskgen_utils


class font_subset(Task):
	color = "CYAN"
	weight = 2

	def keyword(self):
		return "Subsetting"

	def sig_explicit_deps(self):
		super().sig_explicit_deps()
		self.m.update(self.text.encode())
		self.m.update(Utils.h_list(self.unicodes))

	def run(self):
		import fontTools.subset as ftsub

		# https://fonttools.readthedocs.io/en/latest/subset/index.html
		options = ftsub.Options(ignore_missing_unicodes=False)
		options.drop_tables.append("FFTM")
		s = ftsub.Subsetter(options)
		s.populate(text=self.text, unicodes=self.unicodes)
		with contextlib.closing(
			ftsub.load_font(self.inputs[0].abspath(), options, dontLoadGlyphNames=True)
		) as font:
			s.subset(font)
			ftsub.save_font(font, self.outputs[0].abspath(), options)


@feature("font_subset")
@before("process_source")
def process_font_subset(tgen):
	sources = tgen.to_nodes(tgen.source)
	targets = tgen.to_out_nodes(tgen.target)
	if len(sources) != 1:
		raise ValueError("Only a single source is allowed: %r" % tgen)
	if len(targets) != 1:
		raise ValueError("Only a single target is allowed: %r" % tgen)

	task = tgen.create_task("font_subset", sources, targets)
	task.text = getattr(tgen, "text", "")
	task.unicodes = getattr(tgen, "unicodes", [ ])
	tgen.source = [ ]
