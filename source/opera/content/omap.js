// ordered map implementation. hand-written, compatible with ES5 - specially for Opera 12.
// see `omap.js` for an ES2015 implementation with identical interface.
(function(worker) {
'use strict';

worker.GUIp = worker.GUIp || {};

/**
 * These entries form a circular doubly-linked list.
 *
 * @protected
 * @typedef {Object} GUIp._OMapEntry
 * @property {string} key
 * @property {*} value
 * @property {!GUIp._OMapEntry} prev
 * @property {!GUIp._OMapEntry} next
 */

/**
 * @class
 * @param {?GUIp.OMap} [source]
 */
GUIp.OMap = function(source) {
	/**
	 * @protected
	 * @type {!Object<string, !GUIp._OMapEntry>}
	 */
	this._h = Object.create(null);
	/**
	 * @protected
	 * @type {?GUIp._OMapEntry}
	 */
	this._l = null;
	if (source) this._copyFrom(source);
};

GUIp.OMap.prototype = {
	constructor: GUIp.OMap,

	/**
	 * @protected
	 * @param {!GUIp.OMap} source
	 */
	_copyFrom: function(source) {
		var srcFirst = source._l,
			srcCur = srcFirst,
			cur;
		if (srcCur) {
			cur = this._l = {key: srcCur.key, value: srcCur.value, prev: null, next: null};
			while (true) {
				this._h[cur.key] = cur;
				srcCur = srcCur.next;
				if (srcCur === srcFirst) break;
				cur = cur.next = {key: srcCur.key, value: srcCur.value, prev: cur, next: null};
			}
			cur.next = this._l;
			this._l.prev = cur;
		}
	},

	/**
	 * @protected
	 * @param {string} key
	 * @param {*} value
	 */
	_insert: function(key, value) {
		var first = this._l;
		if (first) {
			first = first.prev = first.prev.next = {key: key, value: value, prev: first.prev, next: first};
		} else {
			first = this._l = {key: key, value: value, prev: null, next: null};
			first.prev = first.next = first;
		}
		this._h[key] = first;
	},

	/** @type {boolean} */
	get empty() {
		return !this._l;
	},

	/**
	 * @param {string} key
	 * @returns {*}
	 */
	get: function(key) {
		var entry = this._h[key];
		return entry && entry.value;
	},

	/**
	 * @param {string} key
	 * @param {*} value
	 * @returns {!GUIp.OMap}
	 */
	set: function(key, value) {
		var entry = this._h[key];
		if (entry) {
			entry.value = value;
		} else {
			this._insert(key, value);
		}
		return this;
	},

	/**
	 * @param {string} key
	 * @returns {!GUIp.OMap}
	 */
	remove: function(key) {
		var entry = this._h[key],
			prev, next;
		if (!entry) return this;
		prev = entry.prev;
		next = prev.next = entry.next;
		if (entry !== next) {
			next.prev = prev;
			if (entry === this._l) {
				this._l = next;
			}
		} else {
			this._l = null;
		}
		delete this._h[key];
		return this;
	},

	/**
	 * Must not modify the container during iteration!
	 *
	 * @param {function(*, string, !GUIp.OMap)} callback
	 * @param {*} [thisArg]
	 */
	forEach: function(callback, thisArg) {
		var first = this._l,
			cur = first;
		if (cur) {
			do {
				callback.call(thisArg, cur.value, cur.key, this);
			} while ((cur = cur.next) !== first);
		}
	},

	/**
	 * @returns {!Array<string>}
	 */
	getKeys: function() {
		var keys = [],
			first = this._l,
			cur = first;
		if (cur) {
			do {
				keys.push(cur.key);
			} while ((cur = cur.next) !== first);
		}
		return keys;
	},

	/**
	 * @returns {!GUIp.OMap}
	 */
	clone: function() {
		return new this.constructor(this);
	}
};

/**
 * @class
 */
GUIp.OMultiMap = function() {
	GUIp.OMap.call(this);
};

GUIp.OMultiMap.prototype = Object.create(GUIp.OMap.prototype);
GUIp.OMultiMap.prototype.constructor = GUIp.OMultiMap;

/**
 * @param {string} key
 * @returns {*}
 */
GUIp.OMultiMap.prototype.get1 = function(key) {
	var entry = this._h[key];
	return entry && entry.value[0];
};

/**
 * @param {string} key
 * @returns {!Array}
 */
GUIp.OMultiMap.prototype.require = function(key) {
	var entry = this._h[key],
		values;
	if (entry) return entry.value;
	values = [];
	this._insert(key, values);
	return values;
};

/**
 * @param {string} key
 * @param {*} value
 * @returns {!GUIp.OMultiMap}
 */
GUIp.OMultiMap.prototype.add = function(key, value) {
	var entry = this._h[key];
	if (entry) {
		entry.value.push(value);
	} else {
		this._insert(key, [value]);
	}
	return this;
};

})(this);
