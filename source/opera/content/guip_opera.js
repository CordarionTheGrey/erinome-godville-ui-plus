window.GUIp_browser = 'Opera';
window.GUIp_getResource = function(res,element,src) {
	if (!element) {
		return '';
	}
	var file, reader;
	if (file = window.GUIp_getResourceInternal(res)) {
		if (res.match(/\.(png|jpg)$/)) {
			reader = new FileReader();
			reader.onload = function () {
				if (src !== true) {
					element.style.backgroundImage = 'url(' + reader.result + ')';
				} else {
					element.src = reader.result;
				}
			}
			reader.readAsDataURL(file);
		}
	}
};
window.GUIp_getResourceInternal = function(url) { return null; };
