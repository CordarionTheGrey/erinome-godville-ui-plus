var ui_cmining = GUIp.common.mining = {};

/**
 * @typedef {Array<number>} GUIp.common.mining.Map
 */

/*
	map legend:

	0x0 - ( )   empty
	0x1 - (?)   unknown
	0x2 - (#)   wall
	0x3 - (#11) wall containing bits
	0x4 - (1)   bit
	0x5 - (11)  bits
	0x6 - (+)   medkit
	0x7 - (x)   thruster
	0x8 - (A)   boss
	0x9 - (B)   boss
	0xA - (C)   boss
	0xB - (D)   boss

	0x10  - (+) encouragement
	0x20  - (-) punishment
	0x30  - (=) miracle
	0x40  - (💀) dead boss
	0x080 - (⇡) trail
	0x180 - (⇢) trail
	0x280 - (⇣) trail
	0x380 - (⇠) trail
*/

ui_cmining._charCodes = {
	'#': 0x2,
	'1': 0x4,
	'+': 0x6,
	'x': 0x7,
	'A': 0x8,
	'B': 0x9,
	'C': 0xA,
	'D': 0xB,
	'💀': 0x40
};

ui_cmining._markerCharCodes = {
	'+': 0x10,
	'-': 0x20,
	'−': 0x20,
	'=': 0x30
};

ui_cmining._trailCharCodes = {
	'💀': 0x40,
	'⇡': 0x080,
	'⇢': 0x180,
	'⇣': 0x280,
	'⇠': 0x380
};

/**
 * @param {string} text
 * @returns {!Object<string, boolean>}
 */
ui_cmining.parseConditions = function(text) {
	return /все биты в хорошо видимых сейфах|all bits are visible and locked/.test(text) ? {
		clarity: true
	} : /боссы будут собирать биты даже в пол.те|bosses can pick up bits on the fly/.test(text) ? {
		snatching: true
	} : {}; // we don't need other conditions right now
};

/**
 * @this {number} Index of the own boss.
 * @param {!Element} cell
 * @returns {number}
 */
ui_cmining.parseMapCell = function(cell) {
	var classes = cell.classList,
		text = '',
		child;
	return (
		classes.contains('rmve') ? 0x1 : ( // ?
			(child = cell.firstElementChild) && (text = child.textContent) === '11' ? (
				classes.contains('dmw') ? 0x3 : 0x5 // #11 | 11
			) : ui_cmining._charCodes[text] || (
				text === '@' && 0x8 | this // ABCD
			)
		) | (
			(child = cell.getElementsByClassName('dm2')[0]) && ui_cmining._markerCharCodes[child.textContent] // +-=
		)
	) | (
		(child = cell.getElementsByClassName('dm1')[0]) && ui_cmining._trailCharCodes[child.textContent] // 💀⇡⇢⇣⇠
	);
};
