(function(worker) {
'use strict';

worker.GUIp = worker.GUIp || {};
GUIp.common = {};

//! include './logging.js';
// include './heap.js';
//! include './misc.js';
//! include './activities.js';
//! include './expr/package.js';
//! include './render_tester.js';
//! include './tooltips.js';
//! include './imap/package.js';
//! include './sailing.js';
//! include './mining.js';
//! include './notif.js';

})(this);
