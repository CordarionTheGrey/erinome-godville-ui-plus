/**
 * @const
 * @type {!Array<string>}
 */
ui_cexpr.gvAPI = [
	'health',
	'healthMax',
	'healthPrc',
	'gold',
	'supplies',
	'suppliesMax',
	'suppliesPrc',
	'inventory',
	'inventoryMax',
	'inventoryPrc',
	'inventoryHasItem',
	'inventoryHasType',
	'inventoryCountLike',
	'inventoryHealing',
	'inventoryUnsaleable',
	'inventoryUnsellable',
	'isEquipmentBold',
	'exp',
	'expTrader',
	'expForge',
	'portDistance',
	'auraName',
	'auraDuration',
	'bingoItems',
	'bingoSlotsLeft',
	'bingoTriesLeft',
	'couponPrize',
	'godpowerCapAvailable',
	'enemyHealth',
	'enemyHealthMax',
	'enemyHealthPrc',
	'enemyCount',
	'enemyAliveCount',
	'enemyAbilitiesCount',
	'enemyGold',
	'enemyHasAbility',
	'enemyHasAbilityLoc',
	'alliesHealth',
	'alliesHealthMax',
	'alliesHealthPrc',
	'alliesCount',
	'alliesAliveCount',
	'alliesAliveHealthMax',
	'lowHealth',
	'godpower',
	'godpowerMax',
	'godpowerPrc',
	'charges',
	'arenaAvailable',
	'sparAvailable',
	'dungeonAvailable',
	'sailAvailable',
	'miningAvailable',
	'arenaSendDelay',
	'sparSendDelay',
	'dungeonSendDelay',
	'sailSendDelay',
	'miningSendDelay',
	'fightMode',
	'fightType',
	'fightStep',
	'fightStepText',
	'dungeonChallenge',
	'dungeonChallengeReward',
	'guidedStepsCount',
	'sailConditions',
	'cargo',
	'pushReadiness',
	'bits',
	'bytes',
	'bitsPerByte',
	'bookBytes',
	'bookWords',
	'forgeBytes',
	'forgeWords',
	'inBossFight',
	'inFight',
	'inShop',
	'inTown',
	'nearestTown',
	'currentTown',
	'mileStones',
	'poiMileStones',
	'poiDistance',
	'mProgress',
	'sProgress',
	'lastNews',
	'lastDiary',
	'lastDiaryVoice',
	'lastDiarySign',
	'lastGuildChat',
	'isGoingBack',
	'isGoingForth',
	'isGoingGodville',
	'isGoingToGodville',
	'isFishing',
	'isTrading',
	'isForecast',
	'dailyForecast',
	'hasTemple',
	'hasArk',
	'monstersKilled',
	'currentMonster',
	'chosenMonster',
	'specialMonster',
	'strongMonster',
	'tamableMonster',
	'wantedMonster',
	'questName',
	'questNumber',
	'questProgress',
	'sideJobName',
	'sideJobDuration',
	'sideJobProgress',
	'petKnockedOut',
	'expTimeout',
	'logTimeout',
	'byteTimeout',
	'byteDoubleTimeout',
	'sparTimeout',
	'getSeconds',
	'getMinutes',
	'getHours',
	'getHoursUTC',
	'getHoursMSK',
	'getDay',
	'getDayUTC',
	'getDayMSK',
	'getDate',
	'getDateUTC',
	'getDateMSK',
	'getMonth',
	'getMonthUTC',
	'getMonthMSK',
	'voiceCooldown',
	'windowFocused'
].sort();

/**
 * @const
 * @type {!Object<string, boolean>}
 */
// we pass in an object literal here to allow calling methods inherited from Object.prototype
ui_cexpr.gvAPIObject = GUIp.common.makeHashSet(ui_cexpr.gvAPI, {});
