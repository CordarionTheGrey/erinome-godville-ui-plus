/** @property {boolean} GUIp.common.notif.supported */
/** @property {boolean} GUIp.common.notif.enabled */
/**
 * @function GUIp.common.notif.initialize
 */
/**
 * @function GUIp.common.notif.show
 * @param {string} title
 * @param {string} text
 * @param {?number} [timeout]
 * @param {?function(): (boolean|undefined)} [callback] - Return `false` to prevent focusing the tab.
 * @param {?string} [id]
 */
/**
 * @function GUIp.common.notif.hide
 * @param {string} id
 */

GUIp.common._createStubNotificationImpl = function() {
	var nop = function() { };
	return {
		supported: false,
		enabled: false,
		initialize: nop,
		_show: nop,
		_hide: nop
	};
};

GUIp.common._createDefaultNotificationImpl = function() {
	var notifications = Object.create(null),
		timers = Object.create(null);
	var impl = {
		supported: true,
		get enabled() {
			return Notification.permission === 'granted';
		},
		initialize: function() {
			if (!impl.enabled) {
				Notification.requestPermission();
			}
		},
		_show: function(id, title, text, timeout, callback) {
			var n = notifications[id] = new Notification(title, {
				body: text,
				icon: GUIp_getResource('icon64.png'),
				tag: 'eGUI+ ' + id,
				requireInteraction: true
			});
			GUIp.common.addListener(n, 'click', function() {
				var focus = callback ? callback() : true;
				if ((focus === undefined || focus) && !document.hasFocus()) {
					worker.focus();
				}
				impl._hide(id);
			});
			if (timeout > 0) {
				timers[id] = GUIp.common.setTimeout(impl._hide, timeout, id);
			}
		},
		_hide: function(id) {
			var n = notifications[id],
				timer = 0;
			if (!n) return;
			if (n.close) {
				n.close();
			}
			if ((timer = +timers[id])) {
				clearTimeout(timer);
				delete timers[id];
			}
			delete notifications[id];
		}
	};
	return impl;
};

// Yandex.Browser decided it will not support Notifications API on Android so we are forced to send
// Chrome-specific notifications via our background script
GUIp.common._createChromeNotificationImpl = function() {
	var callbacks = Object.create(null);
	GUIp.common.addListener(worker, 'message', function onNotificationClose(ev) {
		var response = ev.data,
			callback, focus;
		if (!response || !(response = response.erinomeMessage) || response.type !== 'notifyClosed') {
			return;
		}
		if (response.manual) {
			callback = callbacks[response.notifId];
			focus = callback ? GUIp.common.try2(callback) : true;
			// because Vivaldi ignores `window.focus()`
			GUIp.common.postErinomeMessage({
				type: 'makefocus',
				tab: focus === undefined || !!focus,
				window: true
			});
		}
		delete callbacks[response.notifId];
	});
	return {
		supported: true,
		enabled: true, // we have got the permission as part of the installation process
		initialize: function() { },
		_show: function(id, title, text, timeout, callback) {
			callbacks[id] = callback;
			GUIp.common.postErinomeMessage({
				type: 'notify',
				notifId: id,
				title: title,
				message: text,
				timeout: timeout
			});
		},
		_hide: function(id) {
			GUIp.common.postErinomeMessage({type: 'notifyHide', notifId: id});
		}
	};
};

GUIp.common.defineCachedProperty(GUIp.common, 'notif', function notif() {
	var impl = GUIp_browser === 'Chrome' ? (
		GUIp.common._createChromeNotificationImpl()
	) : worker.Notification ? (
		GUIp.common._createDefaultNotificationImpl()
	) : GUIp.common._createStubNotificationImpl();

	// pending notifications are stored in a singly-linked list
	impl._firstPending = null;
	impl._lastPending = null;
	impl._pending = Object.create(null);
	impl._timer = 0;

	var _onTimer = function() {
		// take one notification from the queue and show it
		for (var n = impl._firstPending; n; n = n.next) {
			delete impl._pending[n.id];
			if (n.title) {
				impl._show(n.id, n.title, n.text, n.timeout, n.callback);
				if (!(impl._firstPending = n.next)) {
					impl._lastPending = null;
				}
				return;
			}
		}
		impl._firstPending = impl._lastPending = null;
		clearInterval(impl._timer);
		impl._timer = 0;
	};
	impl.show = function(title, text, timeout, callback, id) {
		var n;
		if (!title) {
			throw new Error("notification's title cannot be empty");
		}
		if (typeof timeout !== 'number' || timeout !== timeout) {
			timeout = 5e3;
		}
		if (!id) id = title;
		if (!impl._timer) {
			// first notification in a while; show it immediately
			impl._show(id, title, text, timeout, callback);
			impl._timer = GUIp.common.setInterval(_onTimer, 500);
		} else if ((n = impl._pending[id])) {
			// update pending notification
			n.title = title;
			n.text = text;
			n.timeout = timeout;
			n.callback = callback;
		} else {
			// queue that notification
			n = impl._pending[id] = {id: id, title: title, text: text, timeout: timeout, callback: callback, next: null};
			if (impl._lastPending) {
				impl._lastPending.next = n;
			} else {
				impl._firstPending = n;
			}
			impl._lastPending = n;
		}
	};
	impl.hide = function(id) {
		var n = impl._pending[id];
		if (n) n.title = '';
		impl._hide(id);
	};
	return impl;
});
