/**
 * @alias GUIp.common.islandsMap.domParsers
 * @namespace
 */
ui_imap.domParsers = {};

/**
 * @param {!SVGElement} svg
 * @returns {number} A radius of a circle circumscribed around a tile.
 */
ui_imap.domParsers.detectMapScale = function(svg) {
	try {
		var p = svg.querySelector('g.tile polygon').getAttribute('points').split(/[\s,]+/),
			dx = +p[0] - +p[6],
			dy = +p[1] - +p[7];
		return Math.sqrt(dx * dx + dy * dy) * 0.5;
	} catch (e) {
		return 11;
	}
};

/**
 * @private
 * @param {!SVGElement} node
 * @param {number} scale
 * @returns {GUIp.common.islandsMap.Vec}
 */
ui_imap.domParsers._getNodePos = function(node, scale) {
	var matrix = node.transform.baseVal.getItem(0).matrix;
	return ui_imap.vec.fromCartesian(matrix.e, matrix.f, scale);
};

/**
 * @private
 * @const {!Object<string, number>}
 */
ui_imap.domParsers._charCodes = {
	'⁂': 0x2C, // ,
	'△': 0x3B, // ;
	'🗻': 0x3B, // ;
	'👾': 0x62, // b
	'🐠': 0x42, // B
	'?': 0x69, // i
	'🙏': 0x76, // v
	'🔧': 0x6E, // n
	'🔦': 0x6D, // m
	'🍴': 0x3C, // <
	'💡': 0x3E, // >
	'🌀': 0x40, // @
	'♂': 0x4D, // M
	'✺': 0x74, // t
	'☀': 0x79, // y
	'♨': 0x75, // u
	'☁': 0x6F, // o
	'❄': 0x5B, // [
	'✵': 0x5D, // ]
	'↗': 0x65, // e
	'→': 0x64, // d
	'↘': 0x63, // c
	'↙': 0x7A, // z
	'←': 0x61, // a
	'↖': 0x71  // q
};

/**
 * Parse info from the tile node and update the model and view with it.
 *
 * @private
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 * @param {!SVGElement} node
 * @param {!Object<string, boolean>} conditions
 */
ui_imap.domParsers._processTileNodeMV = function(model, view, node, conditions) {
	var pos = this._getNodePos(node, view.scale),
		classes = node.classList,
		isIsland = classes.contains('island'),
		code = classes.contains('unknown') ? 0x3F : isIsland ? 0x49 : 0x20 /*?I */,
		tagName = '',
		titleNode = null,
		textNode = null,
		text = '',
		arkIndex = -1,
		poiGroupIndex = -1,
		cls = '',
		cls0 = 0x0,
		num = 0,
		isFoggedPOI = false,
		newCode = 0x0;

	// precalc children to avoid multiple calls to .getElementsByTagName
	for (var child = node.firstChild; child; child = child.nextSibling) {
		tagName = child.tagName;
		if (tagName === 'title') {
			titleNode = child;
		} else if (tagName === 'text') {
			textNode = child;
		}
	}

	if (classes.contains('port')) {
		code = 0x70; // p
	} else if (conditions.locked) {
		if (titleNode) {
			// this code is executed for almost every tile, so don't use regexes
			text = titleNode.textContent.trim().toLowerCase();
			if (text.startsWith('закрытая граница') || text.startsWith('closed edge')) {
				// note: isIsland === true && isBorder === true
				code = 0x24; // $
			}
		}
	} else if (classes.contains('border')) { // note: the port also has this class
		code = 0x23; // #
	}

	for (var i = 0, len = classes.length; i < len; i++) {
		cls = classes[i];
		cls0 = cls.charCodeAt(0);
		if (cls0 === 0x70 /*p*/ && cls.charCodeAt(1) === 0x6C /*l*/ && (num = parseInt(cls.slice(2))) > 0) {
			// ark
			if (arkIndex !== -1) {
				GUIp.common.warn('multiple arks: #' + (arkIndex + 1) + ' vs #' + num, 'at', pos);
				continue;
			}
			arkIndex = num - 1;
		} else {
			isFoggedPOI = cls0 === 0x69; // i
			if ((isFoggedPOI || (cls0 === 0x6F /*o*/ && cls.charCodeAt(1) === 0x69 /*i*/)) &&
				(num = parseInt(cls.slice(2 - isFoggedPOI))) > 0
			) {
				// point of interest
				if (poiGroupIndex !== -1) {
					GUIp.common.warn('multiple POIs: #' + (poiGroupIndex + 1) + ' vs #' + num, 'at', pos);
					continue;
				}
				poiGroupIndex = num - 1;
				if (isFoggedPOI) {
					code = ui_imap.conv.poiCodes[poiGroupIndex % ui_imap.conv.poiCodes.length];
				}
			}
		}
	}

	if (textNode) {
		text = textNode.textContent.trim();
		if (text === '♀') {
			code = isIsland ? 0x66 : 0x46; // fF (yes, fenimals are supposed to live on islands too)
		} else if (text === '💰') {
			code = isIsland ? 0x47 : 0x67; // Gg
		} else {
			// cast to int because it might be a property in the prototype chain
			newCode = this._charCodes[text] | 0;
			if (newCode) code = newCode;
		}
	}

	if (pos in model.tiles) {
		if (arkIndex !== -1 && !model.arks.includes(pos)) {
			// merge the ark with the terrain below it
			model.arks[arkIndex] = pos;
			view.addNode(pos, node);
		} else {
			GUIp.common.warn('duplicate tile at ' + pos + ':', model.tiles[pos], 'vs', node);
		}
		return;
	}

	if (arkIndex !== -1) {
		model.arks[arkIndex] = pos;
	}
	if (poiGroupIndex !== -1) {
		model.poiGroupIndexAt[pos] = poiGroupIndex;
	}
	ui_imap.mtrans.addTile(model, pos, code);
	view.addNode(pos, node);
};

/**
 * @param {!SVGElement} svg
 * @returns {!GUIp.common.islandsMap.View}
 */
ui_imap.domParsers.vFromSVG = function(svg) {
	var view = new ui_imap.View(svg, this.detectMapScale(svg)),
		tiles = svg.getElementsByClassName('tile'),
		node;
	view.root = svg.getElementsByTagName('g')[0] || null;
	for (var i = 0, len = tiles.length; i < len; i++) {
		node = tiles[i];
		// in case of duplicates, keep the latest tile since it will be rendered above former ones
		view.addNode(this._getNodePos(node, view.scale), node);
	}
	return view;
};

/**
 * @param {!SVGElement} svg
 * @param {number} step
 * @param {!Object<string, boolean>} conditions
 * @returns {{model: !GUIp.common.islandsMap.Model, view: !GUIp.common.islandsMap.View}}
 */
ui_imap.domParsers.mvFromSVG = function(svg, step, conditions) {
	var model = new ui_imap.Model(step),
		view = new ui_imap.View(svg, this.detectMapScale(svg)),
		tiles = svg.getElementsByClassName('tile');
	view.root = svg.getElementsByTagName('g')[0] || null;
	for (var i = 0, len = tiles.length; i < len; i++) {
		this._processTileNodeMV(model, view, tiles[i], conditions);
	}
	if (model.port === 0x8080) {
		ui_imap.mtrans.guessPort(model);
	}
	return {model: model, view: view};
};
