(function(window) {
'use strict';

/** @typedef {function({type: string, notifId: string, manual: boolean})} NotificationResponseCallback */

/**
 * @typedef {Object} Notification
 * @property {!Object<string, boolean>} group
 * @property {string} localNotifId
 * @property {NotificationResponseCallback} callback
 * @property {number} timer
 */

// we prevent ourselves from being unloaded while at least one Godville tab is open so using global variable is safe
/** @type {!Object<string, !Notification>} */
var allNotifications = Object.create(null);

/**
 * @param {string} id
 * @param {!Notification} n
 */
var _destroyNotification = (id, n) => {
	if (n.timer) clearTimeout(n.timer);
	chrome.notifications.clear(id);
};

/**
 * @param {string} id
 * @param {boolean} manual
 */
var hideNotification = (id, manual) => {
	var n = allNotifications[id];
	if (!n) return;
	_destroyNotification(id, n);
	n.callback({type: 'notifyClosed', notifId: n.localNotifId, manual: manual});
	delete allNotifications[id];
	delete n.group[id];
};

var _notificationTemplate = {
	type: 'basic',
	iconUrl: chrome.extension.getURL('icon64.png'),
	title: '',
	message: '',
	contextMessage: 'Erinome Godville UI+',
	requireInteraction: true // we use timers to close notifications ourselves
};

class NotificationGroup {
	/**
	 * @param {NotificationResponseCallback} callback
	 */
	constructor(callback) {
		this._groupId = Math.random().toString();
		this._callback = callback;
		/** @type {!Object<string, boolean>} */
		this._ownIds = Object.create(null);
	}
	/**
	 * @param {{notifId: string, title: string, message: string, timeout: number}} options
	 */
	show({notifId: localNotifId, title, message, timeout}) {
		var id = this._groupId + localNotifId,
			existing = allNotifications[id];
		if (existing && existing.timer) {
			clearTimeout(existing.timer);
		}
		_notificationTemplate.title = title;
		_notificationTemplate.message = message;
		chrome.notifications.create(id, _notificationTemplate);
		allNotifications[id] = {
			group: this._ownIds,
			localNotifId: localNotifId,
			callback: this._callback,
			timer: timeout > 0 ? setTimeout(hideNotification, timeout, id, false) : 0
		};
		this._ownIds[id] = true;
	}
	/**
	 * @param {string} localNotifId
	 */
	hide(localNotifId) {
		hideNotification(this._groupId + localNotifId, false);
	}
	destroyAll() {
		var ids = Object.keys(this._ownIds),
			id = '',
			n;
		for (var i = 0, len = ids.length; i < len; i++) {
			id = ids[i];
			if ((n = allNotifications[id])) {
				_destroyNotification(id, n);
				delete allNotifications[id];
			}
		}
		// this._ownIds = Object.create(null);
	}
}

var _tabActivation = {active: true},
	_windowActivation = {focused: true};
chrome.runtime.onConnect.addListener(port => {
	var tab = port.sender.tab,
		notifGroup = new NotificationGroup(response => port.postMessage(response));
	port.onMessage.addListener(function onMessage(msg) {
		switch (msg.type) {
			case 'playsound':
				try {
					new Audio(msg.content).play();
				} catch (e) { }
				break;
			case 'makefocus':
				if (!tab) break;
				if (msg.tab) {
					chrome.tabs.update(tab.id, _tabActivation);
				}
				if (msg.window) {
					chrome.windows.update(tab.windowId, _windowActivation);
				}
				break;
			case 'notify':
				notifGroup.show(msg);
				break;
			case 'notifyHide':
				notifGroup.hide(msg.notifId);
				break;
		}
	});
	port.onDisconnect.addListener(() => notifGroup.destroyAll());
});

chrome.notifications.onClicked.addListener(id => hideNotification(id, true));
chrome.notifications.onClosed.addListener(id => hideNotification(id, false));

})(this);
