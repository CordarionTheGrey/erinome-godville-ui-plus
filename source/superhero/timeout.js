// ui_timeout
var ui_timeout = worker.GUIp.timeout = {};

ui_timeout.bar = null;
ui_timeout.timeout = 0;
ui_timeout.finishDate = 0;
Object.defineProperty(ui_timeout, 'running', {get: function() {
	return Date.now() < this.finishDate;
}});
ui_timeout._tickTimeout = 0;
ui_timeout._tick = function() {
	this._tickTimeout = 0;
	ui_utils.updateVoiceSubmitState();
	ui_informer.updateCustomInformers();
}.bind(ui_timeout);
// creates timeout bar element
ui_timeout.create = function() {
	this.bar = document.createElement('div');
	this.bar.id = 'timeout_bar';
	document.body.insertAdjacentElement('afterbegin', this.bar);
};
// starts timeout bar
ui_timeout.start = function() {
	var customTimeout = parseFloat(ui_storage.get('Option:voiceTimeout'));
	worker.clearTimeout(this._tickTimeout);
	this.timeout = customTimeout > 0 ? customTimeout : 20;
	this.bar.style.transitionDuration = '0s';
	this.bar.classList.remove('running');
	this._tickTimeout = GUIp.common.setTimeout(this._delayedStart, 10);
};
ui_timeout._delayedStart = function() {
	this.bar.style.transitionDuration = this.timeout + 's';
	this.bar.classList.add('running');
	this.finishDate = Date.now() + this.timeout*1000;
	this._tickTimeout = GUIp.common.setTimeout(this._tick, this.timeout*1000);
	ui_utils.updateVoiceSubmitState();
}.bind(ui_timeout);
