// ui_data
var ui_data = worker.GUIp.data = {};

// base variables initialization
ui_data.init = function() {
	var onAlertToggle;
	ui_data._initVariables();
	// init mobile cookies
	GUIp.common.forceDesktopPage();
	// desktop notifications permissions
	if (ui_storage.getFlag('Option:enableInformerAlerts') || ui_storage.getFlag('Option:enablePmAlerts')) {
		GUIp.common.notif.initialize();
	}
	onAlertToggle = function(newValue) {
		if (newValue === 'true') GUIp.common.notif.initialize();
	};
	ui_storage.addListener('Option:enableInformerAlerts', onAlertToggle);
	ui_storage.addListener('Option:enablePmAlerts', onAlertToggle);
	ui_data._createNewspaperVars();
	ui_data._getNewspaper();
	GUIp.common.setInterval(ui_data._getNewspaper, 5*60*1000);
};
ui_data._initVariables = function() {
	this.currentVersion = '@VERSION@';
	this.docTitle = document.title;
	this.isFight = ui_stats.isFight();
	this.isDungeon = ui_stats.isDungeon();
	this.isSail = ui_stats.isSail();
	this.isMining = ui_stats.isMining();
	this.isBoss = ui_stats.fightType() === 'monster';
	this.logId = ui_stats.logId();
	this.god_name = ui_stats.godName();
	// from now on, ui_storage is fully functional
	this.char_name = ui_stats.charName();
	this.char_sex = ui_stats.isMale() ? worker.GUIp_i18n.hero : worker.GUIp_i18n.heroine;
	GUIp.common.setCurrentGodname(this.god_name);
	ui_storage.set('charName', this.char_name);
	ui_storage.set('charIsMale', ui_stats.isMale());
	if (ui_stats.checkShop()) {
		ui_storage.set('charHasShop',true);
		this.hasShop = true;
	} else {
		this.hasShop = ui_storage.getFlag('charHasShop') || false;
	}
	this.inShop = false;
	this.storedPets = JSON.parse(ui_storage.get('charStoredPets')) || [];
	if (!ui_storage.get('Option:activeInformers')) {
		// default preset of informers
		var informersPreset = {
			full_godpower:48, much_gold:48, dead:48, low_health:48, fight:48, selected_town:48, wanted_monster:48, special_monster:16, tamable_monster:112, chosen_monster:48,pet_knocked_out:48, close_to_boss:48, close_to_rival:48, guild_quest:16, custom_informers:48,
			treasure_box:48, charge_box:48, gift_box:48, good_box:48
		};
		if (!ui_stats.hasTemple()) {
			informersPreset['smelter'] = informersPreset['smelt!'] = informersPreset['transformer'] = informersPreset['transform!'] = 48;
		}
		ui_storage.set('Option:activeInformers',JSON.stringify(informersPreset));
		ui_storage.set('Option:disableDieButton',true);
		ui_storage.set('Option:useShortPhrases',true);
		ui_storage.set('Option:enableInformerAlerts',true);
		ui_storage.set('Option:enablePmAlerts',true);
	}
	if (!ui_storage.get('ForumSubscriptions')) {
		ui_storage.set('ForumSubscriptions', '{}');
		ui_storage.set('ForumInformers', '{}');
	}
	document.body.classList.add(
		'superhero',
		this.isDungeon ? 'dungeon' : this.isSail ? 'sail' : this.isMining ? 'mining' : this.isFight ? 'fight' : 'field'
	);
	if (ui_stats.hasTemple()) {
		document.body.classList.add('has_temple');
	}
	if (this.isFight) {
		var abilities = ui_stats.Enemy_AbilitiesText().toLowerCase();
		this._parseBossAbs(abilities);
		if (/mutating|мутирующий/i.test(abilities)) {
			GUIp.common.newMutationObserver(this._parseBossAbs.bind(this, null)).observe(document.querySelector('#o_hk_gold_we + .line:not(#o_hk_death_count) .l_val'),{childList: true, subtree: true});
		}
	} else {
		this.lastFieldInit = ui_storage.set_with_diff('lastFieldInit',Date.now());
	}
	ui_utils.voiceInput = document.getElementById('god_phrase') || document.getElementById('godvoice');
};
ui_data._parseBossAbs = function(abilities) {
	abilities = abilities || ui_stats.Enemy_AbilitiesText().toLowerCase();
	var abilitiesList = {'золотоносный':'auriferous','глушащий':'deafening','лучезарный':'enlightened','взрывной':'explosive','неверующий':'faithless','мощный':'hulking','паразитирующий':'leeching','бойкий':'nimble','ушастый':'overhearing','тащущий':'pickpocketing','спешащий':'scurrying','творящий':'skilled','драпающий':'sneaky','транжирящий':'squandering','зовущий':'summoning','пробивающий':'sweeping','мутирующий':'mutating'},
		abilitiesVals = Object.values(abilitiesList);
	Array.from(document.body.classList).forEach(function(a) {
		if (a.startsWith('boss_')) {
			document.body.classList.remove(a);
		}
	});
	abilities.split(',').forEach(function(a) {
		a = a.trim();
		if (abilitiesList[a] || abilitiesVals.includes(a)) {
			document.body.classList.add('boss_' + (abilitiesList[a] || a));
		}
	});
};
ui_data._createNewspaperVars = function() {
	var updateNewspaperSummary = function() {
		ui_improver.showDailyForecast();
		ui_informer.updateCustomInformers();
	};
	ui_improver.wantedItems = ui_storage.createVar('Newspaper:wantedItems',
		function(text) { return text ? new RegExp('^(?:' + text + ')$', 'i') : null; },
		function(regex) { return regex ? regex.source.slice(4, -2) : ''; },
		ui_inventory.tryUpdate
	);
	ui_improver.wantedMonsterRewards = ui_storage.createVar('Newspaper:wantedMonsterRewards',
		function(text) { return JSON.parse(text || '{}'); },
		JSON.stringify
	);
	ui_improver.dailyForecast = ui_storage.createVar('Newspaper:dailyForecast', ui_utils.splitList);
	ui_improver.dailyForecastText = ui_storage.createVar('Newspaper:dailyForecastText',
		function(text) { return text || ''; },
		null,
		updateNewspaperSummary
	);
	ui_improver.bingoTries = ui_storage.createVar('Newspaper:bingoTries',
		function(text) { return +text || 0; },
		null,
		updateNewspaperSummary
	);
	ui_improver.bingoItems = ui_storage.createVar('Newspaper:bingoItems',
		function(text) { return text ? new RegExp(text, 'i') : null; },
		function(regex) { return regex ? regex.source : ''; },
		ui_inventory.tryUpdate
	);
	ui_storage.addListener('Newspaper:couponPrize', updateNewspaperSummary);
	ui_storage.addListener('Newspaper:godpowerCap', updateNewspaperSummary);
};
ui_data._getNewspaper = function(forced) {
	var date = +ui_storage.get('Newspaper:date');
	if (forced || date !== date || ui_utils.dateToMoscowTimeZone(date) < ui_utils.dateToMoscowTimeZone(Date.now())) {
		if (!forced) {
			ui_improver.wantedItems.setTemp(null);
			ui_improver.wantedMonsterRewards.setTemp({});
			ui_improver.dailyForecast.setTemp([]);
			ui_improver.dailyForecastText.setTemp('');
			ui_improver.bingoTries.setTemp(0);
			ui_improver.bingoItems.setTemp(null);
		}
		GUIp.common.getDomainXHR('/news', ui_data._parseNewspaper);
	} else {
		ui_improver.showDailyForecast();
	}
};
ui_data._parseNewspaper = function(xhr) {
	var newsDate = 0,
		mRewards = {},
		bingoItems = [],
		bingoRE = /<td><span>(.*?)<\/span><\/td>/g,
		text = '',
		temp;
	if (temp = xhr.responseText.match(/<div id="date"[^>]*>[^<]*<span>(\d+)<\/span>/)) {
		newsDate = temp[1] * 86400000 + ((worker.GUIp_locale === 'ru') ? 1195560000000 : 1273492800000);
	} else {
		newsDate = Date.now();
	}
	ui_storage.set('Newspaper:date', newsDate);
	temp = xhr.responseText.match(/(?:Куплю-продам|Buy and Sell)[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>/);
	ui_improver.wantedItems.set(temp && new RegExp('^(?:' + temp[1] + '|' + temp[2] + ')$', 'i'));
	temp = xhr.responseText.match(/(?:Разыскиваются|Wanted)<\/h2>[^]+?<p>([^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?)<\/p>[^]+?<p>([^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?)<\/p>/);
	if (temp) {
		mRewards[temp[2].toLowerCase()] = temp[1].replace(/<a[^]+?<\/a>/g,temp[2]);
		mRewards[temp[4].toLowerCase()] = temp[3].replace(/<a[^]+?<\/a>/g,temp[4]);
	}
	ui_improver.wantedMonsterRewards.set(mRewards);
	temp = xhr.responseText.match(/(?:Астропрогноз|Daily Forecast)[^]+?<p>([^<]+?)<\/p>(?:[^<]+?<p>([^<]+?)<\/p>)?/);
	if (temp) {
		ui_improver.dailyForecastText.set(temp[1] ? (temp[1] + (temp[2] ? '\n' + temp[2] : '')).replace(/&#0149;/g, '•') : '');
		ui_improver.dailyForecast.set(GUIp.common.parseForecasts(ui_improver.dailyForecastText.get()));
	} else {
		ui_improver.dailyForecastText.set('');
		ui_improver.dailyForecast.set([]);
	}
	while ((temp = bingoRE.exec(xhr.responseText))) {
		bingoItems.push(temp[1]);
	}
	temp = /<span\s[^<>]*?\bid\s*=\s*["']?b_cnt\b[^<>]*>(.*?)<\//.exec(xhr.responseText); // span#b_cnt
	ui_improver.bingoTries.set((temp && parseInt(temp[1])) || 0);
	ui_improver.bingoItems.set(bingoItems.length ? new RegExp(bingoItems.join('|'), 'i') : null);
	if ((temp = /<br\s*\/?>([^<>]*)<br\s*\/?>\s*<input(\s[^<>]*?\bid\s*=\s*["']?coupon_b\b[^<>]*)/.exec(xhr.responseText))) { // input#coupon_b
		text = /\sdisabled\b/.test(temp[2]) ? '' : temp[1].trim().replace(/&#39;/g, "'");
		ui_storage.set('Newspaper:couponPrize:raw', text);
		ui_storage.set('Newspaper:couponPrize', text && text.replace(/^(?:an?|the|some) /, ''));
	}
	ui_storage.set('Newspaper:godpowerCap', /\bgp_cap_avail\s*=\s*true\b/.test(xhr.responseText));
	ui_improver.showDailyForecast();
	if (document.querySelector('#inventory li.improved')) {
		ui_inventory._update();
	}
};
