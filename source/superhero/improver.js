// ui_improver
var ui_improver = worker.GUIp.improver = {};

ui_improver.isFirstTime = true;
ui_improver.voiceSubmitted = false;
ui_improver.clockToggling = false;
ui_improver.clock = null;
ui_improver.wantedItems = null;
ui_improver.wantedMonsterRewards = null;
ui_improver.dailyForecast = null;
ui_improver.dailyForecastText = null;
ui_improver.bingoTries = null;
ui_improver.bingoItems = null;
ui_improver.improvementDebounce = null;
// dungeon & sailing
ui_improver.chronicles = {};
ui_improver.directionlessMoves = null;
ui_improver.wormholeMoves = null;
ui_improver.dungeonGuidedSteps = null;
ui_improver.alliesHP = {};
ui_improver.corrections = {n: 'north', e: 'east', s: 'south', w: 'west'};
ui_improver.dungeonXHRCount = 0;
ui_improver.dungeonExtras = {};
ui_improver.needLog = true;
// resresher
ui_improver.softRefreshInt = 0;
ui_improver.hardRefreshInt = 0;

ui_improver.init = function() {
	ui_improver.improvementDebounce = GUIp.common.debounce(250, function improvementDebounce() {
		ui_improver.improve();
		if (ui_data.isFight) {
			ui_logger.update();
		}
	});
};
ui_improver.softRefresh = function() {
	GUIp.common.info('soft reloading...');
	document.getElementById('d_refresh') && document.getElementById('d_refresh').click();
};
ui_improver.hardRefresh = function() {
	GUIp.common.warn('hard reloading...');
	location.reload();
};
ui_improver.improve = function() {
	this.improveInProcess = true;

	if (this.isFirstTime) {
		if (GUIp_browser === 'Opera') {
			document.body.classList.add('e_opera');
		}
		if (!ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail) {
			ui_improver.improveDiary();
			ui_improver.distanceInformerInit();
			ui_improver.improveShop();
		}
		if (ui_data.isDungeon) {
			ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content'));
			var heroNames = ui_stats.Hero_Ally_Names();
			heroNames.push(ui_data.char_name);
			GUIp.common.setExtraDiscardData(heroNames);
			this.dungeonExtras = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':extras') || '{}'); // even after this we need to reparse again in case we missed something
			this.parseDungeonExtras(document.querySelectorAll('#m_fight_log .d_imp .d_msg'), document.getElementById('map'), ui_stats.currentStep());
			GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
			GUIp.common.dmapExclCache = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':excl') || '{}');
		}
		if (ui_data.isSail) {
			var mapSettings = ui_storage.getList('Option:islandsMapSettings');
			if (mapSettings.includes('widen')) {
				ui_improver.sailPageResize = true;
				ui_improver.whenWindowResize();
			}
			ui_improver.initSailing();
		}
		if (ui_data.isMining) {
			ui_mining.processMap();
		}
		if (ui_data.isFight) {
			GUIp.common.cleanupLogStorage();
			GUIp.common.setTimeout(ui_improver.improvePlayers, 250);
			if (ui_improver.alliesHP.sum === undefined) {
				try {
					if (!ui_storage.get('Log:' + ui_data.logId + ':allhp')) {
						ui_improver.alliesHP = {sum: 0};
						for (var i = 1; i <= 5; i++) {
							ui_improver.alliesHP[i] = ui_stats.Hero_Ally_MaxHP(i);
							if (ui_stats.Hero_Ally_Name(i)[0] !== '+') {
								ui_improver.alliesHP.sum += ui_improver.alliesHP[i];
							}
						}
						ui_storage.set('Log:' + ui_data.logId + ':allhp', JSON.stringify(ui_improver.alliesHP));
					} else {
						ui_improver.alliesHP = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':allhp'));
					}
				} catch (e) {}
			}
			ui_improver.addLastFightLink();
		}
		ui_improver.initTouchDragging();
	}
	ui_improver.improveStats();
	ui_improver.improveVoiceDialog();
	if (!ui_data.isFight) {
		ui_improver.improveNews();
		ui_improver.improveEquip();
		ui_improver.improveSkills();
		ui_improver.improvePet();
		ui_improver.improveSendButtons();
		ui_improver.detectors.detectField();
		if (ui_data.hasShop) {
			ui_improver.detectors.detectLS();
		}
	} else {
		if (ui_improver.isFirstTime) {
			ui_logger.update();
		} else {
			// Godville makes a tiny delay before assigning inline styles to players. we have to wait
			// until they're completed.
			ui_improver.improvePlayers();
		}
		ui_improver.improveHP();
		ui_improver.improveColumnWithMap();
	}
	if (ui_data.isDungeon) {
		ui_improver.improveDungeon();
	}
	ui_improver.improveInterface();
	ui_improver.improveChat();
	ui_improver.calculateButtonsVisibility();
	this.isFirstTime = false;
	this.improveInProcess = false;

	ui_informer.update('fight', ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail);
	ui_informer.update('arena available', ui_stats.isArenaAvailable());
	ui_informer.update('dungeon available', ui_stats.isDungeonAvailable());
	ui_informer.update('sail available', ui_stats.isSailAvailable());

	ui_informer.updateCustomInformers();
};
ui_improver.improveVoiceDialog = function() {
	var map;
	// If playing in pure ZPG mode there won't be present voice input block at all;
	if (!document.getElementById('ve_wrap')) {
		return;
	}
	// Add voicegens and show timeout bar after saying
	if (this.isFirstTime) {
		this.freezeVoiceButton = ui_storage.createVar('Option:freezeVoiceButton', function(text) { return text || ''; });
		ui_utils.updateVoiceSubmitState();

		var voiceSubmit = document.getElementById('voice_submit');
		document.getElementById('ve_wrap').insertAdjacentHTML('afterbegin',
			'<div id="clear_voice_input" class="div_link_nu gvl_popover hidden" title="' + worker.GUIp_i18n.clear_voice_input + '">×</div>'
		);
		GUIp.common.addListener(document.getElementById('clear_voice_input'), 'click', function() {
			ui_utils.setVoice('');
		});
		worker.$(document).on('change keypress paste focus textInput input', '#godvoice, #god_phrase', function() {
			GUIp.common.try2(function(target) {
				ui_utils.updateVoiceSubmitState();
				ui_utils.hideElem(document.getElementById('clear_voice_input'), !target.value);
			}, this);
		});
		GUIp.common.addListener(document.getElementById('ve_wrap'), 'keypress', function(e) {
			if (e.which !== 13) return;
			if (voiceSubmit.disabled) {
				e.preventDefault();
				e.stopPropagation();
			} else {
				ui_improver.voiceSubmitted = true;
			}
		}, true);
		GUIp.common.addListener(voiceSubmit, 'click', function() {
			ui_improver.voiceSubmitted = true;
			voiceSubmit.blur();
		});
		// prevent Godville from re-enabling the button when we want to keep it disabled
		GUIp.common.newMutationObserver(ui_utils.updateVoiceSubmitState.bind(ui_utils))
			.observe(voiceSubmit, {attributes: true, attributeFilter: ['disabled']});

		if (!ui_utils.isAlreadyImproved(document.getElementById('cntrl'))) {
			var gp_label = document.getElementsByClassName('gp_label')[0];
			gp_label.classList.add('l_capt');
			document.getElementsByClassName('gp_val')[0].classList.add('l_val');
			if (ui_words.base.phrases.mnemonics.length) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.mnemo_button, 'mnemonics', worker.GUIp_i18n.mnemo_title);
			}
			if (ui_data.isDungeon) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.upstairs, 'go_upstairs', worker.GUIp_i18n.fmt('ask_to_go_upstairs', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.downstairs, 'go_downstairs', worker.GUIp_i18n.fmt('ask_to_go_downstairs', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.east, 'go_east', worker.GUIp_i18n.fmt('ask_to_go_east', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.west, 'go_west', worker.GUIp_i18n.fmt('ask_to_go_west', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.south, 'go_south', worker.GUIp_i18n.fmt('ask_to_go_south', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.north, 'go_north', worker.GUIp_i18n.fmt('ask_to_go_north', ui_data.char_sex[0]));
				if ((map = document.getElementById('map'))) {
					if (GUIp.common.isAndroid) {
						GUIp.common.addListener(map, 'click', ui_utils.mapVoicegen);
					}
					GUIp.common.tooltips.watchSubtree(map.getElementsByClassName('block_content')[0]);
				}
			} else if (!ui_data.isSail) {
				if (ui_data.isFight) {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.defend, 'defend', worker.GUIp_i18n.fmt('ask_to_defend', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
				} else {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.sacrifice, 'sacrifice', worker.GUIp_i18n.fmt('ask_to_sacrifice', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
				}
			}
		}
	}
	//hide_charge_button
	var charge_button = document.querySelector('#cntrl .hch_link');
	if (charge_button) {
		charge_button.style.visibility = ui_storage.getFlag('Option:hideChargeButton') ? 'hidden' : '';
	}
	ui_informer.update('full godpower', ui_stats.Godpower() === ui_stats.Max_Godpower() && !ui_data.isFight);
};
ui_improver._formatPetFeature = function(feature) {
	return GUIp_i18n['pet_feature_' + feature];
};
ui_improver._formatMonsterTitle = function(monster, isTamable) {
	var result = ui_improver.wantedMonsterRewards.get()[monster] || '',
		pet;
	if (isTamable && (pet = ui_words.base.pets[monster])) {
		if (result) result += '\n\n';
		result += GUIp_i18n.fmt('tamable_monster', Array.from(pet.features, ui_improver._formatPetFeature).join(', '));
	}
	return result;
};
ui_improver._improveMonsterNameHTML = function(html, putPetLink) {
	// must not be called twice
	return html.replace(/\uD83E\uDDB4 /g, // bone
		putPetLink ? (
			// we use `white-space: nowrap` instead of `&nbsp;` since the latter might cause unexpected results
			// in custom informers (`gv.lastDiary`, etc.) and user scripts
			'<span class="e_nowrap"><a class="div_link_nu e_emoji e_emoji_bone eguip_font" href="' +
				GUIp_i18n.wiki_pets_table +
			'" title="' + GUIp_i18n.open_wiki_pets_table +
			'" target="_blank">\uD83E\uDDB4</a> </span>'
		) : '<span class="e_nowrap"><span class="e_emoji e_emoji_bone eguip_font">\uD83E\uDDB4</span> </span>'
	).replace(/☠ /g,
		'<span class="e_nowrap"><span class="e_emoji e_emoji_skull' +
			(GUIp.common.renderTester.testChar('☠') ? '' : ' eguip_font') +
		'">☠</span> </span>'
	);
};
ui_improver.improveMonsterName = function() {
	var div = document.querySelector('#news .line .l_val'),
		title = '';
	if (div && !div.getElementsByClassName('improved')[0]) {
		title = div.textContent;
		if ((title = ui_improver._formatMonsterTitle(
			ui_stats.canonicalMonsterName(title).toLowerCase(), title.includes('\uD83E\uDDB4') // bone
		))) {
			GUIp.common.tooltips.watchSubtree(div);
		}
		div.innerHTML = '<span class="improved' + (title && '" title="' + title) + '">' +
			ui_improver._improveMonsterNameHTML(div.innerHTML, true) +
		'</span>';
	}
	div = document.getElementsByClassName('f_news')[0];
	if (div && !div.getElementsByClassName('improved')[0]) {
		div.innerHTML = '<span class="improved">' +
			ui_improver._improveMonsterNameHTML(div.innerHTML, false) +
		'</span>';
	}
};
ui_improver.improveNews = function() {
	var news = document.getElementById('news');
	if (!ui_utils.isAlreadyImproved(news)) {
		ui_utils.addVoicegen(news.getElementsByClassName('l_capt')[0], worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
		Array.prototype.forEach.call(news.getElementsByClassName('p_bar'), GUIp.common.tooltips.watchSubtree);
	}
	ui_improver.improveMonsterName();
	ui_informer.update('wanted monster', ui_stats.wantedMonster());
	ui_informer.update('special monster', ui_stats.specialMonster());
	ui_informer.update('tamable monster', ui_stats.tamableMonster());
	ui_informer.update('chosen monster', ui_stats.chosenMonster());
};
ui_improver.improveColumnWithMap = function() {
	if (ui_storage.getFlag('Option:disableMapColumnImprover')) {
		return;
	}
	var oldColumn = document.getElementsByClassName('e_map_column')[0],
		node = document.getElementById('s_map') || document.getElementById('r_map') || document.getElementById('map'),
		pageClasses;
	for (; node; node = node.parentElement) {
		if (node.classList.contains('group_wrapper')) {
			if (node === oldColumn) return; // no changes
			node.classList.add('e_map_column');
			pageClasses = document.getElementById('main_wrapper').classList;
			if (node.classList.contains('c_col')) {
				pageClasses.add('e_map_in_c_col');
				pageClasses.remove('e_map_in_s_col');
			} else {
				pageClasses.add('e_map_in_s_col');
				pageClasses.remove('e_map_in_c_col');
			}
			break;
		}
	}
	if (oldColumn) {
		oldColumn.classList.remove('e_map_column');
	}
};
ui_improver.improveDungeon = function() {
	if (this.isFirstTime) {
		ui_storage.addListener('Option:dungeonMapSettings', function(newValue) {
			ui_utils.hideElem(document.querySelector('.dmapDimensions'), !(newValue || '').includes('dims'));
		});
	}
	this.updateDungeonVoicegen(); // todo: place this into dungeon colorization? also run this from here but only at first time?
};
ui_improver.updateDungeonVoicegen = function() {
	if (document.querySelectorAll('#map .dml').length) {
		var i, j, chronolen = +worker.Object.keys(this.chronicles).reverse()[0],
			$box = worker.$('#cntrl .voice_generator'),
			$boxML = worker.$('#map .dml'),
			$boxMC = worker.$('#map .dmc'),
			kRow = $boxML.length,
			kColumn = $boxML[0].textContent.length,
			isJumping = this.dungeonExtras.type === 'jumping';
		if (!$box.length) {
			return;
		}
		for (i = 0; i < 6; i++) {
			if (i < 4) {
				$box[i].style.visibility = 'hidden';
			} else {
				$box[i].style.display = 'none';
			}
		}
		var isCellar = GUIp.common.isDungeonCellar();
		for (var si = 0; si < kRow; si++) {
			for (var sj = 0; sj < kColumn; sj++) {
				if ($boxMC[si * kColumn + sj].textContent !== '@') {
					continue;
				}
				var isMoveLoss = [];
				for (i = 0; i < 4; i++) {
					isMoveLoss[i] = this.chronicles[chronolen - i] && this.chronicles[chronolen - i].marks.includes('trapMoveLoss');
				}
				var directionsShouldBeShown = !isMoveLoss[0] || (isMoveLoss[1] && (!isMoveLoss[2] || isMoveLoss[3]));
				if (directionsShouldBeShown) {
					if ($boxMC[(si - 1) * kColumn + sj].textContent !== '#' || isJumping && (si === 1 || si !== 1 && $boxMC[(si - 2) * kColumn + sj].textContent !== '#')) {
						$box[0].style.visibility = ''; // Север
					}
					if ($boxMC[(si + 1) *kColumn + sj].textContent !== '#' || isJumping && (si === kRow - 2 || si !== kRow - 2 && $boxMC[(si + 2) *kColumn + sj].textContent !== '#')) {
						$box[1].style.visibility = ''; // Юг
					}
					if ($boxMC[si * kColumn + sj - 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj - 2].textContent !== '#') {
						$box[2].style.visibility = ''; // Запад
					}
					if ($boxMC[si * kColumn + sj + 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj + 2].textContent !== '#') {
						$box[3].style.visibility = ''; // Восток
					}
					if (this.chronicles[chronolen] && (this.chronicles[chronolen].marks.includes('staircaseHint') || this.chronicles[chronolen].marks.includes('staircase'))) {
						if (!isCellar) {
							$box[4].style.display = ''; // Вниз по лестнице
						} else {
							$box[5].style.display = ''; // Вверх по лестнице
						}
					}
					if (isJumping && ui_storage.getFlag('Option:jumpingOverrideDirections')) {
						for (i = 0; i < 4; i++) {
							$box[i].style.visibility = '';
						}
					}
				}
			}
		}
	}
};
ui_improver._toFixedPoint = function(m0) {
	return (+m0).toFixed(1);
};
ui_improver.improveStats = function() {
	var i, brNode, handler;
	if ((brNode = document.querySelector('#hk_bricks_cnt .l_val'))) {
		brNode.textContent = brNode.textContent.replace(/[\d.]+/, this._toFixedPoint);
	}
	if (ui_improver.isFirstTime) {
		Array.prototype.forEach.call(
			document.querySelectorAll('#stats .p_bar, #o_hl1 .p_bar, #m_info .p_bar, #b_info .p_bar'),
			GUIp.common.tooltips.watchSubtree
		);
	}
	if (ui_data.isDungeon) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Dungeon');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Exp', ui_stats.Exp());
			ui_storage.set('Logger:Map_Level', ui_stats.Level());
			ui_storage.set('Logger:Map_Gold', ui_storage.get('Logger:Gold'));
			ui_storage.set('Logger:Map_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Alls_HP', ui_stats.Map_Alls_HP());
			for (i = 1; i <= 5; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('dungeon'));
		return;
	}
	if (ui_data.isSail) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Sail');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Supplies',ui_stats.Map_Supplies());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_Name', '');
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('sail'));
		return;
	}
	if (ui_data.isMining) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Mining');
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Bits',ui_stats.Bits());
			ui_storage.set('Logger:Bytes',ui_stats.Bytes());
			ui_storage.set('Logger:Push_Readiness',ui_stats.Push_Readiness());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
		}
		return;
	}
	if (ui_data.isFight) {
		if (this.isFirstTime) {
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
			ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Hero_Charges', ui_stats.Charges());
			ui_storage.set('Logger:Enemy_HP', ui_stats.Enemy_HP());
			ui_storage.set('Logger:Enemy_Gold', ui_stats.Enemy_Gold());
			ui_storage.set('Logger:Enemy_Inv', ui_stats.Enemy_Inv());
			ui_storage.set('Logger:Hero_Alls_HP', ui_stats.Hero_Alls_HP());
			for (i = 1; i <= 11; i++) {
				ui_storage.set('Logger:Hero_Ally'+i+'_HP', ui_stats.Hero_Ally_HP(i));
			}
			for (i = 1; i <= 6; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
			ui_storage.set('Logger:Enemy_AliveCount', ui_stats.Enemy_AliveCount());
		}
		ui_informer.update('low health', ui_stats.lowHealth('fight'));
		return;
	}
	if (ui_data.hasShop) {
		if (ui_stats.inShop()) {
			if (ui_storage.get('Logger:Location') === 'Field') {
				ui_storage.set('Logger:Location', 'Store');
				ui_logger.suppressOldStats();
				ui_logger.update(ui_logger.fieldWatchers);
				ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
				ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			}
			ui_data.inShop = true;
			document.body.classList.add('in_own_shop');
		} else if (ui_data.inShop) {
			ui_logger.update(ui_logger.shopWatchers);
			ui_data.inShop = false;
			document.body.classList.remove('in_own_shop');
		}
	}
	if (!ui_data.inShop && ui_storage.get('Logger:Location') !== 'Field') {
		ui_storage.set('Logger:Location', 'Field');
	}
	if (!ui_utils.isAlreadyImproved(document.getElementById('stats'))) {
		// Add voicegens
		ui_utils.addVoicegen(document.querySelector('#hk_level .l_capt'), worker.GUIp_i18n.study, 'exp', worker.GUIp_i18n.fmt('ask_to_study', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_health .l_capt'), worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_gold_we .l_capt'), worker.GUIp_i18n.dig, 'dig', worker.GUIp_i18n.fmt('ask_to_dig', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.cancel_task, 'cancel_task', worker.GUIp_i18n.fmt('ask_to_cancel_task', ui_data.char_sex[0]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.do_task, 'do_task', worker.GUIp_i18n.fmt('ask_to_do_task', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_death_count .l_capt'), worker.GUIp_i18n.die, 'die', worker.GUIp_i18n.fmt('ask_to_die', ui_data.char_sex[0]));
		handler = ui_improver.calculateButtonsVisibility.bind(ui_improver, true);
		ui_storage.addListener('Option:disableDieButton', handler);
		ui_storage.addListener('Option:disableVoiceGenerators', handler);
	}
	if (!document.querySelector('#hk_distance .voice_generator')) {
		ui_utils.addVoicegen(document.querySelector('#hk_distance .l_capt'), document.querySelector('#main_wrapper.page_wrapper_5c') ? '回' : worker.GUIp_i18n.return, 'town', worker.GUIp_i18n.fmt('ask_to_return', ui_data.char_sex[0]));
	}

	ui_informer.update('much gold', ui_stats.Gold() >= (ui_stats.hasTemple() ? 10000 : 3000));
	ui_informer.update('dead', ui_stats.HP() === 0);
	var questName = ui_stats.Task_Name();
	ui_informer.update('guild quest', questName.match(/членом гильдии|member of the guild/) && !questName.match(/\((отменено|cancelled)\)/));
	ui_informer.update('mini quest', questName.match(/\((мини|mini)\)/) && !questName.match(/\((отменено|cancelled)\)/));

	var townInformer = false;
	if (!ui_stats.isGoingBack() && ui_stats.townName() === '') {
		if (this.informTown === ui_stats.nearbyTown()) {
			townInformer = true;
		}
	} else if (this.informTown === ui_stats.townName()) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	}
	ui_informer.update('selected town', townInformer);
	GUIp.common.tooltips.watchSubtree(document.querySelector('#hk_distance .l_val'));

	// shovel imaging
	var digVoice = document.querySelector('#hk_gold_we .voice_generator');
	if (this.isFirstTime) {
		if (worker.GUIp_browser !== 'Opera') {
			digVoice.style.backgroundImage = 'url(' + worker.GUIp_getResource('images/shovel.png') + ')';
		} else {
			worker.GUIp_getResource('images/shovel.png',digVoice);
		}
	}
	if (ui_stats.goldTextLength() > 16 - 2*document.getElementsByClassName('page_wrapper_5c').length) {
		digVoice.classList.add('shovel');
		digVoice.classList.toggle('compact',
			ui_stats.goldTextLength() > 20 - 3*document.getElementsByClassName('page_wrapper_5c').length
		);
	} else {
		digVoice.classList.remove('shovel');
	}
	// improve title of side quest bar with exact values
	var sjName = ui_stats.Side_Job_Name();
	if (sjName) {
		var sjReq = +(/\d+/.exec(sjName) || '')[0] || (/дважды|twice/.test(sjName) ? 2 : 0),
			sjBar = document.querySelector('#hk_quests_completed + .line .p_bar');
		if (sjBar && sjReq > 0) {
			sjBar.title = sjBar.title.replace(/( \(\d+\/\d+\))/,'') + ' (' + Math.round(ui_stats.Side_Job() / 100 * sjReq) + '/' + sjReq + ')';
		}
	}
};
ui_improver.addLastFightLink = function() {
	var container = document.querySelector('#hk_arena_won .l_val'),
		a, child;
	if (!container || container.classList.contains('div_link') || container.getElementsByTagName('a')[0]) {
		return;
	}
	a = document.createElement('a');
	a.href = '/hero/last_fight';
	a.target = '_blank';
	a.style.display = 'block';
	while ((child = container.firstChild)) {
		a.appendChild(child);
	}
	container.appendChild(a);
};
ui_improver.improveShop = function() {
	var trdNode = document.getElementById('trader'), node;
	if (trdNode && !ui_utils.isAlreadyImproved(trdNode)) {
		var a, p, co = (trdNode.querySelectorAll('.line > a') || [])[2];
		if (co) {
			GUIp.common.addListener(co, 'click', function() {
				a = document.querySelector('#tordr + div');
				if (!a || !a.textContent.length) {
					return;
				}
				if (!a.classList.contains('hidden')) {
					p = a.textContent;
					GUIp.common.setTimeout(function() {
						if (p !== a.textContent) {
							return;
						}
						a.classList.add('hidden');
					},100);
				} else {
					a.classList.remove('hidden');
				}
			});
		}
		if ((node = trdNode.getElementsByClassName('l_slot')[0])) {
			node.insertAdjacentHTML('beforeend', '<div id="trademode_timer" class="fr_new_badge"></div>');
			node = node.lastChild;
			GUIp.common.tooltips.watchSubtree(node);
			ui_utils.hideElem(node, true);
		}
		GUIp.common.tooltips.watchSubtree(trdNode.getElementsByClassName('p_bar')[0]);
	}
};
ui_improver.improvePet = function() {
	var timeRemaining,
		petBadge = document.getElementById('pet_badge'),
		petLevelLabel = document.querySelector('#hk_pet_class + div .l_val'),
		level = ui_stats.Pet_Level(),
		petRes  = (level * 0.4 + 0.5).toFixed(1) + 'k',
		petRes2 = (level * 0.8 + 0.5).toFixed(1) + 'k';
	if (ui_stats.petIsKnockedOut()) {
		if (!ui_utils.isAlreadyImproved(document.getElementById('pet'))) {
			document.querySelector('#pet .r_slot').insertAdjacentHTML('afterbegin', '<div id="pet_badge" class="fr_new_badge e_badge_pos hidden">0</div>');
			petBadge = document.getElementById('pet_badge');
		}
		if (document.querySelector('#pet .block_content').style.display !== 'none') {
			petBadge.title = worker.GUIp_i18n.badge_pet2;
			petBadge.textContent = petRes;
		} else {
			timeRemaining = ui_utils.findLabel(worker.$('#pet'), worker.GUIp_i18n.pet_status_label).siblings('.l_val').text().match(/(?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/) || '';
			petBadge.title = worker.GUIp_i18n.badge_pet1;
			petBadge.textContent = ('00' + (timeRemaining[1] || '')).substr(-2) + ':' + ('00' + (timeRemaining[2] || '')).substr(-2)
		}
		ui_utils.hideElem(petBadge, false);
	} else if (petBadge) {
		ui_utils.hideElem(petBadge, true);
	}
	if (petLevelLabel) {
		petLevelLabel.title = '↑ ' + petRes + (worker.GUIp_locale === 'ru' ? '\n⇈ ' + petRes2 : '');
		GUIp.common.tooltips.watchSubtree(petLevelLabel);
	}
	// knock out informer
	ui_informer.update('pet knocked out', ui_stats.petIsKnockedOut());
};
ui_improver.describeEquipBoldness = function(mutations, observer) {
	var names = document.querySelectorAll('#equipment .eq_name'),
		name, eq;
	for (var i = 0, len = names.length; i < len; i++) {
		name = names[i];
		eq = name.parentNode;
		if (name.classList.contains('eq_b')) {
			eq.classList.add('e_eq_bold');
			name.title = GUIp_i18n.equip_boldness[i] ? eq.firstElementChild.textContent + ': ' + GUIp_i18n.equip_boldness[i] : '';
		} else {
			eq.classList.remove('e_eq_bold');
			name.title = '';
		}
	}
	observer.takeRecords();
};
ui_improver.improveEquip = function() {
	var observer;
	if (!ui_utils.isAlreadyImproved(document.getElementById('equipment'))) {
		document.querySelector('#equipment .r_slot').insertAdjacentHTML('afterbegin', '<div id="equip_badge" class="fr_new_badge e_badge_pos">0</div>');
		observer = GUIp.common.newMutationObserver(ui_improver.describeEquipBoldness);
		observer.observe(document.getElementById('equipment'), {subtree: true, attributes: true, attributeFilter: ['class']});
		ui_improver.describeEquipBoldness(null, observer);
	}
	var equipBadge = document.getElementById('equip_badge'),
		averageEquipLevel = 0;
	for (var i = 1; i <= 7; i++) {
		averageEquipLevel += ui_stats['Equip' + i]();
	}
	averageEquipLevel = averageEquipLevel / 7 - ui_stats.Level();
	averageEquipLevel = (averageEquipLevel >= 0 ? '+' : '') + averageEquipLevel.toFixed(1);
	if (equipBadge.textContent !== averageEquipLevel) {
		equipBadge.title = worker.GUIp_i18n.badge_equip;
		equipBadge.textContent = averageEquipLevel;
	}
};
ui_improver.improveSkills = function() {
	var block = document.getElementById('skills');
	if (!ui_utils.isAlreadyImproved(block)) {
		block.getElementsByClassName('r_slot')[0].insertAdjacentHTML('afterbegin',
			'<div id="skill_badge" class="fr_new_badge e_badge_pos"></div>'
		);
	}
	var skillBadge = document.getElementById('skill_badge'),
		skillList = block.getElementsByClassName('skill_info'),
		minSkillLevel = Infinity,
		minSkillPrice = '',
		skill, m, level, price;
	for (var i = 0, len = skillList.length; i < len; i++) {
		skill = skillList[i];
		if (!(m = /\d+/.exec(skill.textContent))) {
			continue;
		}
		level = +m[0];
		price = ((level + 1) / 2).toFixed(1) + 'k';
		skill.title = '↑ ' + price;
		GUIp.common.tooltips.watchSubtree(skill);
		if (level < minSkillLevel) {
			minSkillLevel = level;
			minSkillPrice = price;
		}
	}
	if (minSkillPrice && skillBadge.textContent !== minSkillPrice) {
		skillBadge.title = worker.GUIp_i18n.badge_skill;
		skillBadge.textContent = minSkillPrice;
	}
	ui_utils.hideElem(skillBadge, !minSkillPrice);
};
ui_improver.improvePlayers = function() {
	var nodes = document.querySelectorAll('#alls .opp_n.opp_ng span, #bosses .opp_n.opp_ng span, #o_hk_godname .l_val a');
	GUIp.common.markBlacklistedPlayers(nodes, ui_words.allyBlacklist);
	Array.prototype.forEach.call(nodes, GUIp.common.tooltips.watchSubtree);
};
ui_improver.improveHP = function() {
	var hp, hps = document.querySelectorAll('#alls .opp_h, #opps .opp_h, #bosses .opp_h');
	for (var i = 0, len = hps.length; i < len; i++) {
		if ((hp = /^(\d+) \/ (\d+)$/.exec(hps[i].textContent))) {
			hps[i].title = +(+hp[1] / +hp[2] * 100).toFixed(2) + '%';
		} else {
			hps[i].title = '';
		}
		GUIp.common.tooltips.watchSubtree(hps[i]);
	}
};
ui_improver.improveSendButtons = function(forcedUpdate) {
	var sendToButtons, pants = document.querySelector('#pantheons .block_content'),
		curGP = ui_stats.Godpower();
	if (this.isFirstTime) {
		sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
		for (var i = 0, len = sendToButtons.length; i < len; i++) {
			if (sendToButtons[i].textContent.match(/(Послать на тренировку|Spar a Friend)/i)) {
				sendToButtons[i].parentNode.classList.add("e_challenge_button");
			} else if (sendToButtons[i].textContent.match(/(Направить в подземелье|Drop to Dungeon)/i)) {
				sendToButtons[i].parentNode.classList.add("e_dungeon_button");
			} else if (sendToButtons[i].textContent.match(/(Снарядить в плавание|Set Sail)/i)) {
				sendToButtons[i].parentNode.classList.add("e_sail_button");
			} else if (sendToButtons[i].textContent.match(/(Посетить полигон|Explore Datamine)/i)) {
				sendToButtons[i].parentNode.classList.add("e_mining_button");
			}
		}
		if (ui_stats.Level() >= 14) {
			// personal statistics
			pants.insertAdjacentHTML('afterbegin', '<div class="guip p_group_sep"></div>');
			var panthLines = pants.getElementsByClassName('panth_line');
			ui_utils.observeUntil(pants, {childList: true, subtree: true}, function() {
				return panthLines[0];
			}).then(function() {
				pants.insertAdjacentHTML('beforeend', '<div class="guip_stats_lnk p_group_sep"></div>');
				pants.insertAdjacentHTML('beforeend',
					'<div class="guip_stats_lnk"><div class="line"><a class="no_link div_link"' +
					' href="https://stats.' + worker.location.host + '/me" target="_blank"' +
					' title="' + worker.GUIp_i18n.statistics_title + '">' +
						worker.GUIp_i18n.statistics +
					'</a></div></div>'
				);
			}).catch(GUIp.common.onUnhandledException);
		}
		ui_storage.addListener('Option:relocateDuelButtons', ui_improver.improveSendButtons.bind(ui_improver, true));
	}
	sendToButtons = document.querySelectorAll('a.to_arena');
	for (var lim, i = 0, len = sendToButtons.length; i < len; i++) {
		lim = 50;
		if (!i && ui_improver.dailyForecast.get().includes('arena')) {
			lim = 25;
		}
		sendToButtons[i].classList.toggle('e_low_gp', curGP < lim);
	}
	sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
	var sendToDelay, sendToStr, sendToDesc = document.querySelectorAll('#cntrl2 div.arena_msg, #cntrl2 span.to_arena');
	for (var i = 0, len = sendToDesc.length; i < len; i++) {
		GUIp.common.tooltips.watchSubtree(sendToDesc[i]);
		if (sendToDesc[i].style.display === 'none') {
			continue;
		}
		if ((!sendToDesc[i].title.length || (sendToDesc[i].dataset.expires < Date.now() + 5000)) && (sendToStr = sendToDesc[i].textContent.match(/(Подземелье откроется через|Отплыть можно через|Арена откроется через|Тренировка через|Полигон откроется через|Босс освободится через|Arena available in|Dungeon available in|Sail available in|Sparring available in|Datamine available in|Boss ready in) (?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/))) {
			sendToDelay = ((sendToStr[2] !== undefined ? +sendToStr[2] : 0) * 60 + +sendToStr[3]) * 60;
			sendToStr = sendToStr[1].replace(' через',' в').replace(' in',' at');
			sendToDesc[i].dataset.expires = Date.now() + sendToDelay * 1000;
			sendToDesc[i].title = sendToStr + ' ' + GUIp.common.formatTime(new Date(+sendToDesc[i].dataset.expires),'simpletime');
		}
	}
	if (this.isFirstTime || forcedUpdate) {
		var relocated, buttonInPantheons, relocateDuelButtons = ui_storage.getList('Option:relocateDuelButtons');
		relocated = relocateDuelButtons.includes('arena');
		buttonInPantheons = document.querySelector('#pantheons .arena_link_wrap');
		if (relocated && !buttonInPantheons) {
			pants.insertAdjacentElement('afterbegin', document.getElementsByClassName('arena_link_wrap')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg'));
		}
		relocated = relocateDuelButtons.includes('chf');
		buttonInPantheons = document.querySelector('#pantheons .e_challenge_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_challenge_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg').nextSibling);
		}
		relocated = relocateDuelButtons.includes('dun');
		buttonInPantheons = document.querySelector('#pantheons .e_dungeon_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_dungeon_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[1]);
		}
		relocated = relocateDuelButtons.includes('sail');
		buttonInPantheons = document.querySelector('#pantheons .e_sail_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_sail_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[2]);
		}
		relocated = relocateDuelButtons.includes('min');
		buttonInPantheons = document.querySelector('#pantheons .e_mining_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_mining_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[3]);
		}
	}
};
/**
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_improver.readDiaryOnce = function() {
	var root = document.getElementById('diary'),
		messages = root.querySelectorAll('.d_msg:not(.parsed)'),
		sortButton = root.getElementsByClassName('sort_ch')[0],
		result = [],
		ampm = worker.ampm === '12h',
		today = new Date,
		nearFuture = +today + 300e3, // 5m
		html = '',
		newHTML = '',
		msg, time, m, hours, date, link;
	for (var i = 0, len = messages.length; i < len; i++) {
		msg = messages[i];
		msg.classList.add('parsed');
		time = msg.parentNode.getElementsByClassName('d_time')[0];
		if (!time || !(m = /(\d+):(\d+)/.exec(time.textContent))) {
			GUIp.common.warn('cannot parse diary: no time for "' + msg.textContent + '"');
			continue;
		}
		if (ampm) {
			// there are no AM or PM marks. we have to guess
			hours = +m[1];
			date = today.setHours(hours >= 12 ? hours - 12 : hours, +m[2], 0, 0);
			if (date > nearFuture) {
				date -= 86400e3; // 24h
			}
			if (date + 43200e3 <= nearFuture) { // 12h
				date += 43200e3; // 12h
			}
		} else {
			date = today.setHours(+m[1], +m[2], 0, 0);
			if (date > nearFuture) {
				date -= 86400e3; // 24h
			}
		}
		if (msg.getElementsByClassName('vote_links_b')[0]) {
			result.push({date: date, type: 'foreignVoice', msg: msg.textContent, logID: ''});
		} else {
			html = msg.innerHTML;
			if ((newHTML = ui_improver._improveMonsterNameHTML(html, true)) !== html) {
				msg.innerHTML = newHTML;
			}
			if (msg.classList.contains('m_infl')) {
				result.push({date: date, type: 'influence', msg: msg.textContent, logID: ''});
			} else {
				link = msg.getElementsByTagName('a')[0]; // weird, it has .vote_link class
				result.push({
					date: date,
					type: 'regular',
					msg: msg.textContent,
					logID: link ? link.href.slice(link.href.lastIndexOf('/') + 1) : ''
				});
			}
		}
	}
	if (!sortButton || sortButton.textContent === '▼') {
		// we cannot simply sort the entries by their date, because some of their dates might be identical
		// and we must keep them in the correct order in that case
		result.reverse();
	}
	return result;
};
ui_improver.improveForeignVoices = function() {
	var blocks = document.getElementsByClassName('vote_links_b'),
		links;
	for (var i = 0, len = blocks.length; i < len; i++) {
		links = blocks[i].getElementsByClassName('vote_link');
		GUIp.common.tooltips.watchSubtree(links[2] || links[0]);
	}
};
ui_improver.improveDiary = function() {
	var diary = ui_improver.readDiaryOnce(),
		len = diary.length,
		infl = false,
		reply = false;
	if (len && !ui_improver.isFirstTime && ui_improver.voiceSubmitted) {
		// run voice timeout
		for (var i = 0; i < len; i++) {
			if (diary[i].msg.charCodeAt(0) === 0x27A5) { // ➥
				reply = true;
			} else if (diary[i].type === 'influence') {
				infl = true;
			}
		}
		if (infl) {
			if (reply) {
				ui_timeout.start();
			}
			ui_improver.voiceSubmitted = false;
		}
	}
	ui_timers.updateDiary(diary);
	ui_improver.improveForeignVoices();
};
ui_improver.detectors = {};
ui_improver.detectors.stateLS = {bp: -1, bt: 0, cp: -1, ct: 0, tpp: 0};
ui_improver.detectors.detectLS = function() {
	var tmTimer = document.getElementById('trademode_timer');
	ui_utils.hideElem(tmTimer, !ui_data.inShop);
	if (!ui_data.inShop || !tmTimer) {
		this.stateLS = {bp: -1, bt: 0, cp: -1, ct: 0, tpp: 0};
		return;
	}
	if (!this.stateLS.tpp && ui_data.inShop) {
		tmTimer.title = worker.GUIp_i18n.trademode_timer_wait;
		tmTimer.textContent = '––:––';
	}
	var updateTimer = function() {
		if (this.stateLS.tpp) {
			tmTimer.title = '[' + GUIp.common.formatTime(new Date(+this.stateLS.bt - (this.stateLS.bp - 0.5) * this.stateLS.tpp),'simpletime') + '~' + GUIp.common.formatTime(new Date(+this.stateLS.ct + (100.5 - cp) * this.stateLS.tpp),'simpletime') + ']';
			tmTimer.textContent = GUIp.common.formatTime((+this.stateLS.ct - Date.now() + (100.5 - cp) * this.stateLS.tpp) / 60000,'remaining');
		}
	};
	var cp = ui_stats.sProgress();
	if (this.stateLS.bp < 0) {
		// check cached data if any
		var cache = GUIp.common.parseJSON(ui_storage.get('Cache:stateLS'));
		if (cache && Math.abs(cache.ct + (100.5 - cache.cp) * cache.tpp - Date.now() - (100.5 - cp) * cache.tpp) < 1800e3) {
			this.stateLS = cache;
			updateTimer.call(this);
			return;
		}
		this.stateLS.bp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < 0) {
		this.stateLS.bp = this.stateLS.cp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < cp) {
		this.stateLS.cp = cp;
		this.stateLS.ct = Date.now();
		this.stateLS.tpp = (this.stateLS.ct - this.stateLS.bt)/(this.stateLS.cp - this.stateLS.bp);
		// cache obtained values
		ui_storage.set('Cache:stateLS',JSON.stringify(this.stateLS));
	}
	updateTimer.call(this);
};
ui_improver.detectors.stateTP = {cnt: 0, res: false};
ui_improver.detectors.stateFishing = {pre: false, res: false};
ui_improver.detectors.stateGTF = {cnt: 0, res: false};
ui_improver.detectors.stateGTG = {init: -1, initTown: "", res: false};
ui_improver.detectors.stateField = {inv: 0, gld: 0, dst: 0, intown: false, nbtown: "", task: ""};
Object.defineProperty(ui_improver.detectors, 'stateGB', { get: function() { return this.stateGTG; } });
ui_improver.detectors.detectField = function() {
	var sp = ui_stats.sProgress(),
		inv = ui_stats.Inv(),
		gld = ui_stats.Gold(),
		intown = !!ui_stats.townName(),
		nbtown = ui_stats.nearbyTown(),
		monster = ui_stats.monsterName().replace(/^(Undead|Восставший) /,''),
		milestones = ui_stats.mileStones(),
		fullTask = ui_stats.Task_Name(),
		task = fullTask.replace(/ \((?:выполнено|отменено|эпик|completed|cancelled|epic)\)/g,''),
		taskActive = !/ \((?:выполнено|отменено|completed|cancelled)\)/.test(fullTask);
	// going to capital
	if (ui_stats.isGoingBack()) {
		if (this.stateGTG.init < 0) {
			if (!taskActive || milestones < (GUIp_locale === 'ru' ? 3 : 5) ||
				ui_improver.dailyForecast.get().includes('gvroads') || (this.stateField.nbtown && nbtown !== this.stateField.nbtown && sp <= 85)) {
				this.stateGTG.res = true;
			}
			this.stateGTG.init = this.stateField.dst ? this.stateField.dst : milestones;
			this.stateGTG.initTown = this.stateField.nbtown ? this.stateField.nbtown : nbtown;
		} else if (this.stateGTG.initTown && this.stateGTG.initTown !== nbtown) {
			this.stateGTG.res = true;
		}
	} else if (ui_improver.detectors.stateGTG.init > -1) {
		ui_improver.detectors.stateGTG = {init: -1, initTown: "", res: false};
	}
	// go to fields
	if (monster || intown || milestones < this.stateField.dst || this.stateGTF.res && sp < this.stateField.sp) {
		this.stateGTF.cnt = 0;
		this.stateGTF.res = false;
	} else if (this.stateField.intown && !intown && !ui_stats.lastDiaryIsInfl()) {
		this.stateGTF.res = true;
	} else if (milestones > this.stateField.dst && ui_stats.progressDiff() > 2 && !(/\((мини|mini|гильд|guild)\)/.test(task))) {
		this.stateGTF.cnt++;
		if (this.stateGTF.cnt >= 2) {
			this.stateGTF.res = true;
		}
	}
	// fishing
	if (this.stateFishing.res || this.stateFishing.pre) {
		if (sp < this.stateField.sp || milestones !== this.stateField.dst || inv < this.stateField.inv) {
			this.stateFishing.res = false;
			this.stateFishing.pre = false;
		} else if (this.stateFishing.pre && sp > 0 && sp < 35 && ui_stats.HP() > 0) { // dead people aren't fishing
			if (ui_storage.getFlag('Option:enableDebugMode')) {
				GUIp.common.debug('fishing might be detected (p1)');
				console.log(ui_stats.lastDiary());
			}
			this.stateFishing.res = true;
			this.stateFishing.pre = false;
		}
	} else if (sp < this.stateField.sp && sp < 5 && (this.stateField.inv - inv === 1 || (((new Date()).getMonth() < 2 || (new Date()).getMonth() > 10) && ui_stats.progressDiff() > 1)) && task === this.stateField.task && !(intown || this.stateField.intown || monster || ui_stats.isGoingBack()) && !ui_stats.lastDiaryIsInfl()) {
		if (sp > 0) {
			if (ui_storage.getFlag('Option:enableDebugMode')) {
				GUIp.common.debug('fishing might be detected (p2)');
				console.log(ui_stats.lastDiary());
			}
			this.stateFishing.res = true;
		} else if (ui_stats.lastDiaryIsInfl(1) || !(ui_stats.lastDiaryIsInfl(2) && /(«|“).*?(копай|золот|клад|dig|gold|treasure).*?(»|”)/i.test(ui_stats.lastDiary()[2] || ''))) {
			// ^ the condition above should prevent digging-boss-waiting phase to accidently trigger fishing detector
			if (taskActive) {
				this.stateFishing.pre = true;
			} else {
				if (ui_storage.getFlag('Option:enableDebugMode')) {
					GUIp.common.debug('fishing might be detected (p3)');
					console.log(ui_stats.lastDiary());
				}
				this.stateFishing.res = true;
			}
		}
	}
	// trading process
	if (monster || milestones !== this.stateField.dst || gld < this.stateField.gld || !intown && this.stateField.intown || sp < this.stateField.sp || ui_data.inShop) {
		this.stateTP.cnt = 0;
		this.stateTP.res = false;
	} else if (inv < this.stateField.inv && gld >= this.stateField.gld && sp === 1) {
		if (++this.stateTP.cnt >= 2) {
			this.stateTP.res = true;
		}
	}
	// common
	this.stateField.inv = inv;
	this.stateField.gld = gld;
	this.stateField.dst = milestones;
	this.stateField.intown = intown;
	this.stateField.nbtown = nbtown;
	this.stateField.task = task;
	if (!monster) {
		this.stateField.sp = sp;
	}
};
ui_improver.distanceInformerInit = function() {
	var dstSelected, dstTown, dstContent, dstContentInner,
		dstLine = worker.$('#hk_distance .l_capt'),
		dstSaved = ui_storage.get('townInformer');
	if (dstLine) {
		dstLine.addClass('edst_header');
		dstLine.wup({
			title: worker.GUIp_i18n.town_informer,
			placement: 'bottom',
			width: 320,
			onShow: GUIp.common.try2.bind(null, function onTownTableShow(t) {
				var wup = t[0],
					pos = parseInt(wup.style.left);
				if (pos < 5) {
					wup.style.left = '10px';
					wup.firstElementChild.style.left = (parseInt(wup.firstElementChild.style.left) - (10 - pos)) + 'px';
				}
			}),
			content: GUIp.common.try2.bind(null, function createTownTableContent() {
				dstContent = $('<div></div>');
				dstSelected = worker.$('<div class="edst_headline"></div>');
				GUIp.common.addListener(dstSelected[0], 'click', function(ev) {
					ev.preventDefault();
					ui_improver.distanceInformerReset();
				});
				dstContent.append(dstSelected);
				dstContentInner = $('<div class="edst_towns"></div>');
				var towns, towns_keys, dist, name, desc, townInfo, sh, title = [],
					gvRoads = ui_improver.dailyForecast.get().includes('gvroads'),
					currentTown = ui_stats.townName();
				try {
					towns = JSON.parse(localStorage.town_c);
					towns_keys = Object.keys(towns.t).sort(function(a, b) { return a - b; });
					for (var i = 1, len = towns_keys.length; i < len; i++) {
						dist = +towns_keys[i];
						name = towns.t[dist];
						if (gvRoads && currentTown !== name) {
							continue;
						}
						title.length = 0;
						dstTown = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="l">' + name + '</div><div class="r">(' + dist + (towns_keys[i+1] ? '–' + (+towns_keys[i+1] - 1) : '+') + ')</div>');
						if (currentTown === name) {
							dstTown.addClass('e_current_town');
							title.push(worker.GUIp_i18n.town_current);
						}
						if ((desc = towns.d[name])) {
							title.push(worker.GUIp_i18n.town_banner + desc.replace(/\s*,\s*/, worker.GUIp_i18n.town_as_well_as));
						}
						if ((townInfo = ui_words.base.town_list.find(function(a) { return a.name === name; }))) {
							if (townInfo.wasteFraction &&
								/м(?:ног|ал)о пьют и сберегают|(?:lavish|cheap) parties and (?:good|bad) savings/.test(desc)
							) {
								title.push(GUIp_i18n.fmt('town_waste_fraction', townInfo.wasteFraction));
							}
							if ((townInfo.skillsDiscount || townInfo.skillsMarkup) &&
								/д(?:ешёвы|ороги)е умения|(?:cheap|expensive) skills/.test(desc)
							) {
								title.push(GUIp_i18n.fmt(
									townInfo.skillsDiscount ? 'town_skills_discount' : 'town_skills_markup',
									townInfo.skillsDiscount || townInfo.skillsMarkup
								));
							}
						}
						if ((sh = towns.sh.indexOf(dist)) > -1) {
							dstTown.addClass('e_shifted_town');
							if (towns.t[towns.sh[sh^0x01]]) {
								title.push(worker.GUIp_i18n.fmt('town_shifted', towns.t[towns.sh[sh^0x01]]));
							}
						}
						if (title.length) {
							dstTown.prop('title', title.join('. ') + '.');
						}
						if (!gvRoads) {
							GUIp.common.addListener(dstTown[0], 'click', function(ev) {
								ev.preventDefault();
								ui_improver.distanceInformerSet(this);
							}.bind({name: name}));
						}
						dstTown.appendTo(dstContentInner);
					}
					if (gvRoads) {
						dstTown = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="c">' + worker.GUIp_i18n.town_informer_gvroads + '</div>');
						dstTown.appendTo(dstContentInner);
					}
				} catch (e) {}
				GUIp.common.tooltips.watchSubtree(dstContentInner[0]);
				dstContent.append(dstContentInner);
				ui_improver.distanceInformerUpdate(dstLine,dstSelected);
				dstLine.wup("hide");
				return dstContent;
			})
		});
		if (dstSaved) {
			ui_improver.informTown = dstSaved;
			ui_improver.distanceInformerUpdate(dstLine,dstSelected);
		}
	}
};
ui_improver.distanceInformerUpdate = function(dstLine,dstSelected) {
	dstLine = dstLine || worker.$('#hk_distance .l_capt'),
		dstSelected = dstSelected || worker.$('.edst_headline');
	var town = document.querySelector('.e_selected_town');
	if (town) {
		town.classList.remove('e_selected_town');
	}
	if (ui_improver.informTown) {
		dstSelected.html(worker.GUIp_i18n.town_informer_curtown + ': <strong>' + ui_improver.informTown + '</strong> [x]');
		dstSelected.attr('title',worker.GUIp_i18n.town_informer_reset);
		dstLine.addClass('edst_header_active');
		dstSelected.addClass('edst_headline_active');
		ui_improver.nearbyTownsFix();
	} else {
		dstSelected.text(worker.GUIp_i18n.town_informer_choose);
		dstSelected.attr('title','');
		dstLine.removeClass('edst_header_active');
		dstSelected.removeClass('edst_headline_active');
	}
};
ui_improver.distanceInformerSet = function(town) {
	ui_storage.set('townInformer',town.name);
	ui_improver.informTown = town.name;
	ui_improver.distanceInformerUpdate();
};
ui_improver.distanceInformerReset = function() {
	if (ui_improver.informTown) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	}
};
ui_improver.generateNewspaperSummary = function() {
	var s = GUIp_i18n.daily_forecast + (
		ui_improver.dailyForecast.get().length ? ': [' + ui_improver.dailyForecast.get().join(', ') + ']\n' : ':\n'
	) + ui_improver.dailyForecastText.get();
	var couponPrize = ui_storage.get('Newspaper:couponPrize:raw') || '';
	if (ui_improver.bingoTries.get()) {
		s += GUIp_i18n.fmt('bingo_summary',
			GUIp_i18n.plfmt('bingo_slots_', ui_inventory.getBingoSlots().length),
			GUIp_i18n.plfmt('bingo_clicks_', ui_improver.bingoTries.get())
		);
	}
	if (couponPrize) {
		s += GUIp_i18n.fmt('coupon_available', couponPrize);
	}
	if (ui_storage.getFlag('Newspaper:godpowerCap')) {
		s += GUIp_i18n.godpower_cap_available;
	}
	return s;
};
ui_improver.showDailyForecast = function() {
	var forecastLink = document.getElementById('e_forecast'),
		news = document.getElementById('m_hero').previousElementSibling,
		dfcTimer = 0,
		fr;
	if (forecastLink) {
		if (this.dailyForecastText.get()) {
			forecastLink.title = ui_improver.generateNewspaperSummary();
		} else {
			forecastLink.parentNode.removeChild(forecastLink);
		}
	} else if (news && this.dailyForecastText.get()) {
		forecastLink = document.createElement('a');
		forecastLink.id = 'e_forecast';
		forecastLink.textContent = '❅';
		forecastLink.href = '/news';
		forecastLink.title = ui_improver.generateNewspaperSummary();
		if (GUIp.common.isAndroid) {
			GUIp.common.addListener(forecastLink, 'click', function(ev) {
				ev.preventDefault();
				worker.alert(this.title);
			});
		} else {
			GUIp.common.addListener(forecastLink, 'click', function(ev) { ev.preventDefault(); });
		}
		fr = document.createDocumentFragment();
		fr.appendChild(document.createTextNode(' '))
		fr.appendChild(forecastLink);
		news.insertBefore(fr, news.lastElementChild.nextSibling);
	}
	if (!ui_utils.isAlreadyImproved(news)) {
		GUIp.common.addListener(news, 'mousedown', function(e) {
			if (e.button !== 0) { return; }
			dfcTimer = GUIp.common.setTimeout(function() {
				e.preventDefault();
				ui_data._getNewspaper(true);
				worker.alert(worker.GUIp_i18n.daily_forecast_update_notice);
			},1200);
		});
		GUIp.common.addListener(news, 'mouseup', function(e) {
			if (e.button !== 0) { return; }
			if (dfcTimer) {
				worker.clearTimeout(dfcTimer);
			}
		});
	}
};
ui_improver._onSubCheckboxClick = function() {
	// this: HTMLInputElement
	// see markup below
	var mask = this.dataset.eMask;
	if (!mask) return;
	var subscriptions = GUIp.common.parseJSON(ui_storage.get('ForumSubscriptions')) || {},
		tid = this.parentNode.dataset.eTid,
		sub = subscriptions[tid];
	if (!sub) return;

	if (this.checked) {
		sub.notifications |= mask;
	} else {
		sub.notifications &= ~mask;
	}
	ui_storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
};
ui_improver.showSubsLink = function() {
	var subsLink = document.getElementById('e_subs'),
		forumLink, fr;
	if (!subsLink) {
		forumLink = document.querySelector('#menu_bar a[href="/forums"]');
		if (forumLink) {
			var subs_target = '/forums/show/1/#guip_subscriptions';
			subsLink = document.createElement('a');
			subsLink.id = 'e_subs';
			subsLink.className = 'em_font';
			subsLink.textContent = '▶';
			subsLink.href = subs_target;
			subsLink.title = worker.GUIp_i18n.forum_subs_short;
			fr = document.createDocumentFragment();
			fr.appendChild(document.createTextNode(' '));
			fr.appendChild(subsLink);
			forumLink.parentElement.insertBefore(fr, forumLink.parentElement.lastElementChild.nextSibling);
			worker.$('#e_subs').wup({
				title: worker.GUIp_i18n.forum_subs_short,
				placement: 'bottom',
				onShow: GUIp.common.try2.bind(null, function onSubsShow(t) {
					var wup = t[0],
						pos = parseInt(wup.style.left);
					if (pos < 5) {
						wup.style.left = '10px';
						wup.firstElementChild.style.left = (parseInt(wup.firstElementChild.style.left) - (10 - pos)) + 'px';
					}
					wup.classList.add('e_subs_wup');
				}),
				content: GUIp.common.try2.bind(null, function createSubsContent() {
					var fsubsContent = $('<div class="esubs_content"></div>');
					var fsubsLine, subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
						informers = JSON.parse(ui_storage.get('ForumInformers')) || {},
						topics = worker.Object.keys(subscriptions),
						tid, sub, page, title;
					topics.sort(function(a,b) { return subscriptions[b].date - subscriptions[a].date; });
					for (var i = 0, len = topics.length; i < len; i++) {
						tid = topics[i];
						sub = subscriptions[tid];
						page = Math.ceil(sub.posts / 25);
						title = GUIp.common.escapeHTML(sub.name);
						fsubsLine = worker.$('<div class="esubs_line"></div>').html(
							'<div class="title">' +
								'<img alt="Comment" class="' + (tid in informers ? 'green' : 'grey') +
									'" src="/images/forum/clearbits/comment.gif" />' +
								' <a href="/forums/show_topic/' + tid + '" title="' + title + '" target="_blank">' +
									title +
								'</a>' +
							'</div>' +
							'<div class="info">' +
								GUIp.common.formatTime(new Date(sub.date),'simpledate') +
								', ' +
								GUIp.common.formatTime(new Date(sub.date),'simpletime') +
								'<span class="em_font"> ➠ </span>' +
								'<a href="/forums/show_topic/' + tid + '?page=' + page +
									'&epost=' + (sub.posts + 25 - page*25) +
									'" title="' + worker.GUIp_i18n.forum_subs_info + sub.by +
									'" target="_blank">' +
									sub.by +
								'</a>' +
							'</div>' +
							'<div class="options" data-e-tid="' + tid + '">' +
								(GUIp.common.notif.supported ?
									'<input type="checkbox" data-e-mask="1" ' +
									(sub.notifications & 0x1 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_desktop_notif + '" />'
								: '') +
								'<input type="checkbox" data-e-mask="2" ' +
									(sub.notifications & 0x2 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_sound_notif + '" />' +
							'</div>' +
							'<div style="clear: both;"></div>'
						);
						fsubsLine.appendTo(fsubsContent);
					}
					fsubsLine = worker.$('<div class="esubs_line"></div>').html('<div class="c"><a href="' + subs_target + '" target="_blank">' + worker.GUIp_i18n.forum_subs_full + '</a></div>');
					fsubsLine.appendTo(fsubsContent);
					fsubsContent.on('click', 'input', function() {
						GUIp.common.try2.call(this, ui_improver._onSubCheckboxClick);
					});
					worker.$('#e_subs').wup("hide");
					return fsubsContent;
				})
			});
		}
	}
};
ui_improver.parseChronicles = function(xhr) {
	this.needLog = false;

	var page = document.createElement('div'),
		currentStep = ui_stats.currentStep();
	page.innerHTML = xhr.responseText;
	this.parseDungeonExtras(page.querySelectorAll('#last_items_arena .d_imp .text_content'), null, currentStep); // in case first step is already gone in on-page chronicles
	GUIp.common.parseChronicles.call(ui_improver, page, currentStep, {
		putDMLink: ui_storage.getList('Option:dungeonMapSettings').includes('dmln')
	});
	page = null;

	var alliesMatch, alliesRE = /<a href=["']\/gods\/.*?>(.*?)<\/a>[^]+?<span id=["']hp\d["']>\d+<\/span> \/ (\d+)/g,
		alliesHP = {sum: 0};
	while (alliesMatch = alliesRE.exec(xhr.responseText)) {
		for (var i = 1; i <= 5; i++) {
			if (ui_stats.Hero_Ally_Name(i) === alliesMatch[1]) {
				alliesHP[i] = +alliesMatch[2];
				alliesHP.sum += alliesHP[i];
				break;
			}
		}
	}
	if (alliesHP.sum > 0 && (Object.keys(alliesHP).length === ui_stats.Hero_Alls_Count() + 1)) {
		ui_improver.alliesHP = alliesHP;
		ui_storage.set('Log:' + ui_data.logId + ':allhp', JSON.stringify(ui_improver.alliesHP));
	}
	ui_improver.colorDungeonMap();
};
ui_improver.deleteInvalidChronicles = function() {
	var isHiddenChronicles = true,
		chronicles = document.querySelectorAll('#m_fight_log .line.d_line');
	for (var i = chronicles.length - 1; i >= 0; i--) {
		if (isHiddenChronicles) {
			if (chronicles[i].style.display !== 'none') {
				isHiddenChronicles = false;
			}
		} else {
			if (chronicles[i].style.display === 'none') {
				chronicles[i].parentNode.removeChild(chronicles[i]);
			}
		}
	}
};
ui_improver.improveChronicles = function() {
	if (!GUIp.common[GUIp.common.dungeonPhrases[GUIp.common.dungeonPhrases.length - 1] + 'RegExp']) {
		GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
	} else {
		//ui_improver.deleteInvalidChronicles();
		var i, len, lastNotParsed, cur_pos, cur_step, texts = [], infls = [],
			chronicles = document.querySelectorAll('#m_fight_log .d_msg:not(.parsed)'),
			ch_down = document.querySelector('.sort_ch').textContent === '▼',
			step = ui_stats.currentStep();
		for (cur_step = step, len = chronicles.length, i = ch_down ? 0 : len - 1; (ch_down ? i < len : i >= 0) && step; ch_down ? i++ : i--) {
			lastNotParsed = true;
			if (!chronicles[i].className.includes('m_infl')) {
				texts = [chronicles[i].textContent].concat(texts);
			} else {
				infls = [chronicles[i].textContent].concat(infls);
			}
			if (chronicles[i].parentNode.className.includes(ch_down ? 'turn_separator' : 'turn_separator_inv')) {
				GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step);
				lastNotParsed = false;
				texts = [];
				infls = [];
				step--;
			}
			if (!chronicles[i].className.includes('m_infl')) {
				if (GUIp.common.bossHintRegExp.test(chronicles[i].textContent)) {
					chronicles[i].parentNode.classList.add('bossHint');
					// workaround for an issue when there are two adjacent bosses and bossHint entry is added to the chronicle with a delay,
					// so it never gets inserted into chronicles object and never painted on the already processed map
					if (!this.needLog && step === cur_step && ui_improver.chronicles[cur_step] && !ui_improver.chronicles[cur_step].marks.includes('bossHint')) {
						ui_improver.chronicles[cur_step].marks.push('bossHint');
						if (cur_pos = document.querySelector('#map .map_pos')) {
							cur_pos.classList.add('bossHint');
						}
					}
				}
				if (/voice from above announced that all bosses in|голос откуда-то сверху сообщил, что ни единого живого босса/.test(chronicles[i].textContent)) {
					chronicles[i].innerHTML = chronicles[i].innerHTML.replace(/A pleasant voice from above announced that all bosses in this dungeon have perished and wished the intruders to burn in hell\.|Приятный голос откуда-то сверху сообщил, что ни единого живого босса (?:в этом подземелье|здесь) не осталось, и пожелал виновникам гореть в аду\./, '<strong>$&</strong>');
				}
			}
			chronicles[i].classList.add('parsed');
		}
		if (lastNotParsed) {
			GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step);
		}

		if (this.needLog) {
			if (worker.Object.keys(this.chronicles)[0] === '1' && chronicles.length < 20 && (cur_step === document.querySelectorAll('#m_fight_log .turn_separator' + (!ch_down ? '_inv' : '')).length)) {
				this.needLog = false;
				ui_improver.colorDungeonMap();
			} else if (this.dungeonXHRCount < 3) {
				this.dungeonXHRCount++;
				GUIp.common.requestLog(ui_data.logId, ui_improver.parseChronicles.bind(ui_improver));
			}
		} else if (this.dungeonExtras.transformationPending) {
			// we may need to call this every time since title in map block can be updated with an arbitrary delay
			this.parseDungeonExtras([], document.getElementById('map'), cur_step);
		}

		// informer
		if (worker.Object.keys(this.chronicles).length) {
			ui_informer.update('close to boss', this.chronicles[worker.Object.keys(this.chronicles).reverse()[0]].marks.includes('bossHint'));
		}

		if (ui_storage.get('Log:current') !== ui_data.logId) {
			ui_storage.set('Log:current', ui_data.logId);
			ui_storage.set('Log:' + ui_data.logId + ':dlMoves', '{}');
			ui_storage.set('Log:' + ui_data.logId + ':whMoves', '{}');
			// detect dungeons of pledge and dungeons of mysterious pledge
			if (this.dungeonExtras.type === 'pledge') {
				ui_logger.appendLogs(-1);
				ui_logger.finishBatch();
			} else if (this.dungeonExtras.type === 'mystery') {
				GUIp.common.getDomainXHR('/gods/api/' + encodeURIComponent(ui_data.god_name), function(xhr) {
					var logs = +ui_storage.get('Logger:Logs');
					try {
						logs = JSON.parse(xhr.responseText).wood_cnt - logs;
						if (!logs) return;
					} catch (e) {
						GUIp.common.warn('got invalid JSON from /gods/api/:', xhr.responseText);
						return;
					}
					// we calculate a difference instead of just showing "wd-1" in case user has opened a dungeon
					// after being inactive for some time
					ui_logger.appendLogs(logs);
					ui_logger.finishBatch();
					// and update detected type
					ui_improver.parseDungeonExtras([], 'pledge', cur_step);
				}, function(xhr) {
					GUIp.common.warn('failed to load /gods/api/ (' + xhr.status + '):', xhr.responseText);
				});
			}
		}
		ui_storage.set('Log:' + ui_data.logId + ':steps', ui_stats.currentStep());
		ui_storage.set('Log:' + ui_data.logId + ':map', JSON.stringify(ui_improver.getDungeonMap()));

		// stream the dungeon
		if (this.streamingManager.active) {
			this.streamingManager.uploadStep();
		}
	}
};

ui_improver.getDungeonMapNodes = function() {
	var result = [], rows = document.querySelectorAll('#map .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		result.push(Array.from(rows[i].getElementsByClassName('dmc')));
	}
	return result;
};

ui_improver.getDungeonMap = function() {
	var result = [], cells, row, rows = document.querySelectorAll('#map .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		row = [];
		cells = rows[i].querySelectorAll('.dmc');
		for (var j = 0, len2 = cells.length; j < len2; j++) {
			row.push(cells[j].textContent.trim());
		}
		result.push(row);
	}
	return result;
};

ui_improver.colorDungeonMap = function() {
	if (!ui_data.isDungeon || ui_improver.needLog) {
		return;
	}
	var step, mapCells, currentCell, trapMoveLossCount = 0,
		isJumping = this.dungeonExtras.type === 'jumping',
		isStepInCellar = false,
		isMapInCellar = GUIp.common.isDungeonCellar(),
		coords = GUIp.common.calculateExitXY(isMapInCellar),
		heroesCoords = GUIp.common.calculateXY(GUIp.common.getOwnCell()),
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length;
	if (steps_max !== ui_stats.currentStep()) {
		GUIp.common.warn('step count mismatch: parsed=' + steps_max + ', required=' + ui_stats.currentStep());
		return;
	}
	if ((this.dungeonExtras.nookOffset.x || this.dungeonExtras.nookOffset.y) && (this.dungeonExtras.nookOffset.x + coords.x) === heroesCoords.x && (this.dungeonExtras.nookOffset.y + coords.y) === heroesCoords.y) {
		GUIp.common.debug('party has entered the nook');
		if ((this.dungeonExtras.reward === 'transformation' || this.dungeonExtras.reward === 'unknown') && !this.dungeonExtras.transformationComplete) {
			this.dungeonExtras.transformationPending = true; // now we should wait for a transformation
		}
	}
	// load guidedSteps data if it's not present yet
	this.dungeonGuidedSteps = this.dungeonGuidedSteps || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':guidedSteps')) || {};
	// describe map
	mapCells = document.querySelectorAll('#map .dml');
	for (step = 1; step <= steps_max; step++) {
		if (!this.chronicles[step]) {
			GUIp.common.error('data for step #' + step + ' is missing, colorizing map failed');
			return;
		}
		// check moving to or from the subfloor
		if (this.chronicles[step].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// if currently shown map is unrelated to this step - we just don't care and move on till we get again on the proper map
		if (isStepInCellar !== isMapInCellar) {
			GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,null); // this will do fine without coordinates or proper direction if it's not the dungeon of jumping
			continue;
		}
		// directionless step
		if (this.chronicles[step].directionless) {
			this.directionlessMoves = this.directionlessMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dlMoves')) || {};
			if (this.directionlessMoves[step]) {
				this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
				this.chronicles[step].directionless = false;
			} else {
				Object.assign(this.directionlessMoves,GUIp.common.calculateDirectionlessMove.call(ui_improver, '#map', coords, step));
				if (this.directionlessMoves[step]) {
					this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
					this.chronicles[step].directionless = false;
					ui_storage.set('Log:' + ui_data.logId + ':dlMoves',JSON.stringify(this.directionlessMoves));
				}
			}
		}
		// try to properly count the number of steps which had directional godvoices
		GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,coords);
		// normal step
		GUIp.common.moveCoords(coords, this.chronicles[step]);
		// wormhole jump
		if (this.chronicles[step].wormhole) {
			this.wormholeMoves = this.wormholeMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':whMoves')) || {};
			if (this.wormholeMoves[step]) {
				this.chronicles[step].wormholedst = this.wormholeMoves[step];
			} else if (step === steps_max) {
				GUIp.common.debug('getting wormhole target from actual coords mismatch');
				this.chronicles[step].wormholedst = [heroesCoords.y - coords.y, heroesCoords.x - coords.x];
				this.wormholeMoves[step] = this.chronicles[step].wormholedst;
				ui_storage.set('Log:' + ui_data.logId + ':whMoves',JSON.stringify(this.wormholeMoves));
			} else {
				var result = GUIp.common.calculateWormholeMove.call(ui_improver, '#map', coords, step);
				if (result.wm) {
					this.directionlessMoves = this.directionlessMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dlMoves')) || {};
					Object.assign(this.wormholeMoves,result.wm);
					Object.assign(this.directionlessMoves,result.dm);
					this.chronicles[step].wormholedst = this.wormholeMoves[step];
					ui_storage.set('Log:' + ui_data.logId + ':whMoves',JSON.stringify(this.wormholeMoves));
					ui_storage.set('Log:' + ui_data.logId + ':dlMoves',JSON.stringify(this.directionlessMoves));
				}
			}
			if (this.chronicles[step].wormholedst !== null) {
				if (mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
					currentCell = mapCells[coords.y].children[coords.x];
					GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount,true);
				}
				coords.y += this.chronicles[step].wormholedst[0];
				coords.x += this.chronicles[step].wormholedst[1];
			}
		}
		if (!mapCells[coords.y] || !mapCells[coords.y].children[coords.x]) {
			break;
		}
		currentCell = mapCells[coords.y].children[coords.x];
		if (currentCell.textContent.trim() === '#') {
			GUIp.common.error(
				'parsed chronicle does not match the map at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			break;
		}
		if (currentCell.textContent.trim() === '✖') {
			this.chronicles[step].chamber = true;
		} else if (currentCell.textContent.trim() === '◿') {
			this.chronicles[step].staircase = true;
		}
		if (this.chronicles[step].pointers.length > 0) {
			currentCell.dataset.pointers = this.chronicles[step].pointers.join(' ');
		}
		trapMoveLossCount = GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount);
	}
	if (heroesCoords.x !== coords.x || heroesCoords.y !== coords.y) {
		GUIp.common.error(
			'chronicle processing failed, coords diff: x: ' + (heroesCoords.x - coords.x) + ', y: ' + (heroesCoords.y - coords.y));
		if (ui_utils.hasShownInfoMessage !== true) {
			ui_utils.showMessage('info', {
				title: worker.GUIp_i18n.coords_error_title,
				content: '<div>' + worker.GUIp_i18n.coords_error_desc + ': [x:' + (heroesCoords.x - coords.x) + ', y:' + (heroesCoords.y - coords.y) + '].</div>'
			});
			ui_utils.hasShownInfoMessage = true;
		}
		// reset possibly wrong corrections to try again on the next step
		if (this.directionlessMoves) {
			Object.values(this.chronicles).forEach(function(ch) {
				if (ch.marks.includes('directionless')) {
					ch.direction = null;
					ch.directionless = true;
				}
			});
			this.directionlessMoves = null;
			ui_storage.set('Log:' + ui_data.logId + ':dlMoves','{}');
		}
		if (this.wormholeMoves) {
			this.wormholeMoves = null;
			ui_storage.set('Log:' + ui_data.logId + ':whMoves','{}');
		}
	}
	// save accumulated guided steps information
	ui_storage.set('Log:' + ui_data.logId + ':guidedSteps',JSON.stringify(this.dungeonGuidedSteps));
	// update broadcast link
	ui_utils.updateBroadcastLink(document.getElementById('e_broadcast'));
	ui_utils.updateBroadcastLink(document.querySelector('div.fightblink a'));
	// highlight treasury on the map
	GUIp.common.improveMap.call(this,'map');
	// map settings
	var dmapSettings = ui_storage.getList('Option:dungeonMapSettings');
	// mark exclamations
	if (!this.isFirstTime && dmapSettings.includes('excl') && (['clarity','mystery'].includes(this.dungeonExtras.type) || Object.keys(GUIp.common.dmapExclCache).length)) {
		GUIp.common.dmapExcl();
		if (Object.keys(GUIp.common.dmapExclCache).length) {
			ui_storage.set('Log:' + ui_data.logId + ':excl', JSON.stringify(GUIp.common.dmapExclCache));
		}
	}
	// print dungeon coords
	GUIp.common.dmapCoords();
	// print dungeon dimensions
	if (document.querySelector('#map .block_title')) {
		var mapDimensions;
		if (!(mapDimensions = document.querySelector('.dmapDimensions'))) {
			mapDimensions = document.createElement('span');
			mapDimensions.classList.add('dmapDimensions');
			ui_utils.hideElem(mapDimensions, !dmapSettings.includes('dims'));
			document.querySelector('#map .block_title').insertBefore(mapDimensions,document.querySelector('#map .block_title #e_broadcast'));
		}
		mapDimensions.textContent = GUIp.common.dmapDimensions();
	}
};

ui_improver.parseDungeonExtras = function(impNodes, dungeonType, step) {
	var normalizedType, changed = false,
		xtras = this.dungeonExtras,
		challenges = {
			nook:      /специальной комнате|тайной комнаты|тайной ложи|запретное место| a nook |the nook's/i,
			silence:   /подрулившим гласами не более 8 раз|finding the treasure with 8 direction voices/i,
			agility:   /дошедшим до сокровища за 40 шагов|find the reward in 40 steps/i,
			ecology:   /не тронувшим ни одного босса|prize for avoiding all bosses/i,
			genocide:  /убившим всех боссов( на любом этаже)? здесь полагается награда|prize for hunting down all bosses/i,
			survivors: /до сокровища дойдут все. Либо только двое|whole team or exactly two of the members find/i
		},
		rewards = {
			resurrection: /павшие герои будут оживлены|reviving of knocked out heroes/i,
			key:          /найти ключ, открывающий сокровищницу|key to open the treasury/i,
			hints:        /указатели на сокровище можно включить только там|switch to turn on treasure clues/i,
			dice:         /все ловушки подземелья превратятся в тайники. Или наоборот|traps will turn into secret stashes... or vice versa/i,
			gold:         /найдется бонусная порция золота|extra gold in the treasury/i,
			artifacts:    /будет больше трофеев|more artifacts in the treasury/i,
			notraps:      /ловушки в подземелье отключатся|dungeon trap disabling switch/i,
			noboss:       /охраняющий сокровищницу босс уйдет со своего поста|ousting of the treasure boss/i,
			double:       /появится двойная порция бревен|find an extra log of gopher/i,
			clarity:      /отметятся все интересные места|location of all points of interest/i,
			transformation: /подземелье изменит свой тип|dungeon will change its type/i,
			unknown:      /не доставившего горному королю письмо|без каких-либо намеков на её смысл|nobody knows what the reward/i
		};
	if (!xtras.nookOffset) {
		xtras.nookOffset = GUIp.common.calculateNookOffset();
		changed = true;
	}
	if (impNodes.length) {
		Array.from(impNodes).map(function(a) { return a.textContent; }).some(function(text) {
			for (var key in challenges) {
				if (challenges[key].test(text) && key !== xtras.challenge) {
					xtras.challenge = key;
					for (var key in rewards) {
						if (rewards[key].test(text)) {
							xtras.reward = key;
							break;
						}
					}
					if (!xtras.reward) xtras.reward = 'unknown';
					changed = true;
					break;
				}
			}
		});
	}
	if (dungeonType) {
		dungeonType = dungeonType.textContent || dungeonType;
		normalizedType = /Прыгучести|Jumping/i.test(dungeonType) ? "jumping" : /Загадки|Mystery/i.test(dungeonType) ? "mystery" : /Заметности|Clarity/i.test(dungeonType) ? "clarity" : /Залога|Pledge/i.test(dungeonType) ? "pledge" : "other";
		if (normalizedType !== xtras.initialType) {
			xtras.type = xtras.initialType = normalizedType; // transformation challenge reward could change the dungeon type, so we need to store both declared dungeon type and possibly detected one
			xtras.typeStep = step;
			if (xtras.transformationPending) {
				xtras.transformationComplete = true;
				delete xtras.transformationPending;
			}
			changed = true;
		}
	}
	if (changed) {
		ui_storage.set('Log:' + ui_data.logId + ':extras',JSON.stringify(xtras));
	}
};

/** @namespace */
ui_improver.reporter = {
	// module loading order is arbitrary, so we cannot simply assign a field
	/** @type {string} */
	get url() { return (worker.GUIp_locale === 'ru' ? GUIp.common.erinome_url : GUIp.common.erinome_url_en) + '/reporter'; },
	/** @type {string} */
	get urlMS() { return GUIp.common.erinome_url + '/mapStreamer'; },

	/**
	 * @param {string} logID
	 * @returns {string}
	 */
	getLogURL: function(logID) {
		return ui_data.isDungeon ? '/duels/log/' + logID + '?sort=desc&estreaming=1' : this.url + '/duels/log/' + logID;
	},

	_getHTML: function(id) {
		var node = document.getElementById(id);
		return node ? node.outerHTML : '';
	},

	/** @const {number} */
	protocolVersion: 3,

	/**
	 * @returns {!Object<string, (string|number)>}
	 */
	collect: function() {
		if (ui_data.isDungeon) {
			return {
				id: ui_data.logId,
				step: ui_stats.currentStep(),
				map: JSON.stringify(ui_improver.getDungeonMap()),
				lang: worker.GUIp_locale
			};
		}
		// https://github.com/Godvillers/ReporterServer/blob/master/docs/api.md
		var data = ['alls', 's_map', 'm_fight_log'].map(this._getHTML).join('<&>');
		return {
			protocolVersion: this.protocolVersion,
			agent: 'eGUI+/' + ui_data.currentVersion,
			link: GUIp.common.resolveURL(ui_utils.getStat('#fbclink', 'href') || worker.location.href),
			stepDuration: 18,
			timezone: -new Date().getTimezoneOffset(),
			step: ui_stats.currentStep(),
			playerNumber: ui_improver.islandsMap.ownArk + 1,
			cargo: ui_stats.cargo(),
			data: worker.base64js.fromByteArray(worker.pako.deflate(data)),
			clientData: '{"erinome":' + JSON.stringify({
				conditions: Object.keys(ui_improver.islandsMap.manager.conditions)
			}) + '}'
		};
	},

	/**
	 * @param {!Object} data
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	send: function(data, onsuccess, onfailure) {
		GUIp.common.postXHR((ui_data.isDungeon ? this.urlMS + '/port' : this.url + '/send'), data, 'form-data', onsuccess, onfailure);
	},

	/**
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	fetchAPIInfo: function(onsuccess, onfailure) {
		GUIp.common.getXHR(this.url + '/api.json', onsuccess, onfailure);
	},

	/**
	 * @param {string} text
	 * @returns {?Object<string, *>}
	 */
	parseAPIInfo: function(text) {
		try {
			var api = JSON.parse(text);
			return api.currentVersion >= api.minSupportedVersion ? api : null;
		} catch (e) {
			return null;
		}
	},

	/**
	 * @param {!Object} api
	 * @returns {boolean}
	 */
	checkVersion: function(api) {
		return this.protocolVersion >= api.minSupportedVersion && this.protocolVersion <= api.currentVersion;
	}
};

ui_improver.streamingManager = {
	onsuccess: [],
	onfailure: [],
	onincompatibility: [],
	lastUploadedStep: 0,

	get active() {
		return ui_storage.get('Log:streaming') === ui_data.logId;
	},

	set active(value) {
		ui_storage.set('Log:streaming', value ? ui_data.logId : '');
	},

	_runCallbacks: function(callbacks) {
		callbacks.forEach(function(f) { f(); });
	},

	uploadStep: function() {
		var self = this,
			reporter = ui_improver.reporter,
			step = ui_stats.currentStep();
		if (step <= this.lastUploadedStep) return;

		reporter.send(reporter.collect(), function() {
			// requests are asynchronous, so perform an extra check
			if (step > self.lastUploadedStep) {
				self.lastUploadedStep = step;
				self._runCallbacks(self.onsuccess);
			}
		}, function(xhr) {
			GUIp.common.error('cannot stream step ' + step + ' (' + xhr.status + '):', xhr.responseText);
			// investigate the problem
			if (xhr.status === 501 /*Not Implemented*/) {
				// stop trying to stream
				self.active = false;
				// apparently, we are using an outdated protocol - let's check that
				reporter.fetchAPIInfo(function(xhr) {
					var api = reporter.parseAPIInfo(xhr.responseText);
					if (api == null) {
						// got invalid JSON?
						GUIp.common.error("cannot parse Reporter API: '" + xhr.responseText + "'");
						self._runCallbacks(self.onfailure);
						return;
					}
					GUIp.common.info('version = ' + reporter.protocolVersion + ', api =', api);
					if (reporter.checkVersion(api)) {
						// our version seems to be OK, but we still got 501. Something mystical
						self._runCallbacks(self.onfailure);
					} else {
						// yes, we're indeed too ancient
						self._runCallbacks(self.onincompatibility);
					}
					// as said before, don't try again
				}, function(xhr) {
					GUIp.common.error('cannot fetch Reporter API:', xhr.status);
					self._runCallbacks(self.onfailure);
				});
				return;
			}
			// try again later
			GUIp.common.setTimeout(function() {
				if (step === ui_stats.currentStep()) {
					self.uploadStep();
				}
			}, 5e3);
		});
	}
};

ui_improver.initStreaming = function(container) {
	if (!container) {
		GUIp.common.error('streaming link injection failed');
		return;
	}

	container.insertAdjacentHTML('beforeend',
		'<div id="e_streaming">' +
			'<a href="#">' + GUIp_i18n.start_streaming + '</a>' +
			'<span class="hidden">' + GUIp_i18n.streaming_now +
				' <span class="e_emoji e_emoji_cross eguip_font">❌</span></span>' +
			'<a href="' + this.reporter.getLogURL(ui_data.logId) + '" target="_blank">' +
				GUIp_i18n.streaming_now +
			'</a>' +
			' <a class="e_cancel e_emoji e_emoji_cross eguip_font" href="#" title="' + GUIp_i18n.cancel_streaming +
			'">❌</a>' +
		'</div>'
	);
	container = document.getElementById('e_streaming');
	var startLink = container.firstChild,
		placeholder = startLink.nextSibling,
		streamingLink = placeholder.nextSibling,
		stopLink = container.lastChild, // streamingLink.nextSibling is a text node containing whitespace
		established = this.streamingManager.active;
	if (established) {
		startLink.classList.add('hidden');
		// streamingManager.uploadStep is called from elsewhere
	} else {
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
	}

	GUIp.common.addListener(startLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.add('hidden');
		if (!established) {
			placeholder.classList.remove('hidden');
		} else {
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
		ui_improver.streamingManager.active = true;
		ui_improver.streamingManager.uploadStep();
	});

	GUIp.common.addListener(stopLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.remove('hidden');
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
		ui_improver.streamingManager.active = false;
	});

	this.streamingManager.onsuccess.push(function() {
		if (!established) {
			established = true;
			placeholder.classList.add('hidden');
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
	});

	var handler = function(msg) {
		container.textContent = msg;
		container.classList.add('error');
	};
	this.streamingManager.onincompatibility.push(handler.bind(null, worker.GUIp_i18n.streaming_is_unsupported));
	this.streamingManager.onfailure.push(handler.bind(null, worker.GUIp_i18n.streaming_failed));
};

/** @namespace */
ui_improver.islandsMap = {
	/** @type {?GUIp.common.islandsMap.MigratingMVManager} */
	manager: null,

	_svg: null,

	/**
	 * @readonly
	 * @type {number}
	 */
	ownArk: -1,

	_saveData: function() {
		ui_storage.set('Log:current', ui_data.logId);
		ui_storage.set('Log:' + ui_data.logId + ':model',
			JSON.stringify(GUIp.common.islandsMap.conv.encode(this.manager.model))
		);
	},

	/**
	 * @private
	 * @returns {{model: ?GUIp.common.islandsMap.Model, relevant: boolean}}
	 */
	_tryLoadData: function() {
		var rawModel = GUIp.common.parseJSON(ui_storage.get('Log:' + ui_data.logId + ':model'));
		return rawModel ? {
			model: GUIp.common.islandsMap.conv.decode(rawModel),
			relevant: rawModel.step === ui_stats.currentStep()
		} : {model: null, relevant: false};
	},

	_getOwnArkIndex: function() {
		for (var i = 1; i <= 4; i++) {
			if (ui_stats.Hero_Ally_Name(i) === ui_data.char_name) {
				return i - 1;
			}
		}
		return -1;
	},

	_rollbackModifications: function(svg) {
		if (svg === this._svg) {
			GUIp.common.islandsMap.defaults.vTransUnbindAll(this.manager.model, this.manager.view);
		} else {
			this._svg = svg;
		}
	},

	_checkBeasties: function() {
		if (!GUIp.common.sailing.phrases.beastiesCount) {
			return;
		}
		var enemies = document.querySelectorAll('#opps .opp_n');
		for (var i = 0, len = enemies.length; i < len; i++) {
			var maxHP = ui_stats.EnemySingle_MaxHP(i + 1);
			if (maxHP >= 100) {
				continue; // an ark, apparently
			}
			var text = enemies[i].textContent.trim();
			if (text && !GUIp.common.sailing.isBeastie(text) && !enemies[i].classList.contains('improved')) {
				enemies[i].insertAdjacentHTML('beforeend',
					'<span class="e_beareport"> ' +
					'<a title="' + worker.GUIp_i18n.sea_monster_report +
					'" href="' + GUIp.common.erinome_url +
						'/beastiereport?name=' + encodeURIComponent(text) +
						'&hp=' + maxHP +
						'&locale=' + worker.GUIp_locale +
					'" target="_blank">[?]</a></span>'
				);
				enemies[i].classList.add('improved');
			}
		}
	},

	/**
	 * @private
	 * @param {!Element} mapContainer
	 * @param {number} ownArkPos
	 * @param {!Array<string>} settings
	 */
	_changeMapHeader: function(mapContainer, ownArkPos, settings) {
		var blockTitle, distInfo;
		if (!(blockTitle = mapContainer.getElementsByClassName('block_title')[0])) {
			return;
		}
		if (!(distInfo = blockTitle.getElementsByClassName('e_dist_info')[0])) {
			// opera 12.x can't insert multiple nodes in insertAdjacentHTML at once
			// when any ancestor node has DOMNodeInserted event attached
			blockTitle.appendChild(document.createTextNode(' '));
			blockTitle.insertAdjacentHTML('beforeend', '<span class="e_dist_info"></span>');
			distInfo = blockTitle.lastChild;
		}
		distInfo.innerHTML = ui_utils.formatSailingDistInfo(
			this.manager.model, ownArkPos, this.manager.conditions, settings
		);
	},

	_replaceModelAndView: function(step) {
		this.manager.replaceModelAndView(this._svg, step);
		this._saveData();
	},

	_applyModifications: function(mapContainer) {
		var model = this.manager.model,
			settings = ui_storage.getList('Option:islandsMapSettings'),
			ownArkPos = model.arks[this.ownArk],
			rivalsNearby = false;
		// apply changes to the SVG
		GUIp.common.islandsMap.defaults.vTransBindAll(
			model, this.manager.view, this.manager.conditions, settings, true
		);
		// detect missing beasties in our lists
		this._checkBeasties();
		// stream the sailing
		if (ui_improver.streamingManager.active) {
			// wait until beasties in the chronicle block are highlighted
			GUIp.common.setTimeout(function() { ui_improver.streamingManager.uploadStep(); }, 0);
		}

		if (ownArkPos !== 0x8080) {
			// remember distance to the port for custom informers
			ui_improver.sailPortDistance =
				GUIp.common.islandsMap.vec.dist(ownArkPos, model.port !== 0x8080 ? model.port : 0x0);
			// show distances to port & rim
			this._changeMapHeader(mapContainer, ownArkPos, settings);
			// check distances to other arks
			rivalsNearby = model.step >= 5 && model.arks.some(function(ark) {
				return ark !== 0x8080 && ark !== ownArkPos && GUIp.common.islandsMap.vec.dist(ark, ownArkPos) <= 3;
			});
		}
		ui_informer.update('close to rival', rivalsNearby);
	},

	_extractConditions: function(mapContainer) {
		var logID = ui_data.logId,
			key = 'Log:' + logID + ':conds',
			conditions = ui_storage.get(key);
		if (conditions != null) {
			this.manager.conditions = conditions ? GUIp.common.makeHashSet(conditions.split(',')) : Object.create(null);
			return;
		}

		conditions = GUIp.common.sailing.tryExtractConditions();
		if (conditions != null) {
			this.manager.conditions = GUIp.common.sailing.parseConditions(conditions);
			ui_storage.set(key, Object.keys(this.manager.conditions));
		} else {
			// TODO: retry on failure
			GUIp.common.requestLog(logID, function(xhr) {
				var conds;
				this.manager.conditions = GUIp.common.sailing.parseConditions(
					GUIp.common.sailing.extractConditionsFromHTML(xhr.responseText)
				);
				conds = Object.keys(this.manager.conditions);
				ui_storage.set(key, conds);
				if (conds.length) {
					// they could affect parsing the map
					this._rollbackModifications(mapContainer.getElementsByTagName('svg')[0]);
					this._replaceModelAndView(ui_stats.currentStep());
					this._applyModifications(mapContainer);
				}
			}.bind(this));
		}
	},

	_loadPOIColors: function() {
		if (!ui_storage.getList('Option:islandsMapSettings').includes('rndc')) {
			return;
		}

		var key = 'Log:' + ui_data.logId + ':poiColors',
			colors = GUIp.common.parseJSON(ui_storage.get(key)),
			colorizer = GUIp.common.islandsMap.vtrans.poiColorizer;
		if (GUIp.common.isIntegralArray(colors)) {
			colorizer.colors = colors;
		} else {
			GUIp.common.shuffleArray(colorizer.colors);
			ui_storage.set(key, '[' + colorizer.colors + ']');
		}
	},

	/**
	 * @param {!Element} mapContainer
	 */
	initMap: function(mapContainer) {
		var loaded = this._tryLoadData(), // instead of parsing each time the page is refreshed
			svgs = mapContainer.getElementsByTagName('svg'),
			config = {childList: true, subtree: true},
			observer;
		this.ownArk = this._getOwnArkIndex();
		this.manager = new GUIp.common.islandsMap.MigratingMVManager(loaded.model);
		this._extractConditions(mapContainer);
		this._loadPOIColors();

		// do initial processing
		this._svg = svgs[0]; // nothing to rollback; just assign
		if (loaded.relevant) {
			this.manager.replaceView(this._svg);
		} else {
			this._replaceModelAndView(ui_stats.currentStep());
		}
		this._applyModifications(mapContainer);

		// watch for changes
		observer = GUIp.common.islandsMap.observer.create(function() {
			var step = ui_stats.currentStep();
			this._rollbackModifications(svgs[0]);
			if (step !== this.manager.model.step) {
				this._replaceModelAndView(step);
			} else {
				// surprisingly, sometimes an SVG can be replaced with no changes to it.
				// no need to touch our model in that case.
				this.manager.replaceView(this._svg);
			}
			this._applyModifications(mapContainer);
		}, this);
		observer.observe(mapContainer, config);
		observer.observe(document.querySelector('#m_fight_log h2'), config);
		GUIp.common.tooltips.watchSubtreeSVG(document.getElementById('map_wrap'));
	}
};

ui_improver._wrapCargoEmoji = function(emoji) {
	return (
		'<span class="e_emoji e_emoji_sailing' + (GUIp.common.renderTester.testChar(emoji) ? '' : ' eguip_font') +
		'">' + emoji + '</span>'
	);
};

ui_improver.improveCargo = function() {
	var node = document.querySelector('#hk_cargo .l_val'),
		html = '',
		newHTML = '';
	if (node.getElementsByClassName('e_emoji')[0]) {
		return;
	}
	html = node.innerHTML;
	newHTML = html.replace(/[♂♀]|\uD83D[\uDCB0\uDCE6]/g, ui_improver._wrapCargoEmoji); // ♂♀💰📦
	if (newHTML !== html) {
		node.innerHTML = newHTML;
	}
};

ui_improver.improveSailChronicles = function() {
	GUIp.common.sailing.describeBeastiesOnPage('.d_msg:not(.parsed)', 'parsed', ui_storage.getList('Option:islandsMapSettings').includes('bhp'));
};

ui_improver.initSailing = function() {
	var mapContainer = GUIp.common.sailing.tryFindMapBlock();
	if (!mapContainer) {
		GUIp.common.setTimeout(ui_improver.initSailing, 250);
		return;
	}
	var rulerContainer = mapContainer.querySelector('.block_h .l_slot');
	if (!rulerContainer) {
		GUIp.common.error('ruler injection failed');
	} else {
		rulerContainer.insertAdjacentHTML('beforeend',
			'<span id="e_ruler_button" class="e_emoji e_emoji_ruler eguip_font" title="' + GUIp_i18n.sail_ruler +
			'">📏</span>'
		);
		GUIp.common.islandsMap.vtrans.rulerManager.init(rulerContainer.lastChild);
	}
	ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content'));
	ui_improver.islandsMap.initMap(mapContainer);
	ui_improver.improveCargo();
	ui_improver.improveSailChronicles();
};

ui_improver.whenWindowResize = function() {
	ui_improver.chatsFix();
	// sail mode column resizing
	if (ui_improver.sailPageResize) {
		var sccwidth,
			sclwidth = worker.$('#a_left_block').outerWidth(true),
			scrwidth = worker.$('#a_right_block').outerWidth(true),
			scl2width = worker.$('#a_left_left_block'),
			scr2width = worker.$('#a_right_right_block'),
			maxwidth = worker.$(worker).width() * 0.85;
		scl2width = scl2width[0] && scl2width[0].offsetWidth ? scl2width.outerWidth(true) : 0;
		scr2width = scr2width[0] && scr2width[0].offsetWidth ? scr2width.outerWidth(true) : 0;
		sccwidth = Math.max(Math.min((maxwidth - sclwidth - scl2width - scrwidth - scr2width),932),448); // todo: get rid of hardcoded values?
		worker.$('#a_central_block').width(sccwidth);
		sccwidth = worker.$('#a_central_block').outerWidth(true);
		//worker.$('#main_wrapper').width(sccwidth + sclwidth + scl2width + scrwidth + scr2width + 20);
		worker.$('.c_col .block_title').each(function() { worker.$(this).width(sccwidth - 115); });
	}
	// body widening
	worker.$('body').width(worker.$(worker).width() < worker.$('#main_wrapper').width() ? worker.$('#main_wrapper').width() : '');
};
ui_improver._clockUpdate = function() {
	worker.$('#control .block_title').text(ui_utils.formatClock(ui_utils.getPreciseTime(ui_improver.clock.offset)));
	if (Date.now() > ui_improver.clock.switchOffTime) {
		ui_improver._clockToggle();
	}
};
ui_improver._clockToggle = function(e) {
	if (e) {
		e.stopPropagation();
	}
	if (ui_improver.clockToggling) {
		return;
	}
	ui_improver.clockToggling = true;
	var $clock = worker.$('#control .block_title');
	if (ui_improver.clock) {
		worker.clearInterval(ui_improver.clock.updateTimer);
		$clock.fadeOut(500, GUIp.common.try2.bind(ui_improver.clock.prevText, function() {
			$clock.removeClass('e_clock_inaccurate');
			$clock.text(this).fadeIn(500);
			$clock.prop('title', worker.GUIp_i18n.show_godville_clock);
			ui_improver.clockToggling = false;
		}));
		ui_improver.clock = null;
	} else {
		ui_improver.clock = {
			prevText: $clock.text(),
			switchOffTime: Date.now() + 300e3, // 5 min
			offset: ui_storage.getFlag('Option:localtimeGodvilleClock') ? (
				new Date().getTimezoneOffset() * -60e3
			) : (ui_storage.get('Option:offsetGodvilleClock') || 3) * 3600e3,
			updateTimer: 0
		};
		var ok = false;
		Promise.all([
			ui_utils.syncClock().then(function(success) {
				ok = success;
			}),
			new Promise(function(resolve) {
				$clock.fadeOut(500, GUIp.common.try2.bind(null, function() {
					$clock.fadeIn(500);
					ui_improver.clockToggling = false;
					if (!ok) {
						$clock.text('––:––:––').addClass('e_clock_inaccurate');
					}
					$clock.prop('title', worker.GUIp_i18n.hide_godville_clock);
					resolve();
				}));
			})
		]).then(function() {
			if (!ui_improver.clock || ui_improver.clock.updateTimer) {
				return; // user hid the clock before it got synced
			}
			if (ok) {
				ui_improver._clockUpdate();
				ui_improver.clock.updateTimer = GUIp.common.setTimeout(function() {
					ui_improver._clockUpdate();
					ui_improver.clock.updateTimer = GUIp.common.setInterval(ui_improver._clockUpdate, 1e3);
				}, 1e3 - ui_utils.getPreciseTime().getMilliseconds());
			} else {
				// ui_improver.clockToggling = false;
				ui_improver._clockToggle();
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};

ui_improver.improveInterface = function() {
	var node, handler;
	if (this.isFirstTime) {
		worker.$('a[href="#"]').removeAttr('href');
		ui_improver.whenWindowResize();
		GUIp.common.addListener(worker, 'resize', GUIp.common.debounce(250, ui_improver.whenWindowResize));
		ui_utils.loggerWidthChanger();
		if (ui_storage.getFlag('Option:themeOverride')) {
			ui_utils.switchTheme(ui_storage.get('ui_s'),true);
		} else {
			ui_utils.switchTheme();
		}
		GUIp.common.addListener(worker, 'focus', function() {
			ui_improver.checkGCMark('focus');
			ui_informer.updateCustomInformers();
			// forcefully fire keyup event on focus to prevent possible issues with alt key processing
			worker.$(document).trigger(worker.$.Event('keyup', {originalEvent: {}}));
		});
		// cache private messages
		ui_utils.pmNotificationInit();
		// experimental keep-screen-awake feature for Android
		if (GUIp.common.isAndroid && (worker.navigator.requestWakeLock || worker.navigator.wakeLock)) {
			ui_improver.createWakelock();
		}
		// generate nearby town information on usual place
		if (!ui_data.isFight) {
			ui_improver.nearbyTownsFix();
		}
		if (!ui_data.isFight && !ui_storage.getFlag('Option:disableGodvilleClock') && document.querySelector('#control .block_title')) {
			var controlTitle = document.querySelector('#control .block_title');
			controlTitle.title = worker.GUIp_i18n.show_godville_clock;
			controlTitle.style.cursor = 'pointer';
			GUIp.common.addListener(controlTitle, 'click', ui_improver._clockToggle);
		}
		var encButton = worker.$('#cntrl .enc_link'),
			punButton = worker.$('#cntrl .pun_link'),
			mirButton = worker.$('#cntrl .mir_link');
		if (ui_storage.getFlag('Option:improveDischargeButton')) {
			var dischargeHandlers, dischargeButton = worker.$('#acc_links_wrap .dch_link');
			encButton.click(function() { ui_improver.last_infl = 'enc'; });
			punButton.click(function() { ui_improver.last_infl = 'pun'; });
			mirButton.click(function() { ui_improver.last_infl = 'mir'; });
			if (dischargeButton.length) {
				dischargeHandlers = worker.$._data(dischargeButton[0],'events');
				if (dischargeHandlers && dischargeHandlers.click && dischargeHandlers.click.length === 1) {
					dischargeButton.click(function() {
						try {
							var limit = ui_stats.Max_Godpower() - 50;
							if (ui_improver.dailyForecast.get().includes('accu70')) {
								limit -= 20;
							}
							if (worker.$('#voice_submit').attr('disabled') === 'disabled') {
								limit += 5;
							}
							if (!worker.$('#cntrl .enc_link').hasClass('div_link')) {
								if (ui_improver.last_infl === 'mir') {
									limit += 50;
								} else {
									limit += 25;
								}
							}
							GUIp.common.debug('acc_discharge dynamic limit =', limit);
							localStorage.setItem('gp_thre',limit);
						} catch (e) {
							GUIp.common.error('setting discharge dynamic limit failed:', e);
						}
					});
					dischargeHandlers.click.reverse();
				}
			}
		}
		if (!ui_data.isFight) {
			ui_utils.addGPConfirmation(encButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_encourage_hero' : 'to_encourage_heroine']));
			ui_utils.addGPConfirmation(punButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_punish_hero' : 'to_punish_heroine']));
			ui_utils.addGPConfirmation(mirButton, 50,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n.to_make_a_miracle));
		}
		if (worker.GUIp_browser === 'Firefox') {
			try {
				worker.$(document).bind('click', function(e) {
					if (e.which !== 1) {
						e.stopImmediatePropagation();
					}
				});
				var rmClickFix, clickHandlers = worker.$._data(document,'events');
				if (clickHandlers.click && clickHandlers.click.length) {
					rmClickFix = clickHandlers.click.pop();
					clickHandlers.click.splice(clickHandlers.click.delegateCount || 0,0,rmClickFix);
				}
			} catch (e) {
				GUIp.common.error('failed to init rmclickfix workaround:', e);
			}
		}
		GUIp.common.addListener(worker, 'beforeunload', function() {
			// save unsent personal messages
			var node = document.getElementsByClassName('chat_ph')[0], text;
			if (node) {
				var textareas = node.getElementsByTagName('textarea'),
					names = node.getElementsByClassName('dockfrname');
				for (var i = 0, len = Math.min(textareas.length, names.length); i < len; i++) {
					node = textareas[i];
					if (!node.disabled) {
						if ((text = node.value)) {
							ui_tmpstorage.set('PM:' + names[i].textContent, text);
						} else {
							ui_tmpstorage.remove('PM:' + names[i].textContent);
						}
					}
				}
			}
		});
		if ((node = document.getElementById('logout'))) {
			GUIp.common.addListener(node, 'click', ui_tmpstorage.wipeOut.bind(ui_tmpstorage));
		}
		// workaround for title-related issues
		Object.defineProperty(document,'title', {
			set: function(val) {
				try {
					ui_data.docTitle = val;
					ui_informer.redrawTitle();
					return val;
				} catch (e) { return val; }
			},
			get: function() {
				try {
					return document.querySelector('title').childNodes[0].nodeValue;
				} catch (e) { return ''; }
			},
			configurable: true
		});
		ui_storage.addImmediateListener('Option:discardTitleChanges', ui_informer.redrawTitle.bind(ui_informer));
		ui_storage.addImmediateListener('UserCss', GUIp.common.addCSSFromString);
	}
	if (ui_data.isFight && !document.getElementById('e_broadcast')) {
		var qselector = document.querySelector(ui_data.isDungeon ? '#map .block_title, #control .block_title, #m_control .block_title' : '#control .block_title, #m_control .block_title');
		if (qselector) {
			if ((ui_data.isSail || ui_data.isMining) && worker.GUIp_locale === 'en') {
				qselector.textContent = 'Remote';
			}
			qselector.insertAdjacentHTML('beforeend', '<a id="e_broadcast" class="broadcast" href="/duels/log/' + ui_data.logId + ui_utils.generateBroadcastLinkXtra() + '" target="_blank">' + worker.GUIp_i18n.broadcast + '</a>');
		}
	}
	if (!document.getElementById('e_custom_informers_setup') && ui_informer.activeInformers.get().custom_informers) {
		ui_utils.generateLightboxSetup('custom_informers','#stats .block_title, #m_info .block_title, #b_info .block_title',function() { ui_words.init(); });
	}
	if (!ui_data.isFight && !document.getElementById('e_custom_craft_setup') && ui_storage.getFlag('Option:enableCustomCraft')) {
		ui_utils.generateLightboxSetup('custom_craft','#inventory .block_title',function() { ui_words.init(); ui_inventory.rebuildCustomCraft(); ui_improver.calculateButtonsVisibility(true); });
		ui_storage.addListener('Option:fixedCraftButtons', ui_improver.calculateButtonsVisibility.bind(ui_improver, true));
	}
	if (ui_data.isFight && !document.getElementById('e_ally_blacklist_setup') && document.querySelector('#alls .block_title, #bosses .block_title, #o_info .block_title')) {
		ui_utils.generateLightboxSetup('ally_blacklist',(document.querySelector('#o_info .l_val a[href*="/gods/"]') ? '#o_info' : '#alls')+' .block_title, #bosses .block_title',function() { ui_words.init(); ui_improver.improvePlayers(); });
	}
	if (this.isFirstTime) {
		handler = ui_words.init.bind(ui_words);
		ui_storage.addListener('CustomWords:custom_informers', handler);
		if (ui_data.isFight) {
			ui_storage.addListener('CustomWords:ally_blacklist', function() {
				ui_words.init();
				ui_improver.improvePlayers();
			});
		} else {
			ui_storage.addListener('CustomWords:chosen_monsters', handler);
			ui_storage.addListener('CustomWords:special_monsters', handler);
			ui_storage.addListener('CustomWords:custom_craft', function() {
				ui_words.init();
				ui_inventory.rebuildCustomCraft();
				ui_improver.calculateButtonsVisibility(true);
			});
		}
		ui_storage.addImmediateListener('Option:useBackground', GUIp.common.setPageBackground);
	}
};
/**
 * @param {!Element} root
 * @returns {!Array<!GUIp.common.activities.LastFightsEntry>}
 */
ui_improver.getLastFights = function(root) {
	var ftypes = root.getElementsByClassName('wl_ftype');
	if (!ftypes.length) return [];
	root = ftypes[0].parentNode.parentNode;
	var dates = root.getElementsByClassName('wl_date'),
		result = [],
		ftype, link, href;
	for (var i = 0, len = Math.min(ftypes.length, dates.length); i < len; i++) {
		ftype = ftypes[i];
		link = ftype.getElementsByTagName('a')[0];
		href = link.href;
		result[i] = {
			date: +GUIp.common.parseDateTime(dates[i].textContent),
			type: GUIp.common.activities.parseFightType(link.textContent),
			logID: href.slice(href.lastIndexOf('/') + 1),
			success: ftype.textContent.includes('✓')
		};
	}
	return result.sort(ui_utils.byDate);
};
ui_improver.improveLastFights = function() {
	var popover = document.getElementById('lf_popover_c');
	if (!popover || ui_utils.isAlreadyImproved(popover)) {
		return;
	}
	var wupStyle = $(popover).closest('.wup')[0].style;
	wupStyle.width = (parseInt(wupStyle.width) + 30) + 'px';
	ui_utils.observeUntil(popover, {childList: true, subtree: true}, function() {
		var fights = ui_improver.getLastFights(popover);
		if (fights.length) return fights;
	}).then(function(fights) {
		var activities = ui_timers.updateLastFights(fights),
			byID = Object.create(null),
			links = popover.querySelectorAll('.wl_ftype.wl_stats a'),
			desc = null,
			i, len, act, link, id;
		for (i = 0, len = activities.length; i < len; i++) {
			act = activities[i];
			if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
				byID[act.logID] = act;
			}
		}
		for (i = 0, len = links.length; i < len; i++) {
			link = links[i];
			id = link.href;
			id = id.slice(id.lastIndexOf('/') + 1);
			if (!(act = byID[id]) || !act.result) {
				continue;
			}
			desc = GUIp.common.activities.describe(act, desc);
			link.parentNode.insertAdjacentHTML('afterend',
				'<div class="e_fight_result ' + desc.class + '" title="' + desc.title + '" data-e-id="' + id + '">' +
					desc.content +
				'</div>'
			);
		}
	}).catch(GUIp.common.onUnhandledException);
};
/**
 * @param {string} activitiesStr
 */
ui_improver.redrawLastFights = function(activitiesStr) {
	var root = document.getElementById('lf_popover_c'),
		desc = null,
		nodes, activities, byID, i, len, act, node;
	if (!root) return;
	nodes = Array.from(root.getElementsByClassName('e_fight_result_unknown'));
	if (!nodes.length) return;
	activities = JSON.parse(activitiesStr);
	byID = Object.create(null);
	for (i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		byID[act.logID] = act;
	}
	for (i = 0, len = nodes.length; i < len; i++) {
		node = nodes[i];
		if (!(act = byID[node.dataset.eId]) || act.result < 0) {
			continue;
		}
		desc = GUIp.common.activities.describe(act, desc);
		node.className = 'e_fight_result ' + desc.class;
		node.title = desc.title;
		node.textContent = desc.content;
	}
};
ui_improver.improveLastVoices = function() {
	if (!document.querySelector('#gv_popover_c .gv_list_empty')) {
		return;
	}
	var lvContent = document.querySelectorAll('.lv_text .div_link_nu');
	if (!lvContent.length) {
		return;
	}
	var handler = ui_utils.triggerChangeOnVoiceInput.bind(ui_utils);
	for (var i = 0, len = lvContent.length; i < len; i++) {
		GUIp.common.addListener(lvContent[i], 'click', handler);
	}
};
ui_improver.improveStoredPets = function() {
	var wtitle = document.querySelector('.wup-inner > .wup-title');
	if (!wtitle || !/В ковчеге|In the ark/.test(wtitle.textContent)) {
		return;
	}
	var splinks = wtitle.parentNode.querySelectorAll('.wup-content .wup_line > a:not(.no_link)');
	if (!splinks.length) {
		return;
	}
	var sp = [];
	for (var i = 0, len = splinks.length; i < len; i++) {
		sp.push(splinks[i].textContent.toLowerCase());
	}
	ui_storage.set('charStoredPets',JSON.stringify(sp));
	ui_data.storedPets = sp;
};
/**
 * @private
 * @param {!GUIp.inventory.Model} inv
 * @returns {!Object<string, !Array<{type: string, bossAndLevel: string}>>}
 */
ui_improver._getBossParts = function(inv) {
	var result = {},
		itemName = '',
		part, parts;
	for (var i = 0, len = inv.length; i < len; i++) {
		if (!inv.isBossPart(i)) {
			continue;
		}
		itemName = inv.getName(i);
		part = ui_inventory.splitBossPart(itemName);
		if ((parts = result[part.type])) {
			parts.push(part);
		} else {
			result[part.type] = [part];
		}
		part.type = itemName; // reusing existing field
	}
	return result;
};
ui_improver.improveLab = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeLab = function(mutations,observer) {
			if (!container.querySelector('.bps_mf.wup_line')) {
				return;
			}
			// improve boss parts list
			var itm, bSrc, parts,
				enemyName = ui_stats.Enemy_Bossname(),
				bpLines = container.querySelectorAll('.wup_line.bps_line'),
				// `ui_inventory` is not initialized in duel modes so we force scanning
				available = ui_improver._getBossParts(ui_data.isDungeon ? ui_inventory.getInventory().md : ui_inventory.model),
				toggleHighlighting = function() {
					var bossName = this.textContent,
						highlighted = this.classList.toggle('e_match_part'),
						part;
					for (var i = 0, len = bpLines.length; i < len; i++) {
						if ((part = bpLines[i].getElementsByClassName('bps_val')[0])) {
							part.classList.toggle('e_match_part', highlighted && part.textContent === bossName);
						}
					}
				};
			for (var i = 0, len = bpLines.length; i < len; i++) {
				itm = bpLines[i].querySelector('div.bps_val');
				bSrc = itm.textContent;
				GUIp.common.addListener(itm, 'click', toggleHighlighting);
				if (ui_data.isBoss) {
					if (bSrc.startsWith(enemyName) || (bSrc.startsWith(enemyName, 5) && ui_stats.Enemy_HasAbility(/зовущий|summoning/))) {
						itm.classList.add('e_current_boss');
					}
				} else if ((parts = available[GUIp_i18n.bp_types[i]])) {
					for (var j = 0, jlen = parts.length; j < jlen; j++) {
						if (parts[j].bossAndLevel === bSrc) {
							continue; // exactly the same part; ignore
						}
						if (!itm.title) {
							itm.title = worker.GUIp_i18n.bp_others;
							itm.classList.add('e_new_part');
							GUIp.common.tooltips.watchSubtree(itm);
						}
						itm.title += '\n · ' + parts[j].type; // actually, this field contains item's name
					}
				}
			}
			// update logger with lab creatures
			ui_logger.update(ui_logger.labWatchers);
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeLab);
		observer.observe(container, {subtree: true, childList: true});
		observeLab(null, observer);
	}
};
ui_improver.improveForge = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeForge = function(mutations,observer) {
			if (!document.querySelector('.dm_map_header')) {
				return;
			}
			var fmap = document.getElementById('wrd_map');
			if (fmap)
			Array.from(fmap.children).forEach(function(row) {
				Array.from(row.children).forEach(function(cell) {
					switch (cell.textContent) {
						case '\uD83D\uDEAA': /*🚪*/ cell.classList.add('map_exit_pos_' + worker.GUIp_locale); break;
						case '\uD83D\uDCB0': /*💰*/ cell.classList.add('treasureChestForge'); break;
						case '\u2620':       /*☠*/ cell.classList.add('lesserBossForge'); break;
						case '\uD83D\uDC7E': /*👾*/ cell.classList.add('masterBossForge'); break;
						case '\uD83D\uDD73': /*🕳*/ /*cell.classList.add('trapForge');*/
						default: cell.classList.remove('masterBossForge');
					};
				});
			});
			ui_logger.update(ui_logger.forgeWatchers);
			ui_informer.updateCustomInformers();
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeForge);
		observer.observe(container, {subtree: true, childList: true, attributes: true, attributeFilter: ['class']});
		observeForge(null, observer);
	}
};
ui_improver.improveSparMenu = function() {
	var lines, names, content = document.getElementById('chf_popover_c');
	if (!content) {
		return;
	}
	lines = content.getElementsByClassName('chf_line');
	for (var i = 0, len = lines.length; i < len; i++) {
		names = lines[i].firstChild.textContent.match(/^(.+?) \((.+?)\)/);
		if (!names || names.length < 3) {
			continue;
		}
		lines[i].insertAdjacentHTML('beforeend','<div class="e_chfr"><a class="div_link em_font">[✉]</a><a class="div_link em_font">[➠]</a></div>');
		(function(name) {
			GUIp.common.addListener(lines[i].lastChild.firstChild, 'click', function(e) {
				ui_utils.openChatWith(name);
				e.stopPropagation();
			});
			lines[i].lastChild.firstChild.title = worker.GUIp_i18n.open_pchat;
			GUIp.common.addListener(lines[i].lastChild.lastChild, 'click', function(e) {
				worker.open("/gods/" + encodeURIComponent(name));
				e.stopPropagation();
			});
			lines[i].lastChild.lastChild.title = worker.GUIp_i18n.open_ppage;
		})(names[1]);
	}
};
ui_improver.improveCraftMenu = function() {
	var menus = document.getElementsByClassName('inv_craft_popover'),
		inv = ui_inventory.model,
		index = NaN,
		node, items;
	for (var i = 0, len = menus.length; i < len; i++) {
		node = menus[i];
		if (node.classList.contains('improved')) {
			continue;
		}
		node.classList.add('improved');
		// hide suggested items that cannot be crafted with
		items = node.getElementsByClassName('div_link');
		for (var j = 0, jlen = items.length; j < jlen; j++) {
			node = items[j];
			index = +inv.getItemIndex(node.textContent);
			// we need to check that the item is present in the inventory because Godville's craft menu is buggy
			if (index === index && !inv.isCraftable(index)) {
				node.classList.add('e_uncraftable_choice');
			}
		}
	}
};
ui_improver.calculateButtonsVisibility = function(forcedUpdate) {
	var i, j, len, baseCond = !ui_utils.isHidden(document.getElementById('godvoice') || {}) && !ui_storage.getFlag('Option:disableVoiceGenerators') && ui_stats.HP() > 0,
		isMonster = ui_stats.monsterName();
	if (!ui_data.isFight) {
		// inspect buttons
		var inspBtns = document.getElementsByClassName('inspect_button'),
			inspBtnsBefore = [], inspBtnsAfter = [];
		for (i = 0, len = inspBtns.length; i < len; i++) {
			inspBtnsBefore[i] = !inspBtns[i].classList.contains('hidden');
			inspBtnsAfter[i] = baseCond && !isMonster;
		}
		ui_improver.setButtonsVisibility(inspBtns, inspBtnsBefore, inspBtnsAfter);
		// craft buttons
		if (this.isFirstTime || forcedUpdate) {
			this.crftBtns = [document.getElementsByClassName('craft_button b_b')[0],
							 document.getElementsByClassName('craft_button b_r')[0],
							 document.getElementsByClassName('craft_button r_r')[0],
							 document.getElementsByClassName('craft_button span')[0],
							 document.getElementsByClassName('craft_group b_b')[0],
							 document.getElementsByClassName('craft_group b_r')[0],
							 document.getElementsByClassName('craft_group r_r')[0]
							];
			this.crftCustom = [[],[],[]];
			for (i = 0; i < 3; i++) {
				var ccrbs = this.crftBtns[i+4].getElementsByClassName('craft_button');
				for (j = 0, len = ccrbs.length; j < len; j++) {
					this.crftCustom[i].push(this.crftBtns.length);
					this.crftBtns.push(ccrbs[j]);
				}
			}
		}
		var crftBtnsBefore = [], crftBtnsAfter = [],
			crftFixed = ui_storage.getFlag('Option:fixedCraftButtons');
		for (i = 0, len = this.crftBtns.length; i < len; i++) {
			crftBtnsBefore[i] = !this.crftBtns[i].classList.contains('hidden');
			crftBtnsAfter[i] = !(!crftFixed && (!baseCond || isMonster));
		}
		crftBtnsAfter[0] = crftBtnsAfter[0] && ui_inventory.b_b.length;
		crftBtnsAfter[1] = crftBtnsAfter[1] && ui_inventory.b_r.length;
		crftBtnsAfter[2] = crftBtnsAfter[2] && ui_inventory.r_r.length;
		crftBtnsAfter[3] = crftBtnsAfter[0] || crftBtnsAfter[1] || crftBtnsAfter[2];

		for (i = 7, len = this.crftBtns.length; i < len; i++) {
			crftBtnsAfter[i] = crftBtnsAfter[i] && ui_inventory[this.crftBtns[i].id] && ui_inventory[this.crftBtns[i].id].length;
		}
		for (i = 0; i < 3; i++) {
			var crftGroupActive = false;
			for (j = 0, len = this.crftCustom[i].length; j < len; j++) {
				if (crftBtnsAfter[this.crftCustom[i][j]]) {
					crftGroupActive = true;
					break;
				}
			}
			crftBtnsAfter[i+4] = crftBtnsAfter[i+4] && crftGroupActive && crftBtnsAfter[i];
		}
		ui_improver.setButtonsVisibility(this.crftBtns, crftBtnsBefore, crftBtnsAfter);

		if (crftFixed) {
			for (i = 0, len = this.crftBtns.length; i < len; i++) {
				if (baseCond && !isMonster && !ui_data.inShop) {
					this.crftBtns[i].classList.remove('crb_inactive');
				} else {
					this.crftBtns[i].classList.add('crb_inactive');
				}
			}
		}
		// if we're in trader mode then try to mark some buttons as unavailable
		if (ui_data.hasShop) {
			ui_improver.switchButtonsForStore(!!ui_data.inShop);
		}
	}
	// voice generators
	if (this.isFirstTime) {
		this.voicegens = document.getElementsByClassName('voice_generator');
		this.voicegenClasses = [];
		for (i = 0, len = this.voicegens.length; i < len; i++) {
			this.voicegenClasses[i] = this.voicegens[i].className;
		}
	}
	var voicegensBefore = [], voicegensAfter = [],
		specialConds, specialClasses;
	if (!ui_data.isFight) {
		var isGoingBack = ui_stats.isGoingBack(),
			isTown = ui_stats.townName(),
			isSearching = ui_improver.detectors.stateGTF.res || ui_stats.lastNews().includes('дорогу'),
			isResting = /^(Переводит дух|Сушит вещи|Заканчивает перекур|Catching h.. breath|Drying h.. clothes)\.\.\.$/.test(ui_stats.lastNews()),
			dieIsDisabled = ui_storage.getFlag('Option:disableDieButton'),
			isFullGP = ui_stats.Godpower() === ui_stats.Max_Godpower(),
			isFullHP = ui_stats.HP() === ui_stats.Max_HP(),
			canQuestBeAffected = !/\((?:выполнено|completed|отменено|cancelled)\)/.test(ui_stats.Task_Name());
		specialClasses = ['heal', 'do_task', 'cancel_task', 'die', 'exp', 'dig', 'town', 'pray', 'sacrifice'];
		specialConds = [isMonster || isGoingBack || isTown || isSearching || isFullHP,				// heal
						isMonster || isGoingBack || isTown || isSearching || !canQuestBeAffected,	// do_task
																			 !canQuestBeAffected,	// cancel_task
						isMonster ||				isTown ||				 dieIsDisabled,			// die
						isMonster,																	// exp
						isMonster ||				isTown,											// dig
						isMonster || isGoingBack || isTown || isSearching || isResting,				// town
						isMonster ||										 isFullGP,				// pray
													isTown 											// sacrifice
					   ];
	}
	baseCond = baseCond && !worker.$('.r_blocked:visible').length;
	for (i = 0, len = this.voicegens.length; i < len; i++) {
		voicegensBefore[i] = !this.voicegens[i].classList.contains('hidden');
		voicegensAfter[i] = baseCond;
		if (baseCond && !ui_data.isFight) {
			for (var j = 0, len2 = specialConds.length; j < len2; j++) {
				if (specialConds[j] && this.voicegenClasses[i] && this.voicegenClasses[i].match(specialClasses[j])) {
					voicegensAfter[i] = false;
				}
			}
		}
	}
	ui_improver.setButtonsVisibility(this.voicegens, voicegensBefore, voicegensAfter);
};
ui_improver.setButtonsVisibility = function(btns, before, after) {
	for (var i = 0, len = btns.length; i < len; i++) {
		if (before[i] && !after[i]) {
			ui_utils.hideElem(btns[i], true);
		}
		if (!before[i] && after[i]) {
			ui_utils.hideElem(btns[i], false);
		}
	}
};
ui_improver.switchButtonsForStore = function(disable) {
	var activityButtons = document.querySelectorAll('#cntrl1 a, #cntrl2 a, a.voice_generator, a.craft_button, a.inspect_button');
	for (var i = 0, len = activityButtons.length; i < len; i++) {
		activityButtons[i].classList.toggle('crb_inactive', disable);
	}
	if (document.getElementById('voice_submit')) {
		document.getElementById('voice_submit').style.color = disable ? 'silver' : '';
	}
};
ui_improver.improveTownAbbrs = function(towns) {
	try {
		var ntname, abbr;
		for (var i = 0, len = towns.length; i < len; i++) {
			if (ntname = ui_words.base.town_list.find(function(a) { return a.abbr && a.name === towns[i].name; })) {
				abbr = towns[i].g.lastChild;
				if (abbr.textContent !== ntname.abbr) {
					abbr.textContent = ntname.abbr;
				}
			}
		}
	} catch(e) {}
};
ui_improver.nearbyTownsFix = function(onlyDistance) {
	if (!document.getElementById('hmap')) {
		return;
	}
	try {
		if (worker.GUIp_browser === 'Opera') {
			ui_improver.operaWMapFix();
		}
		var tmr, pos, town, towns, lval = document.querySelector('#hk_distance .l_val');
		pos = ui_stats.mileStones();
		towns = Array.from(document.querySelectorAll('#hmap_svg g.tl title, #hmap_svg g.sl title'),
				function(a) { var b = a.textContent.match(/^(.*?) \((\d+)\)/); return {name:b[1], dst:+b[2], g: a.parentNode}; })
				.sort(function(a,b) { return a.dst - b.dst; });
		if (!onlyDistance) {
			var switchTI = function(town) {
				if (ui_improver.informTown === town.name) {
					ui_improver.distanceInformerReset();
				} else if (!ui_improver.dailyForecast.get().includes('gvroads')) {
					ui_improver.distanceInformerSet(town);
				}
			};
			for (var i = 0, len = towns.length; i < len; i++) {
				if (!ui_utils.isAlreadyImproved(towns[i].g)) {
					GUIp.common.addListener(towns[i].g, 'mousedown', function(town, e) {
						if (e.which === 2) {
							switchTI(town);
							e.preventDefault();
							return;
						}
						tmr = GUIp.common.setTimeout(function() {
							switchTI(town);
							if (towns[0].g.parentNode) towns[0].g.parentNode.classList.add('block_once');
							tmr = 0;
						},1e3);
					}.bind(null, towns[i]));
					GUIp.common.addListener(towns[i].g, 'mouseup', function() {
						if (tmr) {
							worker.clearTimeout(tmr);
						}
					});
				}
				if (ui_improver.informTown === towns[i].name) {
					towns[i].g.classList.add('e_selected_town');
				}
			}
			if (towns.length && !towns[0].g.parentNode.classList.contains('improved')) {
				GUIp.common.addListener(towns[0].g.parentNode, 'click', function(e) {
					if (this.classList.contains('block_once')) {
						this.classList.remove('block_once');
						e.stopPropagation();
					}
				}, true);
				towns[0].g.parentNode.classList.add('improved');
			}
			if (ui_storage.getFlag('Option:improveTownAbbrs')) {
				ui_improver.improveTownAbbrs(towns);
			}
		}
		town = towns.filter(function(b) { return b.dst <= pos; }).pop();
		if (!lval.title.includes(town.name)) {
			lval.title = worker.GUIp_i18n.fmt('nearby_town', town.name, town.dst);
		}
	} catch(e) {}
};
ui_improver.operaWMapFix = function() {
	if (ui_improver.operaWMapObserver) {
		ui_improver.operaWMapObserver.disconnect();
	} else {
		ui_improver.operaWMapObserver = GUIp.common.newMutationObserver(function(mutations) {
			var f = false;
			for (i = 0, len = mutations.length; i < len; i++) {
				if (mutations[i].attributeName === 'd' || mutations[i].attributeName === 'to' && mutations[i].target.nodeName === 'animateTransform' && mutations[i].target.getAttribute('type') === 'translate') {
					mutations[i].target.removeAttribute('oprfix');
					GUIp.common.debug('removing oprfix from "' + mutations[i].attributeName + '"')
					f = true;
				}
			}
			if (f) {
				ui_improver.operaWMapFix();
			}
		});
	}
	// info popups positioned at the bottom most of the time are glitched and not repainted properly, placing them at the top kinda "resolves" this issue
	var hinfos = document.querySelectorAll('.hmap_info');
	for (var i = 0, len = hinfos.length; i < len; i++) {
		hinfos[i].style.bottom = 'inherit';
		hinfos[i].style.top = '0';
	}
	// if the map is placed at central column then it's not required to apply rescaling to it
	if (document.querySelector('.c_col #hmap')) {
		return;
	}
	var svg = document.getElementById('hmap_svg');
	var hscale = function(a) { return (a*0.65).toFixed(2); }
	var paths = svg.querySelectorAll('path');
	for (var j = 0, i = 0; i < paths.length; i++) {
		if (paths[i].getAttribute('d') && !paths[i].getAttribute('oprfix')) {
			paths[i].setAttribute('d',paths[i].getAttribute('d').replace(/\d+(\.\d+)?/g,hscale));
			paths[i].setAttribute('oprfix','1');
			j++;
		}
		if (paths[i].classList.contains('tmap')) {
			paths[i].style.strokeWidth = hscale(2);
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' paths');
	var gs = svg.querySelectorAll('g[transform^="translate"]');
	for (var j = 0, i = 0, len = gs.length; i < len; i++) {
		if (!gs[i].getAttribute('oprfix')) {
			gs[i].setAttribute('transform',gs[i].getAttribute('transform').replace(/\d+(\.\d+)?/g,hscale));
			gs[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' gs');
	var circles = svg.querySelectorAll('circle');
	for (var j = 0, i = 0, len = circles.length; i < len; i++) {
		if (!circles[i].getAttribute('oprfix')) {
			circles[i].setAttribute('r',hscale(circles[i].getAttribute('r')));
			circles[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' circles');
	var texts = svg.querySelectorAll('text');
	for (var j = 0, i = 0, len = texts.length; i < len; i++) {
		if (!texts[i].getAttribute('oprfix')) {
			texts[i].setAttribute('dy',hscale(texts[i].getAttribute('dy')));
			texts[i].setAttribute('font-size',hscale(texts[i].getAttribute('font-size')));
			texts[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' texts');
	var anims = svg.querySelectorAll('animateTransform[type="translate"]');
	for (var j = 0, i = 0, len = anims.length; i < len; i++) {
		if (!anims[i].getAttribute('oprfix')) {
			anims[i].setAttribute('from',anims[i].getAttribute('from').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('to',anims[i].getAttribute('to').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' anims');
	svg.style.transform = 'scale(1)';
	ui_improver.operaWMapObserver.observe(svg,{ subtree: true, attributes: true, attributeFilter: ['d','to']});
};
ui_improver.thirdEyePositionFix = function() {
	var owtop, oatop, wtop, atop,
		content = document.querySelector('.wup.in .tep'),
		wup = document.querySelector('.wup.in');
	try {
		owtop = wup.style.top;
		oatop = wup.firstChild.style.top;
		wup.style.top = '-2000px';
		ui_utils.hideElem(wup,false);
		content.style.maxHeight = (worker.innerHeight - 150) + 'px';
		wtop = worker.innerHeight + worker.scrollY - wup.clientHeight - 75;
		atop = document.getElementById('imp_button').getBoundingClientRect().top + worker.scrollY - wtop + 11;
		if (atop < 50) {
			wtop -= 50 - atop;
			atop = 50;
		}
		if (atop > wup.clientHeight) {
			wtop += atop - wup.clientHeight + 10;
			atop -= atop - wup.clientHeight + 15;
		}
		if (wtop < 0) wtop = 0;
		wup.firstChild.style.top = atop + 'px';
		wup.style.top = wtop + 'px';
	} catch (e) {
		wup && wup.firstChild && (wup.firstChild.style.top = oatop);
		wup && (wup.style.top = owtop);
	}
};
ui_improver.chatsFix = function() {
	var chats = document.getElementsByClassName('frDockCell'),
		len = chats.length;
	// fix overlapping issues when opening a huge number of chats
	for (var i = 0; i < len; i++) {
		var chat = chats[i];
		chat.classList.remove('left');
		chat.style.zIndex = len - i;
		if (chat.getBoundingClientRect().right < 350) {
			chat.classList.add('left');
		}
	}
	// padding for page settings link
	var paddingBottom = len ? chats[0].getBoundingClientRect().bottom - chats[len - 1].getBoundingClientRect().top : worker.GUIp_browser === 'Opera' ? 27 : 0,
		isBottom = worker.scrollY >= document.documentElement.scrollHeight - document.documentElement.clientHeight - 10;
	paddingBottom = Math.floor(paddingBottom*10)/10 + 10;
	paddingBottom = paddingBottom < 0 ? 0 : paddingBottom + 'px';
	document.getElementsByClassName('reset_layout')[0].style.paddingBottom = paddingBottom;
	if (isBottom) {
		worker.scrollTo(0, document.documentElement.scrollHeight - document.documentElement.clientHeight);
	}
};
ui_improver.improveChat = function() {
	var chats = document.getElementsByClassName('chat_ph')[0];
	if (!chats || ui_utils.isAlreadyImproved(chats)) {
		return;
	}
	// by default, guildmate's name is appended to the end of the message when clicked.
	// we insert it into current cursor's position instead
	GUIp.common.addListener(chats, 'click', function(ev) {
		var node = ev.target;
		// ignore unless a godname is clicked
		if (!node.classList.contains('gc_fr_god')) {
			return;
		}
		var godName = node.textContent;
		// find the textarea
		while (!(node = node.parentNode).classList.contains('frMsgBlock')) { }
		node = node.querySelector('.frInputArea textarea');
		// insert godname into it
		var text = node.value,
			pos = node.selectionDirection === 'backward' ? node.selectionStart : node.selectionEnd;
		node.value = text.slice(0, pos) + '@' + godName + ', ' + text.slice(pos);
		node.focus();
		node.selectionStart = node.selectionEnd = pos + godName.length + 3;
		// avoid executing original code
		ev.stopPropagation();
	}, true);
};
ui_improver._getChatMsgText = function(msg) {
	var author = msg.getElementsByClassName('gc_fr_god')[0],
		result = author ? author.textContent : '',
		classes;
	// there might be hyperlinks in the message
	for (var child = msg.firstChild; child && !((classes = child.classList) && classes.contains('fr_msg_meta')); child = child.nextSibling) {
		result += child.textContent;
	}
	return result;
};
ui_improver._fixChatScrolling = function(msgArea) {
	var msgs = msgArea.getElementsByClassName('fr_msg_l'),
		latestMsgText = '',
		latestMsgOffset = 0,
		scroll = msgArea.scrollTop;
	if (msgs.length) {
		latestMsgText = this._getChatMsgText(msgs[msgs.length - 1]);
		latestMsgOffset = msgs[msgs.length - 1].offsetTop;
	}

	msgArea.addEventListener('scroll', function() {
		// executed without a try-catch wrapper for speed; extra careful here
		scroll = this.scrollTop;
	});

	GUIp.common.newMutationObserver(function() {
		var i = msgs.length - 1;
		if (i < 0) return;
		var j = i,
			newLatestMsg = msgs[i],
			newLatestMsgText = ui_improver._getChatMsgText(newLatestMsg),
			latestMsg = newLatestMsg;
		// look for the message we've seen the last time
		if (newLatestMsgText !== latestMsgText) {
			while (i-- && ui_improver._getChatMsgText((latestMsg = msgs[i])) !== latestMsgText) { }
			latestMsgText = newLatestMsgText;
		}

		var newLatestMsgOffset = newLatestMsg.offsetTop;
		if (i < 0 || (j - i <= 5 && scroll >= latestMsgOffset + latestMsg.offsetHeight - msgArea.offsetHeight - 3)) {
			// we've lost it or were already at the bottom; scroll down to the bottom
			// but guild council preserves its scrolling when more than 5 messages arrive at once, so do we
			msgArea.scrollTop = 2147483647;
		} else {
			// restore scrolling with respect to new messages' height
			msgArea.scrollTop = scroll + (latestMsg.offsetTop - latestMsgOffset);
		}
		latestMsgOffset = newLatestMsgOffset;
	}).observe(msgArea, {childList: true, subtree: true});
};
ui_improver.processNewChat = function(chat) {
	var node = chat.getElementsByClassName('dockfrname')[0],
		key, saved, header;
	if (!node || !(key = node.textContent) || !(node = chat.querySelector('.frInputArea textarea'))) {
		return;
	}

	key = 'PM:' + key;
	if ((saved = ui_tmpstorage.get(key))) {
		node.value = saved;
		node.dispatchEvent(new Event('change', {bubbles: true})); // ask Godville to resize the textarea
		if (node.disabled) {
			// Guild Council window is initially disabled, and when it gets enabled, the text is cleared
			GUIp.common.newMutationObserver(function(mutations, observer) {
				if (!node.disabled) {
					node.value = saved;
					observer.disconnect();
				}
			}).observe(node, {attributes: true, attributeFilter: ['disabled']});
		}
	}

	var saveText = function() {
		var text = node.value;
		if (text) {
			ui_tmpstorage.set(key, text);
		} else {
			ui_tmpstorage.remove(key);
		}
	};
	if (GUIp_browser !== 'Opera') {
		GUIp.common.addListener(node, 'blur', saveText);
	} else {
		// beforeunload doesn't fire in Opera, so we need to save the text periodically
		var oprTimer = 0;
		GUIp.common.addListener(node, 'blur', function() {
			saveText();
			worker.clearTimeout(oprTimer);
			oprTimer = 0;
		});
		var oprTimerCallback = function() {
			saveText();
			oprTimer = 0;
		};
		node.addEventListener('keydown', function() {
			// executed without a try-catch wrapper for speed; extra careful here
			if (!oprTimer) {
				oprTimer = GUIp.common.setTimeout(oprTimerCallback, 3e3);
			}
		});
	}

	// wait until chat is loaded
	if ((header = chat.getElementsByClassName('fr_chat_header')[0])) {
		ui_utils.observeUntil(header, {characterData: true, childList: true}, function() {
			return header.textContent.trim() || undefined;
		}).then(function(friendName) {
			var pos = friendName.search(/ и е(?:го|ё) | and h[ie][sr] hero/);
			if (pos >= 0) {
				friendName = friendName.slice(0, pos);
			}
			// hide desktop notification when opening the chat
			// unfortunately, .dockfrname contains just abbreviated name
			ui_utils.hideNotification('pm_' + friendName);
			GUIp.common.addListener(node, 'focus', ui_utils.hideNotification.bind(ui_utils, 'pm_' + friendName));
			// scroll position is reset every time a new message appears. try to workaround that
			var msgArea = chat.getElementsByClassName('frMsgArea')[0];
			if (msgArea) {
				ui_improver._fixChatScrolling(msgArea);
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};
ui_improver.checkGCMark = function(csource) {
	var gc_tab = document.querySelector('.msgDock.frDockCell.frbutton_pressed .dockfrname');
	if (gc_tab && gc_tab.textContent.match(/Гильдсовет|Guild Council/) && gc_tab.parentNode.getElementsByClassName('fr_new_msg').length) {
		worker.$('.frbutton_pressed textarea').triggerHandler('focus');
	}
};
ui_improver.createWakelock = function() {
	var wakelockSwitch = document.createElement('div');
	wakelockSwitch.id = 'wakelock_switch';
	wakelockSwitch.textContent = '\uD83D\uDCA1'; // light bulb
	GUIp.common.addListener(wakelockSwitch, 'click', function() { ui_improver.switchWakelock(); });
	// we need to re-enable a modern wakelock on each visibility state change to visible
	if (worker.navigator.wakeLock) {
		GUIp.common.addListener(document, 'visibilitychange', function() {
			if (document.visibilityState === "visible" && ui_improver.wakeLock === null && ui_storage.getFlag('wakelockEnabled')) {
				ui_improver.switchWakelock(true);
			}
		});
	}
	document.getElementById('main_wrapper').appendChild(wakelockSwitch);
	ui_improver.wakeLock = null;
	if (ui_storage.getFlag('wakelockEnabled')) {
		ui_improver.switchWakelock(true);
	}
};
ui_improver.switchWakelock = function(enable) {
	var wakelockSwitch = document.getElementById('wakelock_switch'),
		wakelockRemoved = function() {
			ui_improver.wakeLock = null;
			wakelockSwitch.classList.remove('wakelock_enabled');
		};
	if (!ui_improver.wakeLock || enable) {
		if (worker.navigator.wakeLock) {
			// modern request
			worker.navigator.wakeLock.request("screen").then(function (wakeLock) {
				GUIp.common.debug('modern wakelock active.');
				ui_improver.wakeLock = wakeLock;
				GUIp.common.addListener(ui_improver.wakeLock, 'release', function() {
					GUIp.common.debug('modern wakelock released.');
					wakelockRemoved();
				});
				ui_storage.set('wakelockEnabled',true);
				wakelockSwitch.classList.add('wakelock_enabled');
			}).catch(function(e) { GUIp.common.error('error trying to enable modern wakelock:',e) });
		} else {
			// legacy firefox
			ui_improver.wakeLock = worker.navigator.requestWakeLock('screen');
			if (ui_improver.wakeLock) {
				ui_storage.set('wakelockEnabled',true);
				wakelockSwitch.classList.add('wakelock_enabled');
			}
		}
	} else {
		if (ui_improver.wakeLock.release) {
			ui_improver.wakeLock.release();
		} else if (ui_improver.wakeLock.unlock) {
			ui_improver.wakeLock.unlock();
			wakelockRemoved();
		}
		ui_storage.set('wakelockEnabled',false);
	}
};
ui_improver.initTouchDragging = function() {
	Array.prototype.forEach.call(document.getElementsByClassName('b_handle'), ui_dragger.register);
};
ui_improver.activity = function() {
	if (!ui_logger.updating) {
		ui_logger.updating = true;
		GUIp.common.setTimeout(function() {
			ui_logger.updating = false;
		}, 500);
		ui_logger.update();
	}
};
