var ui_mining = GUIp.mining = {};
(function() {

//! include './defs.js';
//! include './analysis.js';
//! include './actions.js';

ui_mining.init = actions.init;
ui_mining.processMap = actions.processMap;
ui_mining.processStepChange = actions.processStepChange;

})(); // ui_mining
