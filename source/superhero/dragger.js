// ui_dragger
var ui_dragger = GUIp.dragger = {};

ui_dragger.init = function() {
	ui_dragger._onMouseDown = GUIp.common.try2.bind(null, ui_dragger._onMouseDown);
	ui_dragger._onContextMenu = GUIp.common.try2.bind(null, ui_dragger._onContextMenu);
	ui_dragger._onMouseUp = GUIp.common.try2.bind(null, ui_dragger._onMouseUp);
};

ui_dragger._$target = null;
ui_dragger._timer = 0;
ui_dragger._finishing = false;

/** @const */
ui_dragger._mouseEventProps = [
	'altKey', 'clientX', 'clientY', 'ctrlKey', 'fromElement', 'layerX', 'layerY', 'metaKey', 'movementX', 'movementY',
	'offsetX', 'offsetY', 'pageX', 'pageY', 'relatedTarget', 'screenX', 'screenY', 'shiftKey', 'toElement', 'x', 'y'
];

/**
 * @private
 * @param {!MouseEvent} original
 * @param {!Object} fake
 * @returns {!MouseEvent}
 */
ui_dragger._forgeMouseEvent = function(original, fake) {
	var key = '';
	for (var i = 0, len = ui_dragger._mouseEventProps.length; i < len; i++) {
		key = ui_dragger._mouseEventProps[i];
		fake[key] = original[key];
	}
	return fake;
};

/**
 * @private
 * @param {!MouseEvent} ev
 * @param {number} dx
 * @param {number} dy
 * @returns {!MouseEvent}
 */
ui_dragger._adjustEventCoords = function(ev, dx, dy) {
	ev.clientX += dx;
	ev.clientY += dy;
	ev.screenX += dx;
	ev.screenY += dy;
	if (typeof ev.layerX === 'number') {
		ev.layerX += dx;
		ev.layerY += dy;
	}
	if (typeof ev.movementX === 'number') {
		ev.movementX += dx;
		ev.movementY += dy;
	}
	if (typeof ev.offsetX === 'number') {
		ev.offsetX += dx;
		ev.offsetY += dy;
	}
	if (typeof ev.pageX === 'number') {
		ev.pageX += dx;
		ev.pageY += dy;
	}
	if (typeof ev.x === 'number') {
		ev.x += dx;
		ev.y += dy;
	}
	return ev;
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._start = function(ev) {
	ui_dragger._finishing = false;
	ui_dragger._$target = $(ev.target);
	// emulate holding left button
	ui_dragger._$target.trigger(ui_dragger._forgeMouseEvent(ev, {type: 'mousedown', button: 0, buttons: 0x1, which: 1}));
	// start hooking clicks
	worker.addEventListener('mousedown', ui_dragger._onMouseDown, true);
	worker.addEventListener('contextmenu', ui_dragger._onContextMenu, true);
	worker.addEventListener('mouseup', ui_dragger._onMouseUp, true);
	ui_dragger._timer = GUIp.common.setTimeout(function() {
		ui_dragger._timer = 0;
		// emulate a mouse move so that visuals are shown
		ui_dragger._$target.trigger(ui_dragger._adjustEventCoords(
			ui_dragger._forgeMouseEvent(ev, {type: 'mousemove', button: 0, buttons: 0x1, which: 0}),
			12, 12
		));
	}, 125); // Godville ignores 'mousemove' for the first 100ms
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._finish = function(ev) {
	// stop hooking clicks
	worker.removeEventListener('mousedown', ui_dragger._onMouseDown, true);
	worker.removeEventListener('contextmenu', ui_dragger._onContextMenu, true);
	worker.removeEventListener('mouseup', ui_dragger._onMouseUp, true);
	// emulate releasing left button
	ui_dragger._$target.trigger(ui_dragger._forgeMouseEvent(ev, {type: 'mouseup', button: 0, buttons: 0x1, which: 1}));
	ui_dragger._$target = null;
	if (ui_dragger._timer) {
		clearTimeout(ui_dragger._timer);
		ui_dragger._timer = 0;
	}
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._onMouseDown = function(ev) {
	// allow only right mouse button to be clicked
	if (ev.button === 2) {
		ui_dragger._finishing = true;
	} else {
		ev.preventDefault();
		ev.stopPropagation();
	}
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._onContextMenu = function(ev) {
	// the user has performed a long tap (i.e., has clicked right mouse button)
	ev.preventDefault();
	// prevent mobile browser from selecting the word that is being tapped
	worker.getSelection().removeAllRanges();
	ui_dragger._finish(ev);
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._onMouseUp = function(ev) {
	// Godville stops dragging as soon as any button is released, not only left one. unfortunately, right mouse button
	// is released just after we enter drag mode. so we need to block it, too.
	if (ui_dragger._finishing && ev.button === 2) {
		// 'contextmenu' fires before 'mouseup', and we remove our listeners in `_finish`, so we shouldn't get here.
		// if, however, 'contextmenu' is somehow skipped and the control has reached this point, we'd better stop doing
		// what we are doing.
		GUIp.common.warn('touch dragger has not recevied "contextmenu" event');
		ui_dragger._finish(ev);
	} else {
		ev.stopPropagation();
	}
};

/**
 * @private
 * @param {!MouseEvent} ev
 */
ui_dragger._enterDragMode = function(ev) {
	if (!GUIp.common.isTouching || ui_dragger._$target) return;
	ev.preventDefault();
	// prevent mobile browser from selecting the word that is being tapped
	worker.getSelection().removeAllRanges();
	ui_dragger._start(ev);
};

/**
 * @param {!Element} draggable
 */
ui_dragger.register = function(draggable) {
	GUIp.common.addListener(draggable, 'contextmenu', ui_dragger._enterDragMode);
};
