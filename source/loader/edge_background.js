(function(window) {
'use strict';

//! include './xhrs.js';

chrome.runtime.onMessage.addListener(function(erinomeMessage, sender, sendResponse) {
	console.log(sender.tab ? 'from a content script:' + sender.url : 'from the extension');
	switch (erinomeMessage.type) {
		case 'webxhr':
			xhrs.processWebXHR(erinomeMessage, sendResponse);
			break;
		case 'playsound':
			try {
				var audio = new Audio(erinomeMessage.content);
				audio.play();
			} catch (e) {};
			break;
	}
	return true;
});

})(this);
