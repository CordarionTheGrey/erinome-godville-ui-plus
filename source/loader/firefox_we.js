(function(window) {
'use strict';

//! include './utils.js';
//! include './xhrs.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = browser.extension.getURL('');
localStorage.setItem('eGUI_prefix', prefix);

modules.MODULES.browser = {src: 'guip_chrome.js'};

var messageSender = browser.runtime.sendMessage.bind(browser.runtime);

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr: xhrs.processWebXHR,
	playsound: messageSender
}));

// prevent firefox-we from re-adding extension scripts on every install/update/(re)enable
if (!document.querySelector('script[id^="godville-ui-plus-"]')) {
	initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix), dom.createFontLoader(prefix));
}

})(this);
