/** @namespace */
var modules = {};

/**
 * @typedef {Object} GUIpModule
 * @property {(string|undefined)} name
 * @property {string} src
 * @property {(boolean|undefined)} initialized
 * @property {(!Array<string>|undefined)} deps
 * @property {(number|undefined)} unsatisfied
 * @property {(!Array<!GUIpModule>|undefined)} pending
 */

/** @type {!Object<string, !GUIpModule>} */
modules.MODULES = {
	// 3rd-party libraries:
	base64: {src: 'base64.min.js'},
	jsep: {src: 'jsep.min.js'},
	pako: {src: 'pako_deflate.min.js'},

	common: {src: 'common.js'},
	phrasesRu: {
		src: 'phrases_ru.js',
		deps: ['browser', 'common']
	},
	phrasesEn: {
		src: 'phrases_en.js',
		deps: ['browser', 'common']
	},
	omap: {src: 'omap.js'},
	superhero: {
		src: 'superhero.js',
		deps: ['browser', 'common', 'i18n', 'base64', 'jsep', 'pako']
	},
	optionsPage: {src: 'options_page.js'},
	options: {
		src: 'options.js',
		deps: ['browser', 'common', 'i18n', 'optionsPage', 'jsep']
	},
	forum: {
		src: 'forum.js',
		deps: ['browser', 'common', 'i18n', 'omap']
	},
	log: {
		src: 'log.js',
		deps: ['browser', 'common', 'i18n', 'omap']
	}
};

modules.init = function() {
	utils.postErinomeMessageTo('*', {type: 'initModule', which: this.name});
	// we expect to receive a message in response
};

modules.onInit = function() {
	this.initialized = true;
	if (!this.pending) { return; }
	// notify other modules that we are ready
	for (var i = 0, len = this.pending.length; i < len; i++) {
		if (!--this.pending[i].unsatisfied) {
			modules.init.call(this.pending[i]);
		}
	}
	delete this.pending;
};

modules.onLoad = function() {
	if (!this.deps) {
		// no dependencies - no need for complex initialization logic in the module
		modules.onInit.call(this);
		return;
	}
	this.unsatisfied = this.deps.reduce(function(count, name) {
		var dep = modules.MODULES[name];
		if (dep.initialized) {
			// fine, do nothing
			return count;
		}
		// subscribe for this dependency's initialization event
		if (dep.pending) {
			dep.pending.push(this);
		} else {
			dep.pending = [this];
		}
		return count + 1;
	}.bind(this), 0);
	if (!this.unsatisfied) {
		modules.init.call(this);
	} // otherwise, wait until all the dependencies get initialized
};

/**
 * @param {!Array<string>} moduleNames
 * @param {function(string, function())} loadModule
 */
modules.load = function(moduleNames, loadModule) {
	var requested = {}, initialLength = moduleNames.length;
	for (var i = 0, len = initialLength; i < len; i++) {
		var name = moduleNames[i];
		if (name in requested) { continue; }
		requested[name] = true;

		var mod = modules.MODULES[name];
		mod.name = name;
		loadModule(mod.src, modules.onLoad.bind(mod));
		if (mod.deps) {
			for (var j = 0, jlen = mod.deps.length; j < jlen; j++) {
				if (!(mod.deps[j] in requested)) {
					moduleNames[len++] = mod.deps[j];
				}
			}
		}
	}
	moduleNames.length = initialLength;
};
