(function(window) {
'use strict';

//! include './utils.js';
//! include './xhrs.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = localStorage.eGUI_prefix = chrome.extension.getURL('');

modules.MODULES.browser = {src: 'guip_chrome.js'};

// Chrome 49 (the last available for XP) doesn't natively support Object.values
if (!Object.values) {
	modules.MODULES.object = {src: 'Object.js'};
	modules.MODULES.phrasesRu.deps.push('object');
	modules.MODULES.phrasesEn.deps.push('object');
}

var port = null;

var sendMessage = function(msg) {
	if (!port) {
		port = chrome.runtime.connect();
		port.onMessage.addListener(utils.postErinomeMessageTo.bind(null, '*'));
	}
	port.postMessage(msg);
};

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr: xhrs.processWebXHR,
	playsound: sendMessage,
	makefocus: sendMessage,
	notify: sendMessage,
	notifyHide: sendMessage
}));
initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix), dom.createFontLoader(prefix));

})(this);
