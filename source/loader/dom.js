/** @namespace */
var dom = {};

/**
 * @param {string} type
 * @param {string} id
 * @returns {!Element}
 */
dom.createElement = function(type, id) {
	var elem = document.createElement(type);
	elem.id = 'godville-ui-plus-' + id;
	elem.charset = 'UTF-8';
	return elem;
};

/**
 * @param {string} id
 * @param {string} src
 * @returns {!HTMLScriptElement}
 */
dom.createScript = function(id, src) {
	var script = dom.createElement('script', id);
	script.type = 'text/javascript';
	script.src = src;
	return script;
};

/**
 * @param {string} id
 * @param {string} href
 * @returns {!HTMLLinkElement}
 */
dom.createCSSLink = function(id, href) {
	var link = dom.createElement('link', id);
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = href;
	return link;
};

/**
 * @param {string} id
 * @param {string} fontFamily
 * @param {string} src
 * @returns {!HTMLStyleElement}
 */
dom.createFontFaceStyle = function(id, fontFamily, src) {
	var style = dom.createElement('style', id);
	style.type = 'text/css';
	// `font-display` is used to silence Chrome's warning: https://www.chromestatus.com/feature/5636954674692096
	// this warning makes no sense since our font is effectively local.
	style.textContent =
		'@font-face { font-family: "' + fontFamily + '"; src: url("' + src + '"); font-display: swap; }';
	return style;
};

/**
 * @param {string} prefix
 * @returns {function(string, function())}
 */
dom.createModuleLoader = function(prefix) {
	var serial = 0;
	return function(src, onload) {
		var script = dom.createScript(String(serial++), prefix + src);
		script.onload = onload;
		document.head.appendChild(script);
	};
};

/**
 * @param {string} prefix
 * @returns {function(string)}
 */
dom.createStyleSheetLoader = function(prefix) {
	var serial = 0;
	return function(href) {
		document.head.appendChild(dom.createCSSLink('css-' + serial++, prefix + href));
	};
};

/**
 * @param {string} prefix
 * @returns {function(string, string)}
 */
dom.createFontLoader = function(prefix) {
	var serial = 0;
	return function(fontFamily, src) {
		document.head.appendChild(dom.createFontFaceStyle('font-' + serial++, fontFamily, prefix + src));
	};
};
