(function(window) {
'use strict';

//! include './utils.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = window.chrome.extension.getURL('');
localStorage.setItem('eGUI_prefix', prefix);

modules.MODULES.browser = {src: 'guip_chrome.js'};

var messageSender = chrome.runtime.sendMessage.bind(chrome.runtime);

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr:    messageSender,
	playsound: messageSender
}));
initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix), dom.createFontLoader(prefix));

})(this);
