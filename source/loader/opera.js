(function(window) {
'use strict';

//! include './utils.js';
//! include './modules.js';
//! include './initializer.js';

/**
 * @param {string} tag
 * @param {string} type
 * @param {(string|number)} id
 * @param {string} text
 */
var injectResource = function(tag, type, id, text) {
	var elem = document.createElement(tag);
	elem.type = type;
	elem.id = 'godville-ui-plus-' + id;
	elem.textContent = text;
	document.head.appendChild(elem);
};

/**
 * @param {(string|number)} id
 * @param {string} fontFamily
 * @param {string} fontData
 */
var injectFontFaceStyle = function(id, fontFamily, fontData) {
	injectResource('style', 'text/css', id,
		'@font-face { font-family: "' + fontFamily + '"; src: url("' + fontData + '"); }'
	);
};

/**
 * @this {function(this: !FileReader, string)}
 * @param {string} path
 * @param {function(string)} inject
 * @param {?function()} [onload]
 */
var loadResource = function(path, inject, onload) {
	var file = opera.extension.getFile('/content/' + path);
	if (file) {
		var reader = new FileReader;
		reader.onload = function() {
			inject(reader.result);
			if (onload) {
				onload();
			}
		};
		this.call(reader, file);
	}
};

var loadTextResource = loadResource.bind(FileReader.prototype.readAsText);
var loadBinResource  = loadResource.bind(FileReader.prototype.readAsDataURL);

/**
 * @returns {function(string, function())}
 */
var createModuleLoader = function() {
	var serial = 0;
	return function(src, onload) {
		loadTextResource(src, injectResource.bind(null, 'script', 'text/javascript', serial++), onload);
	};
};

/**
 * @returns {function(string)}
 */
var createStyleSheetLoader = function() {
	var serial = 0;
	return function(href) {
		loadTextResource(href, injectResource.bind(null, 'style', 'text/css', 'css-' + serial++));
	};
};

/**
 * @returns {function(string, string)}
 */
var createFontLoader = function() {
	var serial = 0;
	return function(fontFamily, src) {
		loadBinResource(src, injectFontFaceStyle.bind(null, 'font-' + serial++, fontFamily));
	};
};

modules.MODULES.browser  = {src: 'guip_opera.js'};
modules.MODULES.array    = {src: 'Array.js'};
modules.MODULES.domTokLs = {src: 'DOMTokenList.js'};
modules.MODULES.number   = {src: 'Number.js'};
modules.MODULES.object   = {src: 'Object.js'};
modules.MODULES.string   = {src: 'String.js'};
modules.MODULES.weakMap  = {src: 'WeakMap.js'};
modules.MODULES.mutObserver = {src: 'MutationObserver.js'};

modules.MODULES.phrasesRu.deps.push('array', 'domTokLs', 'number', 'object', 'string');
modules.MODULES.phrasesEn.deps.push('array', 'domTokLs', 'number', 'object', 'string');
modules.MODULES.superhero.deps.push('weakMap', 'mutObserver');
modules.MODULES.forum.deps.push('weakMap', 'mutObserver');
modules.MODULES.log.deps.push('weakMap', 'mutObserver');

if (/godville\.net|godvillegame\.com|gdvl\.tk|gvg?\.erinome\.net/.test(window.location.host)) {
	window.opera.defineMagicFunction('GUIp_getResourceInternal', function(real, thisObject, url) {
		return opera.extension.getFile('/content/' + url);
	});
	var doInit = function() {
		window.removeEventListener('DOMContentLoaded', doInit);
		initializer.registerErinomeListener(initializer.createErinomeListener({}));
		initializer.init(createModuleLoader(), createStyleSheetLoader(), createFontLoader());
	};
	if (document.readyState === 'loading') {
		window.addEventListener('DOMContentLoaded', doInit);
	} else {
		doInit();
	}
}

})(window); // note: this !== window in Opera
