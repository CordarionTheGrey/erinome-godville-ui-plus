/** @namespace */
var xhrs = {};

xhrs.processXHR = function() {
	//! include 'mixins/xhr.js';

	return processXHR;
}();

/**
 * @param {!Object} msg
 * @param {function(!Object)} callback
 */
xhrs.processWebXHR = function(msg, callback) {
	var handler = function(cid, xhr) {
		callback({
			type: 'webxhrResponse',
			cid: cid,
			scid: this.scid,
			fcid: this.fcid,
			xhr: {
				status: xhr.status,
				responseText: xhr.responseText,
				lastModified: xhr.getResponseHeader('Last-Modified')
			}
		});
	};
	xhrs.processXHR(
		msg.url,
		msg.method || 'GET',
		msg.data,
		msg.encoding,
		handler.bind(msg, msg.scid),
		handler.bind(msg, msg.fcid)
	);
};
