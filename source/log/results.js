/**
 * @param {string} msg
 * @param {string} heroName
 * @returns {boolean}
 */
ui_log.parseSparResult = function(msg, heroName) {
	var pos = msg.search(/ получает порцию опыта| gets experience points for today/i);
	return pos >= 0 && msg.endsWith(heroName, pos);
};

/**
 * @param {string} msg
 * @param {!Object<string, string>} names
 * @returns {number}
 */
ui_log.parseDungeonResult = function(msg, names) {
	var heroName = names[this.godname],
		otherNames = Object.values(names),
		endPos = 0,
		pos, logs;
	while ((pos = msg.indexOf(heroName, endPos)) >= 0) {
		pos += heroName.length;
		endPos = Math.min.apply(null, otherNames.map(function(name) {
			var i = msg.indexOf(name, pos);
			return i >= 0 ? i : msg.length;
		}));
		if ((logs = msg.slice(pos, endPos).match(/бревно для ковчега|ещё одно бревно|log for the ark/gi))) {
			return logs.length;
		}
	}
	return 0;
};

/**
 * @param {!GUIp.common.activities.Activity} act
 */
ui_log.addActivity = function(act) {
	if (act.date < Date.now() - GUIp.common.activities.storageTime) {
		return;
	}
	var activities = GUIp.common.activities.load(this.storage),
		existing = activities.find(function(existing) { return existing.logID === act.logID; });
	if (!existing) {
		activities.push(act);
	} else if (existing.result < 0) {
		existing.result = act.result;
		// should not update date here, since we might have inaccurate one
	} else {
		if (existing.result !== act.result) {
			GUIp.common.warn('activity result mismatch:', existing.result, '!=', act.result);
		}
		return;
	}
	GUIp.common.activities.save(this.storage, activities.sort(function(a, b) { return a.date - b.date; }));
};

ui_log.saveSparResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroName = this.getArenaHeroName('hero1_info') || this.getArenaHeroName('hero2_info');
	if (!entry || !heroName) return;
	this.addActivity({
		type: 'spar',
		date: this.getApproximateEndDate(),
		result: +this.parseSparResult(entry.textContent, heroName),
		logID: this.logID
	});
};

ui_log.saveDungeonResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroNames = this.getDungeonHeroNames();
	if (!entry || !heroNames[this.godname]) return;
	this.addActivity({
		type: 'dungeon',
		date: this.getApproximateEndDate(),
		result: this.parseDungeonResult(entry.textContent, heroNames),
		logID: this.logID
	});
};
