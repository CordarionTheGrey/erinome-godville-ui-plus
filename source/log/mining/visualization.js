var visualization = ui_lmining.visualization = {};
(function() {

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {number} fi - Frame index
 * @returns {string}
 */
var _generateBitsSummary = function(md, fi) {
	var unlocked = md.freeUnlockedBits[fi],
		locked = md.freeLockedBits[fi],
		a = md.bossStats[0].collected[fi],
		b = md.bossStats[1].collected[fi],
		c = md.bossStats[2].collected[fi],
		d = md.bossStats[3].collected[fi],
		initial = md.initialBits[fi],
		synthesized = md.synthesizedBits[fi],
		destroyed = md.destroyedBits[fi],
		destroyedUndiscovered = md.destroyedUndiscoveredBits[fi],
		ac = md.bossStats[0].createdBits[fi],
		bc = md.bossStats[1].createdBits[fi],
		cc = md.bossStats[2].createdBits[fi],
		dc = md.bossStats[3].createdBits[fi],
		created = ac + bc + cc + dc,
		s = GUIp_i18n.bits_summary +
			'<abbr title="' + GUIp_i18n.fmt('bits_free', unlocked + ' + ' + locked) + '">' +
			(unlocked + locked) +
			'</abbr> + <abbr title="' +
				GUIp_i18n.fmt('bits_collected', a + '\xA0+\xA0' + b + '\xA0+\xA0' + c + '\xA0+\xA0' + d) +
			'">' + (a + b + c + d) +
			'</abbr> = <abbr title="' + (
				synthesized + created + destroyed + destroyedUndiscovered ? GUIp_i18n.bits_initial : GUIp_i18n.bits_total
			) + '">' +
			initial +
			(destroyedUndiscovered ? '</abbr>…' + (initial + destroyedUndiscovered) : '</abbr>');
	if (synthesized) {
		s += ' + <abbr title="' + GUIp_i18n.bits_synthesized + '">' +
			synthesized +
			'</abbr>';
	}
	if (created) {
		s += ' + <abbr title="' +
			GUIp_i18n.fmt('bits_created', ac + '\xA0+\xA0' + bc + '\xA0+\xA0' + cc + '\xA0+\xA0' + dc) +
			'">' + created + '</abbr><sup>=</sup>';
	}
	if (destroyed + destroyedUndiscovered) {
		s += ' − <abbr title="' + GUIp_i18n.bits_destroyed + '">' +
			destroyed +
			(destroyedUndiscovered ? '</abbr>…' + (destroyed + destroyedUndiscovered) : '</abbr>');
	}
	return s;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis.BossStats} stat
 * @param {number} fi - Frame index
 * @returns {string}
 */
var _generateBossActionsSummary = function(md, stat, fi) {
	var s = '',
		i = 0,
		n = 0;
	if ((n = stat.pushes[fi])) {
		s = '<span title="' + GUIp_i18n.rmap_pushes + '">' + n + '<sup style="font-size: 1.01em;">⇵</sup></span>';
	}
	if ((n = stat.encouragements[fi])) {
		if (s) s += ', ';
		s += '<span title="' + GUIp_i18n.rmap_encouragements + '">' + n + '<sup>+</sup></span>';
	}
	if ((n = stat.punishments[fi])) {
		if (s) s += ', ';
		s += '<span title="' + GUIp_i18n.rmap_punishments + '">' + n + '<sup>−</sup></span>';
	}
	if ((n = stat.miracles[fi])) {
		if (s) s += ', ';
		s += '<span title="' + GUIp_i18n.rmap_miracles + '">' + n + '<sup>=</sup></span>';
	}
	if ((n = stat.collected[fi]) >= md.bitsPerByte) {
		n /= md.bitsPerByte;
		if (s) s += ' | ';
		s += '<span title="' + GUIp_i18n.rmap_success_steps + '"><sup>•</sup>' + (stat.successSteps[0] + 1);
		for (i = 2; i <= n; i++) {
			s += ', <sup>•</sup>' + (stat.successSteps[i - 1] + 1);
		}
		s += '</span>';
	}
	return s;
};

visualization._model = null;
visualization._summaryNode = null;
visualization._bossActionsNodes = [];

var _updateExtraInfo = function() {
	var md = visualization._model,
		fi = dom.getCurrentFrameIndex(md);
	visualization._summaryNode.innerHTML = _generateBitsSummary(md, fi);
	for (var i = 0; i < 4; i++) {
		visualization._bossActionsNodes[i].innerHTML = _generateBossActionsSummary(md, md.bossStats[i], fi);
	}
};

visualization.init = function() {
	var map = document.getElementsByClassName('wrmap')[0];
	if (!map) return;
	if (ui_log.storage.getList('Option:rangeMapSettings').includes('grid')) {
		map.classList.add('e_rmap_grid');
	}
	map.parentNode.insertAdjacentHTML('beforeend',
		'<div class="e_bits_summary"><a href="#">' + GUIp_i18n.rmap_more_info + '</a></div>'
	);
	visualization._summaryNode = map.parentNode.lastChild;
	GUIp.common.addListener(visualization._summaryNode.firstChild, 'click', function(ev) {
		var table = document.getElementById('h_tbl'),
			i = 0,
			nodes;
		ev.preventDefault();
		visualization._model = analysis.processFrameGroups(
			dom.extractFrameGroups(),
			dom.getBitsPerByte(),
			GUIp.common.mining.parseConditions(dom.getSpecialCondition())
		);
		for (i = 0; i < 4; i++) {
			visualization._bossActionsNodes[i] = document.createElement('div');
			visualization._bossActionsNodes[i].className = 'e_boss_actions_summary';
		}
		_updateExtraInfo();
		table.classList.add('e_detailed');
		nodes = table.getElementsByClassName('c1');
		for (i = 0; i < 4; i++) {
			nodes[i].appendChild(visualization._bossActionsNodes[i]);
		}
		GUIp.common.newMutationObserver(_updateExtraInfo).observe(map, {childList: true, subtree: true});
		GUIp.common.tooltips.watchSubtree(visualization._summaryNode);
	});
};

})(); // ui_log.mining.visualization
