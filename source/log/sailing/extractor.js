var ui_imapext = ui_log.islandsMapExt = {};
(function() {

/**
 * @typedef {Object} GUIp.log.islandsMapExt.Data
 * @property {?Array<number>} map
 * @property {?Object<number, !Array<number>>} layeredMap
 * @property {?Object<number, !Array>} tracks
 * @property {?Array<number>} pois
 * @property {?Object<number, !Array<number>>} layeredPois
 */

var _extract = function(regex, text) {
	var m = regex.exec(text);
	return m && GUIp.common.parseJSON(m[1]);
};

/**
 * @private
 * @param {!Object} obj
 * @param {function(*): boolean} checkValue
 * @returns {boolean}
 */
var _validate = function(obj, checkValue) {
	var keys = Object.keys(obj),
		len = keys.length,
		key = '';
	for (var i = 0; i < len; i++) {
		key = keys[i];
		if (!Number.isSafeInteger(+key) || !checkValue(obj[key])) {
			GUIp.common.warn('invalid key-value pair:', key, '=>', obj[key]);
			return false;
		}
	}
	return !!len;
};

/**
 * @returns {!GUIp.log.islandsMapExt.Data}
 */
ui_imapext.extractData = function() {
	var scripts = document.getElementsByTagName('script'),
		scriptText = '',
		map = null,
		layeredMap = null,
		tracks = null,
		pois = null,
		layeredPois = null,
		a;
	for (var i = 0, len = scripts.length; i < len; i++) {
		scriptText = scripts[i].textContent;
		// map data
		a = _extract(/\bm\s*=\s*(\[[^\]]*\])\s*;/, scriptText);
		if (GUIp.common.isIntegralArray(a) && a.length) {
			map = a;
		}
		// points of interest
		a = _extract(/\bsmh\s*=\s*(\[[^\]]*\])\s*;/, scriptText);
		if (GUIp.common.isIntegralArray(a) && a.length) {
			pois = a;
		}
		// tracks
		if ((a = _extract(/\btr\s*=\s*(\{[^}]*\})\s*;/, scriptText)) && _validate(a, Array.isArray)) {
			tracks = a;
		}
		// layered map data
		if ((a = _extract(/\bmc\s*=\s*(\{[^}]*\})\s*;/, scriptText)) && _validate(a, GUIp.common.isIntegralArray)) {
			layeredMap = a;
		}
		// layered points of interest
		if ((a = _extract(/\bsmhc\s*=\s*(\{[^}]*\})\s*;/, scriptText)) && _validate(a, GUIp.common.isIntegralArray)) {
			layeredPois = a;
		}
	}
	return {map: map, layeredMap: layeredMap, tracks: tracks, pois: pois, layeredPois: layeredPois};
};

/**
 * @private
 * @param {string} s
 * @returns {GUIp.common.islandsMap.Vec}
 */
var _parseVec = function(s) {
	return GUIp.common.islandsMap.vec.make(parseInt(s), +s.slice(s.lastIndexOf('_') + 1));
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!Object<number, !Array>} tracks
 */
ui_imapext.setArksFromTracks = function(model, tracks) {
	var step = model.step - 1,
		pos = 0x0,
		positions, s;
	for (var i = 1; i <= 4; i++) {
		if ((positions = tracks[i]) && (s = positions[step])) {
			pos = _parseVec(String(s));
			if (pos in model.tiles) {
				model.arks[i - 1] = pos;
			} else {
				GUIp.common.warn('invalid position for ark #' + i, 'at step #' + (step + 1) + ':', s);
			}
		}
	}
};

})(); // ui_imapext
