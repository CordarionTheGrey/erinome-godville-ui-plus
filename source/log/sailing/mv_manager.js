var ui_imapwcm = ui_log.islandsMapWCM = {};
(function() {

/**
 * @class
 * @implements {GUIp.common.islandsMap.IMVManager}
 * @param {!Array<!GUIp.common.islandsMap.Model>} models
 * @param {number} step
 * @param {!Object<string, boolean>} conditions
 */
ui_imapwcm.WholeChronicleMVManager = function(models, step, conditions) {
	var len = models.length,
		lastModel = models[len - 1];
	/**
	 * @readonly
	 * @type {!Array<!GUIp.common.islandsMap.Model>}
	 */
	this.models = models;
	this.model = models[step - 1] || lastModel;
	this.view = null;
	this._mapRadius = GUIp.common.islandsMap.defaults.predictBorderRadius(lastModel, conditions);
	this._expansionRollbackInfo = [];
	this._originalRadiuses = new Int8Array(len);
	for (var i = 0; i < len; i++) {
		this._originalRadiuses[i] = models[i].radius;
	}
};

ui_imapwcm.WholeChronicleMVManager.prototype = {
	constructor: ui_imapwcm.WholeChronicleMVManager,

	replaceModelAndView: function(svg, step) {
		this.model = this.models[step - 1] || this.models[this.models.length - 1];
		this.replaceView(svg);
	},

	replaceView: function(svg) {
		this.view = GUIp.common.islandsMap.domParsers.vFromSVG(svg);
	},

	_createRedrawCaller: function() {
		var model = this.models[0],
			rawModel;
		return function(redraw) {
			if (!rawModel) rawModel = GUIp.common.islandsMap.conv.encode(model);
			redraw(rawModel);
		};
	},

	expandMap: function(fill) {
		var j = 0,
			rollbackIndex = 0,
			model;
		for (var i = 0, len = this.models.length; i < len; i++) {
			model = this.models[i];
			if (i !== j) {
				if (this._mapRadius > model.radius) {
					model.radius = this._mapRadius;
				}
			} else {
				this._expansionRollbackInfo[rollbackIndex] = GUIp.common.islandsMap.mtrans.expand(
					model,
					this._mapRadius,
					fill,
					this._expansionRollbackInfo[rollbackIndex] || null
				);
				j += 10;
				rollbackIndex++;
			}
		}
		return this._createRedrawCaller();
	},

	unexpandMap: function() {
		var j = 0,
			rollbackIndex = 0;
		for (var i = 0, len = this.models.length; i < len; i++) {
			if (i !== j) {
				this.models[i].radius = this._originalRadiuses[i];
			} else {
				GUIp.common.islandsMap.mtrans.unexpand(this.models[i], this._expansionRollbackInfo[rollbackIndex++]);
				j += 10;
			}
		}
		this._expansionRollbackInfo.length = 0;
		return this._createRedrawCaller();
	},

	/**
	 * It's technically not a valid map because it defines multiple tiles at the same position,
	 * but LEM is able to parse it and prefers this format.
	 *
	 * @param {number} step
	 * @returns {!Array<number>}
	 */
	encodeMapWithDuplicatesUntil: function(step) {
		var result = [],
			encodeTilePosCode = GUIp.common.islandsMap.conv.encodeTilePosCode,
			rematerializationStep = 10,
			i = 0,
			j = 0,
			k = 0,
			jlen = 0,
			key = '',
			code = 0x0,
			tiles, keys, prevTiles;
		for (i = 0; i < step; i++) {
			tiles = this.models[i].tiles;
			keys = Object.keys(tiles);
			j = 0;
			jlen = keys.length;
			if (i !== rematerializationStep) {
				for (; j < jlen; j++) {
					key = keys[j];
					result[k++] = encodeTilePosCode(+key, tiles[key]);
				}
			} else {
				for (; j < jlen; j++) {
					key = keys[j];
					code = tiles[key];
					// if the map got rematerialized, it will have redundant tiles, which we should ignore here
					if (code !== prevTiles[key]) {
						result[k++] = encodeTilePosCode(+key, code);
					}
				}
				rematerializationStep += 10;
			}
			prevTiles = tiles;
		}
		GUIp.common.replaceArrayTail(result, k, GUIp.common.islandsMap.conv.encodeArks(this.models[step - 1]));
		return result;
	}
};

/**
 * @param {!GUIp.log.islandsMapExt.Data} extracted
 * @returns {number}
 */
var _getSteps = function(extracted) {
	var steps = 0,
		n = 0,
		track;
	for (var i = 1; i <= 4; i++) {
		if ((track = extracted.tracks[i]) && (n = track.length) > steps) {
			steps = n;
		}
	}
	return steps + 1; // there are no arks on the map on the last step
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.log.islandsMapExt.Data} extracted
 * @param {number} step
 */
var _decodeAndAdd = function(model, extracted, step) {
	var a;
	if ((a = extracted.layeredMap[step])) {
		GUIp.common.islandsMap.conv.decodeAndAddTiles(model, a);
	}
	if (extracted.layeredPois && (a = extracted.layeredPois[step])) {
		GUIp.common.islandsMap.conv.decodeAndAddPois(model, a);
	}
};

/**
 * @private
 * @param {!Object<GUIp.common.islandsMap.Vec, number>} from
 * @param {!Object<GUIp.common.islandsMap.Vec, number>} to
 */
var _rematerialize = function(from, to) {
	for (var key in from) {
		if (!(key in to)) {
			to[key] = from[key];
		}
	}
};

var _migrateWhirlpools = function(from, to) {
	var len = to.whirlpools.length;
	if (len) {
		GUIp.common.replaceArrayTail(to.whirlpools, len, from.whirlpools);
	} else {
		to.whirlpools = from.whirlpools;
	}
};

var _migrateBeasties = function(to, oldArr, newArr) {
	var pos = 0x0;
	for (var i = 0, len = oldArr.length; i < len; i++) {
		pos = oldArr[i];
		// contrary to whirlpools, beasties can disappear from the map
		if ((to.tiles[pos] | 0x20) === 0x62) { // Bb
			newArr.push(pos);
		}
	}
};

var _migrateThermoHints = function(from, to) {
	var i = 0,
		j = 0,
		len = from.thermos.length,
		jlen = to.thermos.length,
		th;
	if (!jlen) {
		to.thermos = from.thermos;
		return;
	}
outer:
	for (i = 0; i < len; i++) {
		th = from.thermos[i];
		for (j = 0; j < jlen; j++) {
			if (th.pos === to.thermos[j].pos) {
				continue outer;
			}
		}
		to.thermos.push(th);
	}
};

/**
 * @param {!GUIp.log.islandsMapExt.Data} extracted
 * @param {number} step
 * @param {!Object<string, boolean>} conditions
 * @returns {!GUIp.log.islandsMapWCM.WholeChronicleMVManager}
 */
ui_imapwcm.create = function(extracted, step, conditions) {
	var oldModel = new GUIp.common.islandsMap.Model(1),
		models = [oldModel],
		rematerializationStep = 10,
		newModel;
	_decodeAndAdd(oldModel, extracted, 1);
	if (oldModel.port === 0x8080) {
		GUIp.common.islandsMap.mtrans.guessPort(oldModel);
	}
	ui_imapext.setArksFromTracks(oldModel, extracted.tracks);
	for (var i = 1, steps = _getSteps(extracted); i < steps; i++) {
		newModel = new GUIp.common.islandsMap.Model(i + 1);
		if (i !== rematerializationStep) {
			newModel.tiles = Object.create(oldModel.tiles);
		}
		newModel.radius = oldModel.radius;
		newModel.borderRadius = oldModel.borderRadius;
		newModel.nonBorderRadius = oldModel.nonBorderRadius;
		_decodeAndAdd(newModel, extracted, i + 1);
		if (i === rematerializationStep) {
			// we re-create `tiles` hash map from scratch every 10 steps to avoid long prototype chains.
			// number 10 was chosen since it's a square root of 100 (maximal number of steps in a sailing).
			//
			// more formally, assuming:
			// n = number of steps,
			// m = number of visible tiles on the last step,
			// t = total number of unique tiles,
			//
			// we have a data structure with operations:
			// getTile(step, pos): O(sqrt(n))
			// setTile(step, pos, tile): O(sqrt(n))
			// memory consumption: O(t + m * sqrt(n))
			_rematerialize(oldModel.tiles, newModel.tiles);
			rematerializationStep += 10;
		}
		if (newModel.port === 0x8080) {
			if (oldModel.port in newModel.tiles) {
				newModel.port = oldModel.port;
			} else {
				GUIp.common.islandsMap.mtrans.guessPort(newModel);
			}
		}
		ui_imapext.setArksFromTracks(newModel, extracted.tracks);
		_migrateWhirlpools(oldModel, newModel);
		_migrateBeasties(newModel, oldModel.beasties, newModel.beasties);
		_migrateBeasties(newModel, oldModel.roamingBeasties, newModel.roamingBeasties);
		_migrateThermoHints(oldModel, newModel);
		GUIp.common.islandsMap.mtrans.migrate(oldModel, newModel);
		models[i] = oldModel = newModel;
	}
	return new ui_imapwcm.WholeChronicleMVManager(models, step, conditions);
};

})(); // ui_imapwcm
