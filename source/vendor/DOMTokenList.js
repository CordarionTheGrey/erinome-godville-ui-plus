// polyfills of several DOMTokenList methods' overloads for Opera 12
(function() {
'use strict';

var ls = document.createElement('div').classList,
	nativeAdd = ls.add,
	nativeRemove = ls.remove,
	nativeToggle = ls.toggle,
	descriptor = {configurable: true, writable: true, value: null};

ls.add('a', 'b');
if (ls.length === 1) {
	ls.add('b');
	descriptor.value = function add() {
		for (var i = 0, len = arguments.length; i < len; i++) {
			nativeAdd.call(this, arguments[i]);
		}
	};
	Object.defineProperty(DOMTokenList.prototype, 'add', descriptor);
}

ls.remove('a', 'b');
if (ls.length === 1) {
	descriptor.value = function remove() {
		for (var i = 0, len = arguments.length; i < len; i++) {
			nativeRemove.call(this, arguments[i]);
		}
	};
	Object.defineProperty(DOMTokenList.prototype, 'remove', descriptor);
}

ls.toggle('a', false);
if (ls.contains('a')) {
	descriptor.value = function toggle(token, force) {
		if (force === undefined) {
			return nativeToggle.call(this, token);
		} else if (force) {
			nativeAdd.call(this, token);
			return true;
		} else {
			nativeRemove.call(this, token);
			return false;
		}
	};
	Object.defineProperty(DOMTokenList.prototype, 'toggle', descriptor);
}

})();
