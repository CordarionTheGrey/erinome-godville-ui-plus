// topic other improvements
var fixPaginationLinks = function() {
	var links, a;
	if (queryString.get1('epost') == null) {
		return;
	}
	links = $Q('.pagination a');
	for (var i = 0, len = links.length; i < len; i++) {
		a = links[i];
		a.href = a.href.replace(/([?&])epost=[^&#]*&?/, '$1');
	}
};
var checkHash = function() {
	var postID = '',
		post, m, table;
	// scroll to a certain post #
	if ((postID = queryString.get1('epost')) && (post = $C('spacer')[+postID - 1])) {
		location.hash = post.id;
		queryString.remove('epost');
		history.replaceState(history.state, '',
			location.pathname + GUIp.common.stringifyQueryString(queryString) + location.hash
		);
	}

	if ((m = /#post_(\d+)/.exec(location.hash))) {
		postID = m[1];
		// highlight target post
		if (!storage.getFlag('Option:disableTargetPostHighlight')) {
			if ((post = $c('e_highlight'))) {
				post.classList.remove('e_highlight');
			}
			if ((post = $id('post_' + postID + '-row'))) {
				post.classList.add('e_highlight');
				// force redrawing to workaround a rendering issue in Chrome
				var tmpStyle = post.style.cssText;
				post.style.cssText += ';-webkit-transform:rotateZ(0deg)';
				post.offsetHeight;
				post.style.cssText = tmpStyle;
			}
		}
		// show a warning when target post appears to be on another page
		if (!$id('post_' + postID)) {
			try {
				table = $q('table.posts');
				table.insertAdjacentHTML('beforebegin',
					'<div class="e_missing_post">' + GUIp_i18n.forum_missing_target_post + '</div>'
				);
				table.previousSibling.getElementsByTagName('a')[0].href =
					'/forums/redirect_to_post/' + /\/show_topic\/+(\d+)/.exec(location.pathname)[1] + '?post=' + postID;
				if ((post = $id('pnf'))) {
					post.style.display = 'none'; // our warning is more informative than Godville's one
				}
			} catch (e) {
				GUIp.common.error(e);
			}
		}
	}
};
var findPost = function(el) {
	do {
		el = el.parentNode;
	} while (!el.classList.contains('post'));
	return el;
};
var picturesAutoreplace = function() {
	if (!storage.getFlag('Option:disableLinksAutoreplace')) {
		var links;
		try {
			links = $Q('.post .body a[href*=".jpeg" i], .post .body a[href*=".jpg" i], .post .body a[href*=".png" i], .post .body a[href*=".gif" i]');
		} catch (e) {
			links = Array.prototype.filter.call($Q('.post .body a'), function(a) {
				return /\.(?:jpe?g|png|gif)/i.test(a.href);
			});
		}
		var imgs = [],
			onerror = function(i) {
				links[i].removeChild(links[i].getElementsByTagName('img')[0]);
				imgs[i] = null;
			},
			onload = function(i) {
				var oldBottom, hash = location.hash.match(/\d+/),
					post = findPost(links[i]),
					linkBeforeCurrentPost = hash ? +post.id.match(/\d+/)[0] < +hash[0] : false;
				if (linkBeforeCurrentPost) {
					oldBottom = post.getBoundingClientRect().bottom;
				}
				links[i].removeChild(links[i].getElementsByTagName('img')[0]);
				var hint = links[i].innerHTML;
				links[i].outerHTML = '<div class="img_container"><a id="link' + i + '" href="' + links[i].href + '" target="_blank" alt="' + worker.GUIp_i18n.open_in_a_new_tab + '"></a><div class="hint">' + hint + '</div></div>';
				imgs[i].alt = hint;
				var new_link = document.getElementById('link' + i),
					width = Math.min(imgs[i].width, 456),
					height = imgs[i].height*(imgs[i].width <= 456 ? 1 : 456/imgs[i].width);
				if (height < 1500) {
					new_link.insertAdjacentHTML('beforeend', '<div style="width: ' + width + 'px; height: ' + height + 'px; background-image: url(' + imgs[i].src + '); background-size: ' + width + 'px;"></div>');
				} else {
					new_link.insertAdjacentHTML('beforeend', '<div style="width: ' + width + 'px; height: 750px; background-image: url(' + imgs[i].src + '); background-size: ' + width + 'px;"></div>' +
															 '<div id="linkcrop' + i + '" style="width: ' + width + 'px; height: ' + (342*width/456) + 'px; background-image: url(' + worker.GUIp_getResource('images/crop.png') + '); background-size: ' + width + 'px; position: absolute; top: ' + (750 - 171*width/456) + 'px;"></div>' +
															 '<div style="width: ' + width + 'px; height: 750px; background-image: url(' + imgs[i].src + '); background-size: ' + width + 'px; background-position: 100% 100%;"></div>');
					if (worker.GUIp_browser === 'Opera') {
						worker.GUIp_getResource('images/crop.png',document.getElementById('linkcrop' + i));
					}
				}
				if (linkBeforeCurrentPost) {
					var diff = post.getBoundingClientRect().bottom - oldBottom;
					worker.scrollTo(0, worker.scrollY + diff);
				}
			};
		for (i = 0, len = links.length; i < len; i++) {
			links[i].insertAdjacentHTML('beforeend', '<img class="img_spinner" src="/images/spinner.gif">');
			imgs[i] = document.createElement('img');
			GUIp.common.addListener(imgs[i], 'error', onerror.bind(null, i));
			GUIp.common.addListener(imgs[i], 'load', onload.bind(null, i));
			imgs[i].src = links[i].href;
		}
	}
};
var updatePostsNumber = function() {
	var topics = JSON.parse(storage.get('ForumSubscriptions')) || {},
		informers = JSON.parse(storage.get('ForumInformers'));
	if (topics[topic]) {
		var notify = {};
		var page = (+queryString.get1('page') || 1) - 1;
		var posts = page*25 + document.getElementsByClassName('post').length;
		var posts_total = getTotalPosts() || posts;
		var is_last_page = !document.querySelector('.next_page') || !!document.querySelector('.next_page.disabled');
		if (topics[topic].posts < posts) {
			topics[topic].posts = posts;
			var dates = document.getElementsByTagName('abbr');
			topics[topic].date = (new worker.Date(dates[dates.length - 1].title)).getTime();
			topics[topic].by = document.querySelectorAll('.fn > span:first-child > a')[dates.length - 1].textContent;
			notify = {posts: posts, date: topics[topic].date, by: topics[topic].by};
		}
		if (informers[topic]) {
			if (is_last_page) {
				notify.obsolete = true;
			} else if (posts > informers[topic].posts) {
				notify.iposts = posts;
				notify.tposts = posts_total;
			}
		}
		if (Object.keys(notify).length > 0) {
			notify.tid = +topic;
			notify.ndate = Date.now();
			GUIp.common.debug('writing forum notification:', JSON.stringify(notify));
			storage.set('ForumInformersNotify' + Math.random(), JSON.stringify(notify));
		}
	}
};
/**
 * @param {string} text
 * @returns {?Node} null if nothing needs to be done.
 */
var expandLinksInText = function(text) {
	var lastPos = 0,
		pos = 0,
		url = '',
		nesting = 0,
		oParenPos = 0,
		cParenPos = 0,
		linkRegex, m, result, a;
	if (!text.includes('://')) return null; // fast path
	linkRegex = /https?:\/\/[\wа-яё/=]+(?:[-!#$%&'(*+,.:;?@~]+[\wа-яё)/=]+)*/gi;
	m = linkRegex.exec(text);
	if (!m) return null;
	result = document.createDocumentFragment();
	do {
		url = m[0];
		pos = m.index;
		if (pos !== lastPos) {
			result.appendChild(document.createTextNode(text.slice(lastPos, pos)));
		}
		lastPos = linkRegex.lastIndex;
		// cut the link as soon as unbalanced parenthesis is encountered,
		// so that links placed inside parentheses can be parsed correctly
		nesting = 0;
		oParenPos = url.indexOf('(') >>> 0;
		cParenPos = url.indexOf(')');
		while (cParenPos >= 0) {
			if (oParenPos < cParenPos) {
				nesting++;
				oParenPos = url.indexOf('(', oParenPos + 1) >>> 0;
			} else if (nesting--) {
				cParenPos = url.indexOf(')', cParenPos + 1);
			} else {
				// unbalanced closing parenthesis
				// strip trailing punctuation from the link
				while ("-!#$%&'(*+,.:;?@~".includes(url[cParenPos - 1])) {
					cParenPos--;
				}
				url = url.slice(0, cParenPos);
				// we do not rescan the tail for more links, since that would result in O(n**2) complexity
				lastPos = pos + cParenPos;
				break;
			}
		}
		a = document.createElement('a');
		a.href = a.textContent = url;
		result.appendChild(a);
	} while ((m = linkRegex.exec(text)));
	if (lastPos !== text.length) {
		result.appendChild(document.createTextNode(text.slice(lastPos)));
	}
	return result;
};
/**
 * @param {!Element} postBody
 */
var expandPlainTextLinksInPost = function(postBody) {
	var cur = postBody.firstChild,
		steppedOut = false,
		willStepOut = false,
		tagName = '',
		next, newNode;
	if (!postBody.textContent.includes('://')) {
		return; // fast path; also handles the case when cur is null
	}
	while (true) {
		next = cur.nextSibling;
		if ((willStepOut = !next)) {
			next = cur.parentNode;
		}
		switch (cur.nodeType) {
			case 1: // element
				if (!steppedOut && (tagName = cur.tagName) !== 'A' && tagName !== 'PRE' && tagName !== 'CODE' && (cur = cur.firstChild)) {
					continue; // step into it
				}
				break;
			case 3: // text
				if ((newNode = expandLinksInText(cur.nodeValue))) {
					cur.parentNode.replaceChild(newNode, cur);
				}
				break;
		}
		if (next === postBody) return;
		cur = next;
		steppedOut = willStepOut;
	}
};
var expandPlainTextLinks = function() {
	Array.prototype.forEach.call($Q('.post .body'), expandPlainTextLinksInPost);
};
var checkInternalLinks = function() {
	var target, links = $Q('.post .body a[href*="/show_topic/"], #chronicle .post_content a[href*="/show_topic/"]');
	for (i = 0, len = links.length; i < len; i++) {
		if (target = links[i].href.match(/^(https?:\/\/godville(?:game\.com|\.net)\/+forums)\/+show_topic\/+(\d+)\?page=\d+#post_(\d+)$/i)) {
			links[i].href = target[1] + '/redirect_to_post/' + target[2] + '?post=' + target[3];
		}
	}
};
var improveTopic = function() {
	fixPaginationLinks();
	checkHash();
	if (GUIp.common.isAndroid) {
		// on mobile devices, it might be hard to impossible to select and copy a plain-text link if it is long enough,
		// so let's make users' life easier by making those links clickable. we do not change anything for desktop
		// browsers, which are used by the majority of UI+ users, since it's not a problem to follow such link there and
		// somebody needs to tell plain-text-link posters to stop doing that.
		expandPlainTextLinks();
	}
	picturesAutoreplace();
	updatePostsNumber();
	checkInternalLinks();
};
