var godPage = {};
(function() {

var _improveGravatars = function() {
	var av = document.querySelector('#avatar img');
	var avSwitch = function() {
		if (av.src.includes('&size=50')) {
			var oldSrc = av.src, img = new Image;
			GUIp.common.addListener(img, 'load', function() {
				av.height = av.width = 50;
				av.src = img.src;
				av.parentNode.parentNode.classList.toggle('e_av_large');
			});
			GUIp.common.addListener(img, 'error', function() {
				av.height = av.width = 50;
				av.src = oldSrc;
			});
			img.src = av.src.replace('&size=50','&size=200');
			av.height = av.width = 32;
			av.src = worker.GUIp_getResource('images/loader.gif');
		} else if (av.src.includes('&size=200')) {
			av.parentNode.parentNode.classList.toggle('e_av_large');
		}
	};
	if (av.src.includes('gravatar.com/avatar/')) {
		av.classList.add('e_clickable');
		GUIp.common.addListener(av, 'click', avSwitch);
	}
};

var _fixExoticCharacters = function() {
	var spans = $Q('#ach_b li > span'),
		replacement = '<span class="e_emoji e_emoji_arrow' +
			(GUIp.common.renderTester.testChar('⤑') ? '' : ' eguip_font') +
		'">⤑</span>',
		html = '',
		span;
	for (var i = 0, len = spans.length; i < len; i++) {
		span = spans[i];
		html = span.innerHTML;
		if (html.includes('⤑')) {
			span.innerHTML = html.replace(/⤑/g, replacement);
		}
	}
};

godPage.init = function() {
	checkInternalLinks();
	_improveGravatars();
	_fixExoticCharacters();
	addFormattingButtons(false);
	GUIp.common.renderTester.deinit();
};

})(); // godPage
