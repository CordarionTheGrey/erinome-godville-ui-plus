var lastFights = {};
(function() {

/**
 * @private
 * @param {!HTMLCollection} rows
 * @returns {!Array<!GUIp.common.activities.LastFightsEntry>}
 */
var _getLastFights = function(rows) {
	var result = [],
		date, ftype, link, href;
	for (var i = 1, len = rows.length; i < len; i++) {
		date = rows[i].firstElementChild;
		ftype = date.nextElementSibling;
		link = ftype.getElementsByTagName('a')[0];
		href = link.href;
		result[i - 1] = {
			date: +GUIp.common.parseDateTime(date.textContent),
			type: GUIp.common.activities.parseFightType(link.textContent),
			logID: href.slice(href.lastIndexOf('/') + 1),
			success: ftype.textContent.includes('✓')
		};
	}
	return result.sort(function(a, b) { return a.date - b.date; });
};

lastFights.init = function() {
	var table = document.getElementsByTagName('table')[0],
		rows = table.getElementsByTagName('tr'),
		activities = GUIp.common.activities.load(storage),
		byID = Object.create(null),
		desc = null,
		i, len, act, cell, href;
	GUIp.common.activities.updateLastFights(
		storage,
		GUIp.common.activities.readThirdEyeFromLS(storage.god_name),
		GUIp.common.parseJSON(storage.get('ThirdEye:Gaps')) || [],
		activities,
		GUIp.common.activities.loadStatuses(storage),
		_getLastFights(rows)
	);
	for (i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
			byID[act.logID] = act;
		}
	}
	table.classList.add('e_last_fights');
	for (i = 1, len = rows.length; i < len; i++) {
		cell = rows[i].firstElementChild.nextElementSibling;
		href = cell.getElementsByTagName('a')[0].href;
		if (!(act = byID[href.slice(href.lastIndexOf('/') + 1)]) || !act.result) {
			continue;
		}
		desc = GUIp.common.activities.describe(act, desc);
		cell.insertAdjacentHTML('beforeend',
			'<div class="e_fight_result ' + desc.class + '" title="' + desc.title + '">' + desc.content + '</div>'
		);
	}
};

})(); // lastFights
